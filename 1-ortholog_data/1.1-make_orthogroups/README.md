Generate orthogroups from protein sequence simularity
================
Gareth Gillard
20/06/2018

``` r
knitr::opts_chunk$set(eval = FALSE, warning=FALSE, message=FALSE, error=FALSE)
options(stringsAsFactors = FALSE)

# Load require R librarys and source code
library(phangorn)
library(seqinr)
library(ape)
library(tidyr)
library(dplyr)
library(knitr)

# Working directory on cigene Orion server
setwd("/mnt/users/garethg/orthogroups.2018")

# Latest clanfinder script from Simen
source("scripts/clanfinder_v7.R")
```

``` r
# Auto root a given tree using set out outgroup species
auto.root = function(tree, outgr=c("Gacu", "Olat", "Drer", "Locu", "Mmus", "Hsap")){
  put.root = unlist(sapply(outgr, function(i) grep(i, tree$tip.label)))
  names(put.root) <- sub('[:0-9:]', '', names(put.root))
  
  if(sum(unique(names(put.root)) %in% c('Mmus', 'Hsap'))==2){
    #cat('Two mammal outgroups\n')
    mam.root = grep('Mmus|Hsap', tree$tip.label)
    if(is.monophyletic(tree, mam.root)) { 
      #cat('Monophyletic mammal outgroup\n')
      root.node = getMRCA(tree, mam.root)
      return(root(tree, node =  root.node))
    } else { 
      #cat('Not monophyletic mammal outgroup\n....rooting with human\n')
      return(root(tree, outgroup = grep('Hsap', tree$tip.label)[1], resolve.root = T))
    }
  }
  if(sum(unique(names(put.root)) %in% c('Mmus', 'Hsap'))==1){
    #cat('One mammal outgroup\n')
    return(root(tree, outgroup = put.root[1], resolve.root = T))
  }
  if(sum(unique(names(put.root)) %in% c('Mmus', 'Hsap'))==0){
    #cat('No mammal outgroup\n')
    return(root(tree, outgroup = put.root[1], resolve.root = T))
  }
}
```

Generate gene level orthogroups from protein sequence data
==========================================================

**Species:**

| Common name | Scientific name | Code |
|:------------|:----------------|:-----|

*Mammals*

|       |               |      |
|:------|:--------------|:-----|
| Human | Homo\_sapiens | Hsap |
| Mouse | Mus\_musculus | Mmus |

*Non-teleost fish*

|             |                       |      |
|:------------|:----------------------|:-----|
| Spotted Gar | Lepisosteus\_oculatus | Locu |

*Non-salmonid teleosts*

|             |                         |      |
|:------------|:------------------------|:-----|
| Zebrafish   | Danio\_rerio            | Drer |
| Stickleback | Gasterosteus\_aculeatus | Gacu |
| Pike        | Esox\_lucius            | Eluc |
| Medaka      | Oryzias\_latipes        | Olat |

*Salmonids*

|                  |                       |      |
|:-----------------|:----------------------|:-----|
| Rainbow          | Oncorhynchus\_mykiss  | Omyk |
| Grayling         | Thymallus\_thymallus  | Tthy |
| Salmon\_atlantic | Salmo\_salar          | Ssal |
| Salmon\_coho     | Oncorhynchus\_kisutch | Okis |

Source of data
--------------

``` r
species_meta <- tbl_df(read.table("species_meta.txt", header = TRUE))
kable(species_meta)
```

| common\_name     | scientific\_name        | identifier | source  | protein\_fasta                                                                                                                  | cds\_fasta                                                                                                                               | gff                                                                                                                             |
|:-----------------|:------------------------|:-----------|:--------|:--------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Grayling         | Thymallus\_thymallus    | Tthy       | local   | /local/genome/references/Thymallus\_thymallus/2018-CEES/Tthy2\_maker\_filter2\_proteins.fasta                                   | /mnt/users/garethg/species\_data/Tthy/Tthy.cds.fa                                                                                        | /local/genome/references/Thymallus\_thymallus/2018-CEES/Maker\_filtered\_m\_filter2.gff                                         |
| Danube\_salmon   | Hucho\_hucho            | Hhuc       | local   | /local/genome/references/Hucho\_hucho/hucho.cds.pep                                                                             | /mnt/users/garethg/species\_data/Hucho\_hucho/hucho.cds.fa                                                                               | /local/genome/references/Hucho\_hucho/transcripts.fasta.transdecoder.genome.gff3                                                |
| Human            | Homo\_sapiens           | Hsap       | ensembl | <ftp://ftp.ensembl.org/pub/release-92/fasta/homo_sapiens/pep/Homo_sapiens.GRCh38.pep.all.fa.gz>                                 | <ftp://ftp.ensembl.org/pub/release-92/fasta/homo_sapiens/cds/Homo_sapiens.GRCh38.cds.all.fa.gz>                                          | <ftp://ftp.ensembl.org/pub/release-92/gff3/homo_sapiens/Homo_sapiens.GRCh38.92.gff3.gz>                                         |
| Mouse            | Mus\_musculus           | Mmus       | ensembl | <ftp://ftp.ensembl.org/pub/release-92/fasta/mus_musculus/pep/Mus_musculus.GRCm38.pep.all.fa.gz>                                 | <ftp://ftp.ensembl.org/pub/release-92/fasta/mus_musculus/cds/Mus_musculus.GRCm38.cds.all.fa.gz>                                          | <ftp://ftp.ensembl.org/pub/release-92/gff3/mus_musculus/Mus_musculus.GRCm38.92.gff3.gz>                                         |
| Zebrafish        | Danio\_rerio            | Drer       | ensembl | <ftp://ftp.ensembl.org/pub/release-92/fasta/danio_rerio/pep/Danio_rerio.GRCz11.pep.all.fa.gz>                                   | <ftp://ftp.ensembl.org/pub/release-92/fasta/danio_rerio/cds/Danio_rerio.GRCz11.cds.all.fa.gz>                                            | <ftp://ftp.ensembl.org/pub/release-92/gff3/danio_rerio/Danio_rerio.GRCz11.92.gff3.gz>                                           |
| Stickleback      | Gasterosteus\_aculeatus | Gacu       | ensembl | <ftp://ftp.ensembl.org/pub/release-92/fasta/gasterosteus_aculeatus/pep/Gasterosteus_aculeatus.BROADS1.pep.all.fa.gz>            | <ftp://ftp.ensembl.org/pub/release-92/fasta/gasterosteus_aculeatus/cds/Gasterosteus_aculeatus.BROADS1.cds.all.fa.gz>                     | <ftp://ftp.ensembl.org/pub/release-92/gff3/gasterosteus_aculeatus/Gasterosteus_aculeatus.BROADS1.92.gff3.gz>                    |
| Medaka           | Oryzias\_latipes        | Olat       | ensembl | <ftp://ftp.ensembl.org/pub/release-92/fasta/oryzias_latipes/pep/Oryzias_latipes.MEDAKA1.pep.all.fa.gz>                          | <ftp://ftp.ensembl.org/pub/release-92/fasta/oryzias_latipes/cds/Oryzias_latipes.MEDAKA1.cds.all.fa.gz>                                   | <ftp://ftp.ensembl.org/pub/release-92/gff3/oryzias_latipes/Oryzias_latipes.MEDAKA1.92.gff3.gz>                                  |
| Spotted\_Gar     | Lepisosteus\_oculatus   | Locu       | ensembl | <ftp://ftp.ensembl.org/pub/release-92/fasta/lepisosteus_oculatus/pep/Lepisosteus_oculatus.LepOcu1.pep.all.fa.gz>                | <ftp://ftp.ensembl.org/pub/release-92/fasta/lepisosteus_oculatus/cds/Lepisosteus_oculatus.LepOcu1.cds.all.fa.gz>                         | <ftp://ftp.ensembl.org/pub/release-92/gff3/lepisosteus_oculatus/Lepisosteus_oculatus.LepOcu1.92.gff3.gz>                        |
| Salmon\_atlantic | Salmo\_salar            | Ssal       | ncbi    | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_protein.faa.gz>     | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_cds_from_genomic.fna.gz>     | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_genomic.gff.gz>     |
| Rainbow\_trout   | Oncorhynchus\_mykiss    | Omyk       | ncbi    | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/163/495/GCF_002163495.1_Omyk_1.0/GCF_002163495.1_Omyk_1.0_protein.faa.gz>       | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/163/495/GCF_002163495.1_Omyk_1.0/GCF_002163495.1_Omyk_1.0_cds_from_genomic.fna.gz>       | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/163/495/GCF_002163495.1_Omyk_1.0/GCF_002163495.1_Omyk_1.0_genomic.gff.gz>       |
| Northern\_pike   | Esox\_lucius            | Eluc       | ncbi    | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/721/915/GCF_000721915.3_Eluc_V3/GCF_000721915.3_Eluc_V3_protein.faa.gz>         | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/721/915/GCF_000721915.3_Eluc_V3/GCF_000721915.3_Eluc_V3_cds_from_genomic.fna.gz>         | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/721/915/GCF_000721915.3_Eluc_V3/GCF_000721915.3_Eluc_V3_genomic.gff.gz>         |
| Salmon\_coho     | Oncorhynchus\_kisutch   | Okis       | ncbi    | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/021/735/GCF_002021735.1_Okis_V1/GCF_002021735.1_Okis_V1_protein.faa.gz>         | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/021/735/GCF_002021735.1_Okis_V1/GCF_002021735.1_Okis_V1_cds_from_genomic.fna.gz>         | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/021/735/GCF_002021735.1_Okis_V1/GCF_002021735.1_Okis_V1_genomic.gff.gz>         |
| Arctic\_char     | Salvelinus\_alpinus     | Salp       | ncbi    | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/910/315/GCF_002910315.2_ASM291031v2/GCF_002910315.2_ASM291031v2_protein.faa.gz> | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/910/315/GCF_002910315.2_ASM291031v2/GCF_002910315.2_ASM291031v2_cds_from_genomic.fna.gz> | <ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/910/315/GCF_002910315.2_ASM291031v2/GCF_002910315.2_ASM291031v2_genomic.gff.gz> |

Get species protein data
------------------------

Downloaded available protein fasta data from Ensembl ftp site: (release-89 at 04/08/2017)

``` r
# Make orthofinder and logs folders
system("mkdir fastas cds_fastas gff orthofinder logs")

# Creating unfiltered protein fastas
# Copy or download species protein and cds fasta, and gff3 files from species_meta file to a fastas folder
# For each species in meta file
for (i in 1:nrow(species_meta)) {
  # If protein fasta link is ftp
  if (length(grep("ftp", species_meta$protein_fasta[i]))) {
    # Download file from ftp link, extract, and rename into folder
    system(paste("wget", species_meta$protein_fasta[i]))
    system(paste("gunzip", basename(species_meta$protein_fasta[i])))
    system(paste0("mv ", sub(".gz", "", basename(species_meta$protein_fasta[i])), " fastas/", species_meta$identifier[i], ".fasta.unfiltered"))
  } else {
    # Else, copy file from local server and rename into fastas folder
    system(paste("cp ", species_meta$protein_fasta[i], " fastas/", species_meta$identifier[i], ".fasta.unfiltered", sep = ""))
  }
  
  # If cds fasta link is ftp
  if (length(grep("ftp", species_meta$cds_fasta[i]))) {
    # Download file from ftp link, extract, and rename into folder
    system(paste("wget", species_meta$cds_fasta[i]))
    system(paste("gunzip", basename(species_meta$cds_fasta[i])))
    system(paste0("mv ", sub(".gz", "", basename(species_meta$cds_fasta[i])), " cds_fastas/", species_meta$identifier[i], ".cds.fasta"))
  } else {
    # Else, copy file from local server and rename into folder
    system(paste("cp ", species_meta$cds_fasta[i], " cds_fastas/", species_meta$identifier[i], ".cds.fasta", sep = ""))
  }
  
  # If gff link is ftp
  if (length(grep("ftp", species_meta$gff[i]))) {
    # Download file from ftp link, extract, and rename into folder
    system(paste("wget", species_meta$gff[i]))
    system(paste("gunzip", basename(species_meta$gff[i])))
    system(paste0("mv ", sub(".gz", "", basename(species_meta$gff[i])), " gff/", species_meta$identifier[i], ".gff"))
  } else {
    # Else, copy file from local server and rename into folder
    system(paste("cp ", species_meta$gff[i], " gff/", species_meta$identifier[i], ".gff", sep = ""))
  }
}

# Make cds2protein id tables, for Ensembl cds fastas
ensembl.species <- c("Drer", "Olat", "Hsap", "Mmus", "Locu", "Gacu")
for (sp in ensembl.species) {
  system(paste0("cat gff/", sp, ".gff | grep -P '\tCDS\t' | cut -f9 | sed 's/.*Parent=transcript://' | sed 's/;protein_id=/\t/' | uniq > cds_fastas/", sp, ".cds2protein_id.table"))
}
```

Filter proteins to keep the longest protein per gene.
-----------------------------------------------------

``` r
# Species-specific function to remove redundant proteins from fasta files, so that there's only one longest protein per gene
filter_fasta <- function (species){
   print(paste("Processing", species, "..."))
  
  # Don't need to filter, only copy, Tthy and Hhuc protein files
  if (species %in% c("Tthy")) {
    system(paste0("cp fastas/", species, ".fasta.unfiltered fastas/", species, ".fasta"))
    print(paste(species, "done."))
    
  } else if (species %in% c("Hhuc")) {
    fa <- read.fasta(paste("fastas/", species, ".fasta.unfiltered", sep = ""))
    gene_names <- sub("Gene", "Hhuc|Gene", gsub(":.*", "", names(fa)))
    for (i in 1:length(fa)) {
      fa[[i]] <- toupper(sub("\\.", "-", fa[[i]]))
    }
    write.fasta(fa, names = gene_names, file = paste("fastas/", species, ".fasta", sep = ""))
    print(paste(species, "done."))
  
  } else {  
    
  # Read in fasta
  fa <- read.fasta(paste("fastas/", species, ".fasta.unfiltered", sep = ""))
  gene_names <- vector()
  
  # Check if protein file was from NCBI or Ensembl
  NCBI <- grepl("ncbi", filter(species_meta, identifier == species)$protein_fasta)
  ENSEMBL <- grepl("ensembl", filter(species_meta, identifier == species)$protein_fasta)
  
  # If species proteins are from NCBI, assign gene-protein names using NCBI gff
  if (NCBI == TRUE) {
    # get gene names from NCBI gff and create a gene <-> protein name table
    names_tbl <- read.table(paste0("species_gene2protein_tables/", species, ".gene2protein.txt"), sep = "\t", header = TRUE, blank.lines.skip = TRUE, fill = TRUE)
    # set protein fasta entry names to the protein name in the fasta header
    # create list of gene names from names_tbl by using linked protein names
    for (i in 1:length(fa)) { 
      protein_id <- gsub(">(\\S+).*", "\\1", attr(fa[[i]], "Annot"))
      names(fa)[i] <- protein_id
      attr(fa[[i]], "name") <- protein_id
      gene_name <- as.character(names_tbl[names_tbl$protein == protein_id, "gene_id"])
      gene_names[i] <- ifelse(length(gene_name) > 0, gene_name, "NA")
    }
  # For Ensembl species: assign gene-protein names using the "Annot" field
  } else if (ENSEMBL == TRUE) {
    # create list of gene names 
    for (i in 1:length(fa)) { 
        gene_names[i] <- gsub(".*gene:(\\S+).*", "\\1", attr(fa[[i]], "Annot"))
    }
  # Not NCBI or Ensembl
  } else {
    print(paste(species, "failed!"))
    break
  }
  
  # Filter fasta file, keeping only the longest protein per gene
  # No longer filter out short proteins (aa <= 30) to retain a protein per all genes
  # First remove gene_names and fasta entries with "NA"
  fa <- fa[gene_names != "NA"]
  gene_names <- gene_names[gene_names != "NA"]
  filtered_fa <- tapply(sapply(fa, c2s), gene_names, function(i) { return(i[which(nchar(i) == max(nchar(i)))]) })
  filtered_fa <- filtered_fa[filtered_fa != "NULL"]
  
  # Get first seq (encase of multiple longest) from the filtered list, converting format for writing
  new_fa <- list()
  protein_names <- list()
  for (i in 1:length(filtered_fa)) {
    new_fa[[i]] <- toupper(sapply(filtered_fa[[i]][1], s2c))
    protein_names[[i]] <- paste(species, "|", names(filtered_fa[[i]][1]), sep = "")
  }
  
  # Write the new filtered fasta file
  write.fasta(new_fa, protein_names, file = paste("fastas/", species, ".fasta", sep = ""))
  print(paste(species, "done."))
  }
}

# Creating filtered protein fasta files:
# Run filtering function on protein fasta file to filter only the longest protein entry per gene
species.list <- species_meta$identifier
for (s in species.list) {
  filter_fasta(s)
}

# Note: Hhuc has problem with protein gaps as "." instead of "-". Creates error with bastdb.
```

Run Orthofinder on filtered proteins to generate orthogroups
------------------------------------------------------------

``` r
# Move the final filtered fasta files into the orthofinder folder
system("mv fastas/*.fasta orthofinder/")

# Run orthofinder script (32 CPUS) to generate orthogroups
system("sbatch scripts/orthofinder.sh")
```

Protein alignment of orthogroups and generate trees
---------------------------------------------------

After generating the orthogroups (OGs) with orthofinder, run orthofinder script "trees\_for\_orthogroups.py" to generate multiple sequence alignments of OGs (with MAFFT) and then generate trees for OGs (with fasttree).

``` r
# Run alignments and generate OG trees
system("sbatch scripts/trees_for_orthogroups.sh orthofinder/Results 32")

# Read result into a list of trees
trees.files <- dir("orthofinder/Results_Jul25/Orthologues_Jul25/Gene_Trees/", full.names = TRUE)
trees <- lapply(trees.files, read.tree)
names(trees) <- sub("_tree.txt", "", basename(trees.files))

# Remove trees with less than 3 nodes
trees <- trees[sapply(trees, function(i) length(i$tip.label) >= 3)]

# # Remove double species names in tree tip labels, fix Tthy names
for(i in 1:length(trees)) {
   trees[[i]]$tip.label <- sub("^\\w{4}_", "",  trees[[i]]$tip.label)
 }
```

Refine orthogroup trees into smaller orthogroup "clans"
=======================================================

``` r
# Run clanFinder R function to generate a list of clans from trees
clans.list <- sapply(trees, clanFinder, c("Gacu", "Olat", "Drer", "Locu", "Mmus", "Hsap"))

# Remove trees with zero clans and unlist
clans <- unlist(clans.list[sapply(clans.list, length) > 0] , recursive = FALSE)

# Rename clan trees
clans.num <- as.numeric(unlist(sapply(clans.list[sapply(clans.list, length) > 0], function(i) 1:length(i))))
names(clans) <- paste(substr(names(clans), 1, 9), paste(clans.num, ".", sep = ""), sep= "_")

# Plot number of clans per tree
clans.per_tree <- table(table(substr(names(clans), 1, 9)))
barplot(clans.per_tree, xlab = "Clans per gene tree")
# Total number of clan gene trees after clanfinder
sum(clans.per_tree * as.numeric(names(clans.per_tree))) 

# Make table of genes in clans
clans.tips <- sub("Tthy2_", "Tthy|Tthy2_", unlist(sapply(clans, function(i) i$tip.label)))
clan_table <- data.frame(OG = sub("_.*", "", names(clans.tips)), 
                                clan = sub("\\..*", "", names(clans.tips)), 
                                protein = sapply(strsplit(clans.tips, "\\|"), "[", 2 ),
                                species = sapply(strsplit(clans.tips, "\\|"), "[", 1 ))

# Make table of all genes in trees
names(trees) <- paste(names(trees), ".", sep="")
tree.tips <- sub("Tthy2_", "Tthy|Tthy2_", unlist(sapply(trees, function(i) i$tip.label)))
tree_table <- data.frame(OG = sub("\\..*", "", names(tree.tips)), 
                         clan = rep(NA, length(tree.tips)), 
                         protein = sapply(strsplit(tree.tips, "\\|"), "[", 2 ),
                         species = sapply(strsplit(tree.tips, "\\|"), "[", 1 ))
head(clan_table)
dim(clan_table)
head(tree_table)
dim(tree_table)


# Number of genes in clans
nrow(clan_table)
# Number of genes in trees
nrow(tree_table)

# Make a lookup table by combining the clan table of genes with tree table of all genes, removing redundant rows
OG_lookup_table <- rbind(clan_table, tree_table)
OG_lookup_table <- OG_lookup_table[!duplicated(OG_lookup_table$protein),]

# number of genes (non-redundant) in combined clans+trees
nrow(OG_lookup_table)

# Check the number of genes with clans in lookup table is the same as the number of genes in the clan table
length(na.omit(OG_lookup_table$clan)) == length(clan_table$protein)

# Check the number of clans in lookup table is the same as the number of clans in the list clans
length(unique(na.omit(OG_lookup_table$clan))) == length(clans)

# Save tree and clan data to RData files
save(trees, clans, file = "OG_and_clan_trees.29.07.18.RData")
save(OG_lookup_table, file = "OG_clan_lookup_table.29.07.18.RData")
```

Orthogroup clan cds alignment
=============================

``` r
# Make clan cds fasta directory
system("mkdir orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_fastas")
clan.cds.dir <- "orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_fastas"

# Make clan cds nt and aa alignment and tree directories
system("mkdir orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_nt_aln")
clan.cds.nt.aln.dir <- "orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_nt_aln"
system("mkdir orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_aa_aln")
clan.cds.aa.aln.dir <- "orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_aa_aln"
system("mkdir orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_nt_trees")
clan.cds.nt.tree.dir <- "orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_nt_trees"
system("mkdir orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_aa_trees")
clan.cds.aa.tree.dir <- "orthogroups.2018/orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_aa_trees"

load("OG_clan_lookup_table.29.07.18.RData")

# Species ids
species_meta <- tbl_df(read.table("species_meta.txt", header = TRUE))
species <- species_meta$identifier

# For each species, add cds seq to clan lookup table
clan.cds_seq <- OG_lookup_table %>% filter(!is.na(clan)) %>% 
  #mutate(protein_id = sub("\\..{1,2}$*", "", protein)) %>% 
  select(clan, species, protein_id = protein)
for (sp in species) {
  # Read in species cds fasta
  print(paste0("Read cds fasta for ", sp))
  cds.fasta <- read.fasta(paste0("cds_fastas/", sp, ".cds.fasta"))
  
  # Make simple table of cds names and sequences
  cds.seq.table <- tbl_df(data.frame(cds_id = names(cds.fasta), 
                                     cds_seq = as.character(unlist(getSequence(cds.fasta, as.string = TRUE)))))
  
  # Add protein ids
  print(paste0("Adding protein ids for ", sp))
  # If ensembl source, join with cds2protein id table
  if (filter(species_meta, identifier == sp)$source == "ensembl") {
    cds2protein.table <- read.table(paste0("cds_fastas/", sp, ".cds2protein_id.table"), header = FALSE, col.names = c("cds_id", "protein_id"))
    cds.seq.table <- cds.seq.table %>% 
      mutate(cds_id = sub("\\..*", "", cds_id)) %>% 
      left_join(cds2protein.table) %>% select(protein_id, cds_seq)
    
  # Else from ncbi, get protein ids from cds names
  } else if (filter(species_meta, identifier == sp)$source == "ncbi") {
    cds.seq.table <- cds.seq.table %>% 
      mutate(protein_id = sub("(\\..)_.*$", "\\1", sub(".*cds_", "", cds_id))) %>% 
      select(protein_id, cds_seq)
    
  # Else if Hhuc, get protein ids from cds names
  } else if (sp == "Hhuc") {
    cds.seq.table <- cds.seq.table %>% mutate(protein_id = sub(":.*", "", cds_id)) %>% select(protein_id, cds_seq)
    
  # Else if Tthy, get protein ids from cds names
  } else if (sp == "Tthy") {
    cds.seq.table <- cds.seq.table %>% mutate(protein_id = cds_id) %>% select(protein_id, cds_seq)
  }
  
  # Join sequences with clan lookup table
  print(paste0("Joining cds seqs for ", sp))
  if (length(clan.cds_seq$cds_seq) > 0) {
    clan.cds_seq <- clan.cds_seq %>% left_join(cds.seq.table, by = "protein_id") %>% 
    mutate(cds_seq = coalesce(cds_seq.x, cds_seq.y)) %>% select(-cds_seq.x, -cds_seq.y)
  } else {
    clan.cds_seq <- clan.cds_seq %>% left_join(cds.seq.table, by = "protein_id")
  }
}

# Save clan lookup table with cds sequences to save time rerunning code
save(clan.cds_seq, file = "clan.cds_seq.RData")
# Load saved clan cds data
load("clan.cds_seq.RData")

# For each clan, write cds fasta file
for (c in clan.cds_seq$clan) {
  # Get cds sequences in clan
  seqs <- clan.cds_seq %>% filter(clan == c, !is.na(cds_seq)) %>% .$cds_seq
  # Turn into vector of characters
  seqs <- sapply(seqs, s2c)
  # Get cds names in clan
  names <- clan.cds_seq %>% filter(clan == c, !is.na(cds_seq)) %>% mutate(name = paste(species, protein_id, sep = "|")) %>% .$name
  # Write cds fasta file
  write.fasta(sequences = seqs, names = names, as.string = FALSE, file.out = paste0(clan.cds.dir, "/", c, ".cds.fa"))
}

# Command to run MACSE abd FastTree script
cmd <- paste("sbatch scripts/run_macse.sh",
             clan.cds.dir,
             clan.cds.nt.aln.dir,
             clan.cds.aa.aln.dir,
             clan.cds.nt.tree.dir,
             clan.cds.aa.tree.dir)
cmd
```

``` r
# Read pal2nal cds alinged trees - nt trees
clan.cds.trees.files <- dir(clan.cds.nt.tree.dir, full.names = TRUE)
clan.cds.trees <- lapply(clan.cds.trees.files, read.tree)
names(clan.cds.trees) <- basename(sub(".cds.fa", "", clan.cds.trees.files))

# Number of trees with 3 tips or greater
table(sapply(clan.cds.trees, function(i) length(i$tip.label) >= 3))
#  TRUE 
# 20734   

# Filter out trees with too few tips
clan.cds.trees <- clan.cds.trees[sapply(clan.cds.trees, function(i) length(i$tip.label) >= 3)]

# Auto root clan trees using Hsap, Mmus, Drer, or Gacu species
clan.cds.trees <- lapply(clan.cds.trees, auto.root)

# Save cds trees
save(clan.cds.trees, file = "clan_cds_trees.20.09.18.RData")
```
