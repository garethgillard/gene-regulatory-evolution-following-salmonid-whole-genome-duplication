#!/bin/sh
#SBATCH --ntasks=1              
#SBATCH --nodes=1               
#SBATCH --job-name=run_fasttree
#SBATCH --cpus-per-task=32
#SBATCH --output=logs/run_fasttree-%A_%a.out

module load fasttree

USAGE="Usgae: sbatch scripts/run_fasttree.sh cds_nt_aln_out_dir cds_aa_aln_out_dir cds_nt_tree_out_dir cds_aa_tree_out_dir"
# sbatch scripts/run_fasttree.sh /mnt/users/garethg/orthogroups.2018/orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_nt_aln /mnt/users/garethg/orthogroups.2018/orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_aa_aln /mnt/users/garethg/orthogroups.2018/orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_nt_trees /mnt/users/garethg/orthogroups.2018/orthofinder/Results_Jul25/Orthologues_Jul25/clan_cds_aa_trees
# If no arguments, print usage and exit
if [ $# -ne 4 ] ; then
  echo $USAGE
  exit
fi

# Set folder arguments
export CDS_NT_ALN="$1"
export CDS_AA_ALN="$2"
export CDS_NT_TREE="$3"
export CDS_AA_TREE="$4"

# Find cds aligment files and run FastTree on both nt and aa alignments
find $CDS_NT_ALN -type f -printf '%f\n' | xargs -n 1 -P 32 sh -c 'FastTree -quiet -nopr -nt "$CDS_NT_ALN/$0" > "$CDS_NT_TREE/$0"'
find $CDS_AA_ALN -type f -printf '%f\n' | xargs -n 1 -P 32 sh -c 'FastTree -quiet -nopr "$CDS_AA_ALN/$0" > "$CDS_AA_TREE/$0'