#!/bin/sh
#SBATCH --ntasks=1             
#SBATCH --nodes=1             
#SBATCH --job-name=orthofinder
#SBATCH --cpus-per-task=32
#SBATCH --output=logs/orthofinder-%A_%a.out

source /mnt/various/profile/cigene.sh
source /mnt/various/profile/modules.sh
#module load orthofinder
module load blast+
module load mcl
module load fastme
#module load dclpar

CMD="/mnt/users/garethg/bin/OrthoFinder-2.2.6/orthofinder -f orthofinder -S diamond -t 32"

echo $CMD
$CMD