#!/bin/sh
#SBATCH --ntasks=1              
#SBATCH --nodes=1               
#SBATCH --job-name=run_macse
#SBATCH --cpus-per-task=32
#SBATCH --output=logs/run_macse-%A_%a.out

module load fasttree

USAGE="Usgae: sbatch scripts/run_macse.sh cds_fasta_dir cds_nt_aln_out_dir cds_aa_aln_out_dir cds_nt_tree_out_dir cds_aa_tree_out_dir"
# sbatch scripts/run_macse.sh orthofinder/Results/align_clans/cds_fasta orthofinder/Results/align_clans/cds_macse_nt_align orthofinder/Results/align_clans/cds_macse_aa_align

# If no arguments, print usage and exit
if [ $# -ne 5 ] ; then
  echo $USAGE
  exit
fi

# Set folder arguments
export CDS_FASTA="$1"
export CDS_NT_ALN="$2"
export CDS_AA_ALN="$3"
export CDS_NT_TREE="$4"
export CDS_AA_TREE="$5"

# Find cds fasta files and run for each macse and FastTree on both nt and aa alignments
find $CDS_FASTA -type f -printf '%f\n' | xargs -n 1 -P 32 sh -c 'java -jar /mnt/users/garethg/bin/macse_v2.01.jar -prog alignSequences -seq "$CDS_FASTA/$0" -out_NT "$CDS_NT_ALN/$0" -out_AA "$CDS_AA_ALN/$0"'
find $CDS_NT_ALN -type f -printf '%f\n' | xargs -n 1 -P 32 sh -c 'FastTree -quiet -nopr -nt "$CDS_NT_ALN/$0" > "$CDS_NT_TREE/$0"'
find $CDS_AA_ALN -type f -printf '%f\n' | xargs -n 1 -P 32 sh -c 'FastTree -quiet -nopr "$CDS_AA_ALN/$0" > "$CDS_AA_TREE/$0'