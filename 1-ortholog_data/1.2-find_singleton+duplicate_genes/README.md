Find singleton and duplicate gene groups
================
Gareth Gillard
15/05/2019

``` r
knitr::opts_chunk$set(eval = TRUE, warning=FALSE, message=FALSE, error=FALSE)
options(stringsAsFactors = FALSE)

# Libraries
library(phangorn)
library(ape)
library(geiger)
library(tidyverse)
library(RColorBrewer)
#library(pheatmap)
library(superheat)
```

``` r
# Auto root a given tree using set out outgroup species
auto.root = function(tree, outgr=c("Hsap", "Mmus", "Locu", "Drer", "Olat", "Gacu")){
  put.root = unlist(sapply(outgr, function(i) grep(i, tree$tip.label)))
  names(put.root) <- sub('[:0-9:]', '', names(put.root))
  
  if(sum(unique(names(put.root)) %in% c('Mmus', 'Hsap'))==2){
    #cat('Two mammal outgroups\n')
    mam.root = grep('Mmus|Hsap', tree$tip.label)
    if(is.monophyletic(tree, mam.root)) { 
      #cat('Monophyletic mammal outgroup\n')
      root.node = getMRCA(tree, mam.root)
      return(root(tree, node =  root.node))
    } else { 
      #cat('Not monophyletic mammal outgroup\n....rooting with human\n')
      return(root(tree, outgroup = grep('Hsap', tree$tip.label)[1], resolve.root = T))
    }
  }
  if(sum(unique(names(put.root)) %in% c('Mmus', 'Hsap'))==1){
    #cat('One mammal outgroup\n')
    return(root(tree, outgroup = put.root[1], resolve.root = T))
  }
  if(sum(unique(names(put.root)) %in% c('Mmus', 'Hsap'))==0){
    #cat('No mammal outgroup\n')
    return(root(tree, outgroup = put.root[1], resolve.root = T))
  }
}

# Function dupFinder
# source('/mnt/users/srsand/Salmon/Scripts_and_workflows/Rscript/Rfunctions/dupFinder120914.r')
dupFinder2 = function(x=OFI_raxml_clans[[3]], add.clade.name=T, species.tag='Ssal') {
    edge_from = x$edge[,1]
    edge_to = x$edge[,2]
    edge_to_label = x$tip.label[edge_to] # tip corresponding to last col in edge matrix (with NA's for internal nodes)
    edge.length = x$edge.length
    Ntip = length(x$tip.label)
    internalNodes = (1:x$Nnode) + Ntip
    
    node <- 18
    
    for (node in internalNodes) {
        descend_row = which(edge_from==node)
        desc = edge_to[descend_row]   # the children of 'node'
        if(length(desc) != 2) next        # dont proceed if not exactly two children (ALLOW HIGHER DEGREES?)
        
        # Leaves belonging to the two clades
        clade1 = sort(tips(x, desc[1]))
        clade2 = sort(tips(x, desc[2]))
        if(length(clade1) < 2 || length(clade2) < 2) next
        
        # Extracting species names (first part of the name, until the first '_' or '.')
        species1 = sapply(strsplit(clade1, '\\|'), '[', 1)
        species2 = sapply(strsplit(clade2, '\\|'), '[', 1) 
        
        # Compute intersection. Proceed if at least two species in common
        inters = intersect(species1, species2)
        if(length(inters) >= 2) {
            
            # Repeated species within each clade
            rep1 = unique(species1[duplicated(species1)])
            rep2 = unique(species2[duplicated(species2)])
            
            # Extract lengths of ancestral branch and Ssa tip branch for clade1 and clade2 (in that order)
            ancestral = edge.length[descend_row]
            Ssa_fullnames = c(clade1[species1==species.tag], clade2[species2==species.tag])
            Ssa_tip = edge.length[match(Ssa_fullnames, edge_to_label)]
            
            if(ancestral[1] >= ancestral[2]) # make sure alpha branch is the one with longest ancestral!
                return(list(splitnode=node, species.in.common=inters, 
                    alpha=clade1, alpha_ancestral=ancestral[1], alpha_Ssa_tip=Ssa_tip[1], repeat.alpha=rep1, 
                    beta=clade2, beta_ancestral=ancestral[2], beta_Ssa_tip=Ssa_tip[2], repeat.beta=rep2))
            else 
                return(list(splitnode=node, species.in.common=inters, 
                    alpha=clade2, alpha_ancestral=ancestral[2], alpha_Ssa_tip=Ssa_tip[2], repeat.alpha=rep2, 
                    beta=clade1, beta_ancestral=ancestral[1], beta_Ssa_tip=Ssa_tip[1], repeat.beta=rep1))
        }
    }   
    return(list())
}
```

Find complete singleton and duplicate gene orthogroups
======================================================

We first removed outgroup species which were not going to be analysed with EVE (Hsap, Mmus, Gacu, Locu).

We then chose to remove two salmonid species (Hhuc, Tthy) that caused, due to poorer quality of their genome assembly, a large reduction in the number of complete 1:1 singleton and 1:2 duplicate (non-salmonids:salmonids) ortholog groups identified. The table below shows the great increase in the duplicate groups when Hhuc and/or Tthy salmonids are removed.

| Salmonids +/- | Singtons | Duplicates |
|:--------------|:---------|:-----------|
| +Hhuc/+Tthy   | 1235     | 784        |
| +Hhuc/-Tthy   | 1518     | 1178       |
| -Hhuc/+Tthy   | 1736     | 1884       |
| -Hhuc/-Tthy   | 2232     | 3083       |

``` r
# Read in clan cds tree data
load("../1.1-make_orthogroups/clan_cds_trees.20.09.18.RData")

# Drop outgroup species not analysed with EVE
# Drop salmonids casing reduced complete singleton+duplicate orthogroups
clan.trees <- lapply(clan.cds.trees, function(i) drop.tip(i, grep("Hsap|Mmus|Gacu|Locu|Hhuc|Tthy", i$tip.label)))
# length(clan.trees) # n=20734

# Remove any now empty trees
clan.trees <- clan.trees[sapply(clan.trees, function(i) length(i$tip.label) > 0)]
# length(clan.trees) # n=20631

# Create clan-protein id table from trees
clans.tips <- unlist(sapply(setNames(clan.trees, paste0(names(clan.trees), "|")), function(i) i$tip.label))
clan.table <- tbl_df(data.frame(clan = sapply(strsplit(names(clans.tips), "\\|"), "[", 1 ),
                                protein = sapply(strsplit(clans.tips, "\\|"), "[", 2 ),
                                species = sapply(strsplit(clans.tips, "\\|"), "[", 1 )))
```

``` r
# Get number of total number of proteins for each species
total.proteins <- system("grep -c '^>' /mnt/users/garethg/orthogroups.2018/orthofinder/*fasta", intern = TRUE) %>%
  sub(".*/", "", .) %>% sub("\\.fasta:", " ", .)

# Load orthogroup and clan lookup table
load("../1.1-make_orthogroups/OG_clan_lookup_table.29.07.18.RData")

# Count the number of proteins per species that are in orthogroups
proteins.in.orthogroups <- OG_lookup_table %>%
  count(species)

# Levels of species, using only the species in the downstream EVE analysis
species.levels <- rev(c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis"))

# Species colours, grey = non-salmonids, orange = salmonids
species.colours <- list(species = c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis"),
                        colours = c("gray20", "gray40", "gray60", "darkorange3", "darkorange2", "darkorange1", "darkorange"))

# Table of protein counts
protein.counts <- tibble(
  species = factor(proteins.in.orthogroups$species, levels = species.levels),
  genesInOGs = proteins.in.orthogroups$n
) %>% left_join(
  tibble(
    species = factor(sub(" .*", "", total.proteins), levels = species.levels),
    genesInTotal = as.integer(sub(".* ", "", total.proteins))
  ), by = "species"
) %>%
  filter(species %in% species.levels)

# Bar plot of protein counts
protein.counts %>%
  ggplot(aes(x = species)) +
  geom_bar(aes(y = genesInTotal, fill = "white", colour = species), stat = "identity") +
  geom_bar(aes(y = genesInOGs, fill = species), stat = "identity") +
  scale_fill_manual(values = species.colours$colours,
                    limits = species.colours$species) +
  scale_colour_manual(values = species.colours$colours,
                    limits = species.colours$species) +
  labs(y = "Number of genes", y = "Species") +
  coord_flip() +
  guides(colour = "none", fill = "none") +
  theme_classic()
```

![](1.2-find_singleton+duplicate_genes_files/figure-markdown_github/plot%20number%20species%20genes%20and%20number%20in%20orthogroups-1.png) Number of proteins in orthogroups (filled bar) vs total proteins (unfilled bar), per species. Non-salmonids are coloured grey while the salmonids are coloured orange.

``` r
# Create species count table
clan.species.counts <- clan.table %>% group_by(clan) %>% dplyr::count(clan, species) %>% spread(species, n)

# Change NA values to 0
clan.species.counts[is.na(clan.species.counts)] <- 0

# Find complete singleton clans, with a single ortholog in all teleost species
single.clans <- clan.species.counts %>% 
  filter_if(is.numeric, all_vars(. == 1))
# length(single.clans$clan) # n=2232

# Find complete duplicate clans, with right ortholog numbers representing single copy in non-salmonids and duplicate in salmonids
duplicate.clans <- clan.species.counts %>% 
  filter_at(vars(one_of(c("Drer", "Olat", "Eluc"))), all_vars(. == 1)) %>% 
  filter_at(vars(one_of(c("Ssal", "Salp", "Omyk", "Okis"))), all_vars(. == 2))
# length(duplicate.clans$clan) # n=3083
```

``` r
# Plot heatmap of species counts within the clans
clan.heatmap <- data.frame(row.names = clan.species.counts$clan, clan.species.counts[, rev(c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis"))])
clan.heatmap[clan.heatmap > 3] <- 3

#png(file = "clan.heatmap.plot.8x4grey3.png", width = 8, height = 4, units = "in", res = 300)
superheat(t(clan.heatmap),
          scale = FALSE, 
          pretty.order.rows = FALSE, 
          pretty.order.cols = TRUE,
          heat.col.scheme = "grey",
          #heat.pal = c("white", "gray80", "gray40", "gray10"),
          heat.pal = c("#FFFFFF", "#B3B3B3", "#4D4D4D", "#000000"),
          #heat.pal.values = c(0, 1, 2, 3),
          heat.lim = c(0, 3),
          legend = TRUE)
```

![](1.2-find_singleton+duplicate_genes_files/figure-markdown_github/heatmap%20of%20species%20counts%20in%20clans-1.png) Heatmap of the number of genes a species has within an orthogroup. White = 0, light grey = 1 (single copy), dark grey = 2 (duplicate copies), black = 3+ gene copies.

Singleton and duplicate gene tables
===================================

``` r
# Get complete singleton clans
single.clan.table <- clan.table %>% 
  filter(clan %in% single.clans$clan) %>% 
  spread(species, protein)
```

``` r
# Use dupFinder to get the duplicate orthogroups that have their duplicate copies in two separate branches
dup.clan.trees <- lapply(clan.trees[duplicate.clans$clan], auto.root)
clan.trees.dupFinder <- lapply(dup.clan.trees, function(i) dupFinder2(i, species.tag = "Ssal"))
n.species.in.common <- unlist(lapply(clan.trees.dupFinder, function(i) length(i$species.in.common)))
# table(n.species.in.common)
# n.species.in.common
#  0    2    3    4 
# 32   90   74 2887 

# Create tables of alpha duplicates and beta duplicates
duplicate.clan.table <- NULL
# Get names of ideal structured clans
ideal.clans <- names(n.species.in.common[n.species.in.common == 4])
# Create rows of ids for each clan, with alpha and beta duplicates labeled
for(clan in ideal.clans) {
  clan.tips <- clan.trees[[clan]]$tip.label
  alpha.tips <- clan.trees.dupFinder[[clan]]$alpha
  beta.tips <- clan.trees.dupFinder[[clan]]$beta
  data <- data.frame(clan = clan,
                     Drer = sub(".*\\|", "", clan.tips[grep("Drer", clan.tips)]),
                     Olat = sub(".*\\|", "", clan.tips[grep("Olat", clan.tips)]),
                     Eluc = sub(".*\\|", "", clan.tips[grep("Eluc", clan.tips)]),
                     Ssal.a = sub(".*\\|", "", alpha.tips[grep("Ssal", alpha.tips)]),
                     Ssal.b = sub(".*\\|", "", beta.tips[grep("Ssal", beta.tips)]),
                     Salp.a = sub(".*\\|", "", alpha.tips[grep("Salp", alpha.tips)]),
                     Salp.b = sub(".*\\|", "", beta.tips[grep("Salp", beta.tips)]),
                     Omyk.a = sub(".*\\|", "", alpha.tips[grep("Omyk", alpha.tips)]),
                     Omyk.b = sub(".*\\|", "", beta.tips[grep("Omyk", beta.tips)]),
                     Okis.a = sub(".*\\|", "", alpha.tips[grep("Okis", alpha.tips)]),
                     Okis.b = sub(".*\\|", "", beta.tips[grep("Okis", beta.tips)]))
  duplicate.clan.table <- rbind(duplicate.clan.table, data)
}

# Create a table of duplicate branch lengths and protein ids
duplicate.branch.table <- tibble(
  orthogroup = names(clan.trees.dupFinder),
  alpha.branch_length = unlist(as.numeric(as.character(map(clan.trees.dupFinder, "alpha_ancestral")))),
  beta.branch_length = unlist(as.numeric(as.character(map(clan.trees.dupFinder, "beta_ancestral")))),
  alpha.protein_ids = sapply(map(clan.trees.dupFinder, "alpha"), function(i) paste(i, collapse = " ")),
  beta.protein_ids = sapply(map(clan.trees.dupFinder, "beta"), function(i) paste(i, collapse = " "))
) %>%
  filter(orthogroup %in% duplicate.clan.table$clan)

# Save duplicate branch table
write.table(duplicate.branch.table, file = "duplicate.branch.table.txt", sep = "\t", quote = FALSE, row.names = FALSE)

# Split duplicate lookup table

# Alpha duplicate clade
dupA.clan.table <- tbl_df(duplicate.clan.table) %>% select(clan, Drer, Olat, Eluc, ends_with(".a"))
names(dupA.clan.table) <- sub("\\.a", "", names(dupA.clan.table))

# Beta duplicate clade
dupB.clan.table <- tbl_df(duplicate.clan.table) %>% select(clan, Drer, Olat, Eluc, ends_with(".b"))
names(dupB.clan.table) <- sub("\\.b", "", names(dupB.clan.table))
```

Phylotree from orthofinder results
==================================

``` r
orthofinder.tree <- read.tree("../1.1-make_orthogroups/orthofinder/Results_Jul25/Orthologues_Jul25/SpeciesTree_rooted_node_labels.txt")
plot(orthofinder.tree)
```

![](1.2-find_singleton+duplicate_genes_files/figure-markdown_github/Orthofinder%20species%20phylotree-1.png)

``` r
# Drop unused species for EVE
EVE.phylo.tree <- drop.tip(orthofinder.tree, grep("Hsap|Mmus|Locu|Gacu|Hhuc|Tthy", orthofinder.tree$tip.label))
EVE.phylo.tree$node.label <- NULL
plot.phylo(EVE.phylo.tree)
```

![](1.2-find_singleton+duplicate_genes_files/figure-markdown_github/Orthofinder%20species%20phylotree-2.png)

``` r
# Save tree as newick
write.tree(EVE.phylo.tree, file = "EVE.phylo.tree.newick.txt")
```

Save data for EVE input
=======================

``` r
save(single.clan.table,
     duplicate.clan.table,
     dupA.clan.table,
     dupB.clan.table,
     file = "EVE.clan.tables.RData")
```
