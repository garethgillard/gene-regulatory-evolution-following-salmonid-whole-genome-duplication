Expression normalisation
================
Gareth Gillard
15/05/2019

``` r
knitr::opts_chunk$set(eval = TRUE, warning=FALSE, message=FALSE, error=FALSE)
options(stringsAsFactors = FALSE)

# Libraries
library(tidyverse)
library(edgeR)
library(gridExtra)
library(ggridges)
library(knitr)

# Set ggplot default theme
theme_set(theme_bw())

# Set levels of species
species.levels <- c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis")

# Set colours of species
species.colours <- list(species = c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis"),
                        colours = c("gray20", "gray40", "gray60", "darkorange3", "darkorange2", "darkorange1", "darkorange"))
```

``` r
# Combines several expression tables using a set of reference genes
makeCommonExprTbl <- function (refGenes, exprs.table) {
  combinedExpr <- refGenes
  for(spc in names(exprs.table)) {
    combinedExpr <- combinedExpr %>% 
      left_join(exprs.table[[spc]], by = setNames(spc, spc))
  }
  return(combinedExpr %>% 
           select(-one_of(names(exprs.table))))
}

# Apply normalisation factors to expression table
applyNormFactors <- function (exprs.table, normFactors) {
  for (sampleID in names(exprs.table)) {
    exprs.table[[sampleID]] <- exprs.table[[sampleID]] %>%
      mutate_if(is.numeric, funs(. / normFactors[sampleID]))
  }
  return(exprs.table)
}

# Calculate normalisation factors using the RLE method
calcNormFactorsRLE <- function (exprs.table) {
  setNames(calcNormFactors.default(object = as.matrix(exprs.table), method = "RLE"), colnames(exprs.table))
}

# Log2 scale a data table
applyLog2Scale <- function (exprs.table, add.count = 0.01) {
  for (sampleID in names(exprs.table)) {
    exprs.table[[sampleID]] <- exprs.table[[sampleID]] %>% 
      mutate_if(is.numeric, funs(log2(. + add.count)))
  }
  return(exprs.table)
}
```

Normalise expression data for within and between species effects on gene expression
===================================================================================

Load tables of singleton and duplicate orthogroups and their gene expression tables
-----------------------------------------------------------------------------------

``` r
# load and convert clan lookup tables
clanTbls <- new.env()
load("../../1-ortholog_data/1.2-find_singleton+duplicate_genes/EVE.clan.tables.RData", envir = clanTbls)
clanTbls <-
  # convert to list and rename
  list( 
    duplicate = clanTbls$duplicate.clan.table,
    single = clanTbls$single.clan.table,
    dupA = clanTbls$dupA.clan.table,
    dupB = clanTbls$dupB.clan.table
  ) %>% 
  # for each table remove protein isoform suffix for Drer and Olat columns
  map( ~ mutate_at( .x, vars(everything()), ~ sub("\\..*", "", .) ))

# Load protein expression data, minus Tthy species
load("../2.2-make_ortholog_expression_tables/EVE.ortholog.exprs.tables.RData")
exprs.tables <- ortholog.exprs.tables

# Select only liver tissue samples
exprs.tables <- lapply(exprs.tables, function (i) tbl_df(i) %>% select(last(colnames(.)), contains("liver")))

# Remove protein isoform suffix
exprs.tables <- exprs.tables %>%
  map( ~ mutate_if( .x, is.character, ~ sub("\\..*", "", .) ))

# Change column names to 'exprs'
exprs.tables <- lapply(exprs.tables, function (i) set_names(i, sub("liver.TPM", "exprs", names(i))))
```

Select the same number of liver samples form each species for analysis
----------------------------------------------------------------------

### Number of liver samples from each species

``` r
data.frame(species = factor(names(exprs.tables), levels = species.levels),
           n.livers = unlist(lapply(exprs.tables, function (i) ncol(i)))) %>%
  ggplot(aes(x = species, y = n.livers, fill = species)) +
  geom_bar(stat = "identity") + 
  guides(fill = "none") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  labs(y = "Total number of liver samples", x = "Species")
```

![](2.3-expression_normalisation_files/figure-markdown_github/number%20of%20samples%20per%20species-1.png)

``` r
# Select 4 samples per species, chosen based on closeness of expression
for (sp in names(exprs.tables)) {
  if (ncol(exprs.tables[[sp]]) > 4) {
    x <- exprs.tables[[sp]] %>%
      select(-sp) %>% cor() %>% apply(2, mean) %>% order(decreasing = TRUE) + 1
    exprs.tables[[sp]] <- exprs.tables[[sp]] %>% 
      select(sp, x[1:4])
  }
}
```

Number of expressed genes at different expression levels
--------------------------------------------------------

``` r
# Number of genes with TPM > cutoff
exprs.cutoffs <- c(0, 1, 10, 100)
data.frame(species = factor(rep(names(exprs.tables), length(exprs.cutoffs)), levels = rev(species.levels)),
           exprs.cutoff = paste("TPM >=", factor(rep(exprs.cutoffs, each = length(exprs.tables)))),
           ngenes = lapply(exprs.cutoffs, function (i) {
             lapply(exprs.tables, function (x) {
               sum(rowMeans(x[, -1]) >= i)
             }) %>% unlist()
           }) %>% unlist()
) %>%
  ggplot(aes(x = species, y = (ngenes/ 1000), fill = species)) +
  geom_bar(stat = "identity", position = "dodge") + 
  facet_grid(~exprs.cutoff, scales = "free") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  coord_flip() +
  guides(fill = "none") +
  labs(title = "Number of genes expressed at a given level", y = "number of genes (x1000)", x = "Species") +
  theme(strip.background = element_blank())
```

![](2.3-expression_normalisation_files/figure-markdown_github/number%20of%20expressed%20genes%20per%20species-1.png)

Normalisation approach
----------------------

**1. Within species normalization (WSN)**

Normalising betwen replicate samples of the sample species, using all gene expression data to calculate normalisation factors (TMM method).

**2. Between species normalization (BSN) using RLE norm. factors**

1.  Select reference genes (singletons, or randomly dupA or dupB)

2.  Compute normalisation factors between species based on reference gene expression (TMM method)

**3. Log2 transform expression data**

### Gene expression distribtion before normalisation

``` r
# Raw expression distribution of all genes
exprs.tables %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>%
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate(sampleID = sub(".exprs", "", sampleID)) %>% 
  mutate(sampleID = factor(sampleID, levels = rev(unique(sampleID)))) %>%
  ggplot(aes( x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc) ) + 
  geom_density_ridges(scale=6) +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  scale_colour_manual(values = c("grey60", "grey20", "grey20", rep("black", 4)), limits = species.colours$species) +
  labs(title = "Gene expression distribution before normalisation", x = "Gene expression log2(TPM+0.01)", y = "Species") +
  guides(fill = "none", colour = "none") +
  facet_grid(spc ~ ., scales = "free_y", switch = "y") +
  coord_cartesian(xlim = c(log2(0.01), 10)) +
  theme(strip.background = element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank())
```

![](2.3-expression_normalisation_files/figure-markdown_github/plot%20raw%20gene%20expression%20distribution-1.png)

1. Within species normalisation (WSN)
-------------------------------------

``` r
# Within species normalisation factors
WSN.factors <-
  # for each exprTbl in exprs.tables
  map( exprs.tables, function(exprTbl){
    tibble( method = c("TMM", "RLE", "upperquartile")) %>% 
      # for each method
      mutate( normFactorTbl = map(
        # make a table with normalisation factors and sampleIDs
        method, ~ tibble( normFactor = 
                            calcNormFactors.default( object = as.matrix(select(exprTbl,contains("exprs"))),
                                                     method = .x, lib.size = rep(1e6,ncol(exprTbl)-1)),
                          sampleID = colnames(select(exprTbl,contains("exprs"))))
        ) ) %>% 
      # unnest tables for each method
      unnest(normFactorTbl)
  }) %>%
  # bind table for each exprTbl
  bind_rows( .id="spc")

# Apply WSN factors, using TMM method
exprs.tables.WSN <- lapply(exprs.tables, function (table) {
  factors <- WSN.factors %>% 
    filter(method == "TMM") %>% 
    filter(sampleID %in% names(table)[-1]) %>% 
    .$normFactor
  for (i in 2:ncol(table)) {
   table[,i] <- table[, i] / factors[i - 1]
  }
  return(table)
})
```

### WSN factors

``` r
WSN.factors %>% 
  filter(method == "TMM") %>%
  mutate(fold.change = round(1/normFactor, 2)) %>%
  kable()
```

| spc  | method |  normFactor| sampleID     |  fold.change|
|:-----|:-------|-----------:|:-------------|------------:|
| Drer | TMM    |   0.8068678| Drer.exprs   |         1.24|
| Drer | TMM    |   0.6410870| Drer.exprs.1 |         1.56|
| Drer | TMM    |   2.5244623| Drer.exprs.2 |         0.40|
| Drer | TMM    |   0.7657936| Drer.exprs.3 |         1.31|
| Olat | TMM    |   1.1753907| Olat.exprs.1 |         0.85|
| Olat | TMM    |   1.1094852| Olat.exprs.2 |         0.90|
| Olat | TMM    |   0.9916629| Olat.exprs   |         1.01|
| Olat | TMM    |   0.7732717| Olat.exprs.3 |         1.29|
| Eluc | TMM    |   1.2961485| Eluc.exprs.2 |         0.77|
| Eluc | TMM    |   1.0665012| Eluc.exprs   |         0.94|
| Eluc | TMM    |   1.1703775| Eluc.exprs.1 |         0.85|
| Eluc | TMM    |   0.6180988| Eluc.exprs.4 |         1.62|
| Ssal | TMM    |   0.9906707| Ssal.exprs.6 |         1.01|
| Ssal | TMM    |   0.9837585| Ssal.exprs.7 |         1.02|
| Ssal | TMM    |   0.9523319| Ssal.exprs.4 |         1.05|
| Ssal | TMM    |   1.0774418| Ssal.exprs.2 |         0.93|
| Salp | TMM    |   0.9771991| Salp.exprs.2 |         1.02|
| Salp | TMM    |   0.8955168| Salp.exprs.1 |         1.12|
| Salp | TMM    |   1.1114879| Salp.exprs   |         0.90|
| Salp | TMM    |   1.0281074| Salp.exprs.3 |         0.97|
| Omyk | TMM    |   0.9456320| Omyk.exprs   |         1.06|
| Omyk | TMM    |   1.0279045| Omyk.exprs.1 |         0.97|
| Omyk | TMM    |   1.0287860| Omyk.exprs.2 |         0.97|
| Okis | TMM    |   0.9893517| Okis.exprs.1 |         1.01|
| Okis | TMM    |   1.0435901| Okis.exprs   |         0.96|
| Okis | TMM    |   0.9887843| Okis.exprs.3 |         1.01|
| Okis | TMM    |   0.9795301| Okis.exprs.2 |         1.02|

### Gene expression distribtion after WSN

``` r
# Plot WSN expression
data1 <- exprs.tables.WSN %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
data2 <- exprs.tables %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
ggplot(data = data1, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc)) + 
  geom_density_ridges(size = 1, scale=6, fill = NA, colour = "red") +
  geom_density_ridges(data = data2, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc), scale = 6, fill = NA, colour = "black") +
  labs(title = "Gene expression distribtion after WSN (red lines)", x = "Gene expression log2(TPM+0.01)", y = "Species") +
  guides(fill = "none", colour = "none") +
  facet_grid(spc ~ ., scales = "free_y", switch = "y") +
  coord_cartesian(xlim = c(log2(0.01), 10)) +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), strip.background = element_blank())
```

![](2.3-expression_normalisation_files/figure-markdown_github/Plot%20WSN%20gene%20expression%20distribution-1.png)

2. Between-species normalisation (BSN)
--------------------------------------

``` r
# Calculate species mean expression levels of genes
exprs.means <- map(exprs.tables.WSN, function (table) {
  means <- table %>% select(contains("exprs")) %>% apply(1, mean)
  table %>% select(-contains("exprs")) %>% mutate(means = means) %>% 
    setNames(., c(colnames(table)[1], paste0(colnames(table)[1], ".exprs.mean")))
})

# Generate reference expression tables
refexprs <- list(
  single = makeCommonExprTbl(clanTbls$single, exprs.means),
  dupAlpha =
    clanTbls$duplicate %>% 
    # remove beta duplicate, i.e. columns ending with ".b"
    select( -ends_with(".b")) %>%
    # remove column .a suffix
    rename_at( vars(ends_with(".a")), ~ sub(".a$","",.)) %>% 
    makeCommonExprTbl(exprs.table = exprs.means),
  dupBeta =
    clanTbls$duplicate %>% 
    # remove beta duplicate, i.e. columns ending with ".a"
    select( -ends_with(".a")) %>%
    # remove column .b suffix
    rename_at( vars(ends_with(".b")), ~ sub(".b$","",.)) %>% 
    makeCommonExprTbl(exprs.table = exprs.means)
)

# Randomly select alpha or beta duplicate
rndAB <- sample(c("a","b"),size = nrow(clanTbls$duplicate),replace = T)
refexprs$dupRnd <- 
  bind_rows(refexprs$dupAlpha[rndAB=="a", ],
            refexprs$dupBeta[rndAB=="b", ])

# Combined list of single + random duplicate
refexprs$combined <- bind_rows(refexprs$single, refexprs$dupRnd)

# There are a few genes (in Drer) with NA in expression table, need to remove these
refexprs <- map(refexprs, na.omit)

# Between species normalization factors
BSN.factors <-
  # for each exprTbl in refexprs.
  map(refexprs, function (exprTbl) {
    tibble( method = c("TMM", "RLE", "upperquartile")) %>% 
      # for each method
      mutate( normFactorTbl = map(
        # make a table with normalisation factors and sampleIDs
        method, ~ tibble( normFactor = 
                            calcNormFactors.default( object = as.matrix(exprTbl[,-1]),
                                                     method = .x, lib.size = rep(1e6,ncol(exprTbl[,-1]))),
                          sampleID = colnames(exprTbl)[-1]) )) %>% 
      # unnest tables for each method
      unnest(normFactorTbl)
  }) %>%
  # bind table for each exprTbl
  bind_rows( .id="refGenes")

# BSN using single orthologs
exprs.tables.BSN.sgl <- lapply(exprs.tables.WSN, function (table) {
  factors <- BSN.factors %>% filter(method == "TMM", refGenes == "single") %>%
    mutate(sampleID = sub("\\..*", "", sampleID)) %>%
    filter(sampleID %in% sub("\\..*", "", names(table)[-1])) %>% .$normFactor
  for (i in 2:ncol(table)) {
   table[,i] <- table[, i] / factors
  }
  return(table)
})

# BSN using combination of single and random dup
exprs.tables.BSN.cmb <- lapply(exprs.tables.WSN, function (table) {
  factors <- BSN.factors %>% filter(method == "TMM", refGenes == "combined") %>%
    mutate(sampleID = sub("\\..*", "", sampleID)) %>%
    filter(sampleID %in% sub("\\..*", "", names(table)[-1])) %>% .$normFactor
  for (i in 2:ncol(table)) {
   table[,i] <- table[, i] / factors
  }
  return(table)
})

# BSN using random dup
exprs.tables.BSN.dpR <- lapply(exprs.tables.WSN, function (table) {
  factors <- BSN.factors %>% filter(method == "TMM", refGenes == "dupRnd") %>%
    mutate(sampleID = sub("\\..*", "", sampleID)) %>%
    filter(sampleID %in% sub("\\..*", "", names(table)[-1])) %>% .$normFactor
  for (i in 2:ncol(table)) {
   table[,i] <- table[, i] / factors
  }
  return(table)
})
```

### BSN factors

``` r
BSN.factors %>% 
  filter(method == "TMM") %>%
  arrange(sampleID) %>%
  mutate(fold.change = round(1/normFactor, 2)) %>%
  kable()
```

| refGenes | method |  normFactor| sampleID        |  fold.change|
|:---------|:-------|-----------:|:----------------|------------:|
| single   | TMM    |   0.9064237| Drer.exprs.mean |         1.10|
| dupAlpha | TMM    |   1.3565782| Drer.exprs.mean |         0.74|
| dupBeta  | TMM    |   1.3712873| Drer.exprs.mean |         0.73|
| dupRnd   | TMM    |   1.3484597| Drer.exprs.mean |         0.74|
| combined | TMM    |   1.1281785| Drer.exprs.mean |         0.89|
| single   | TMM    |   1.0745809| Eluc.exprs.mean |         0.93|
| dupAlpha | TMM    |   1.3754162| Eluc.exprs.mean |         0.73|
| dupBeta  | TMM    |   1.3378978| Eluc.exprs.mean |         0.75|
| dupRnd   | TMM    |   1.3477534| Eluc.exprs.mean |         0.74|
| combined | TMM    |   1.1912177| Eluc.exprs.mean |         0.84|
| single   | TMM    |   1.1007398| Okis.exprs.mean |         0.91|
| dupAlpha | TMM    |   0.8704605| Okis.exprs.mean |         1.15|
| dupBeta  | TMM    |   0.8641351| Okis.exprs.mean |         1.16|
| dupRnd   | TMM    |   0.8636944| Okis.exprs.mean |         1.16|
| combined | TMM    |   0.9754922| Okis.exprs.mean |         1.03|
| single   | TMM    |   0.7857373| Olat.exprs.mean |         1.27|
| dupAlpha | TMM    |   1.1388170| Olat.exprs.mean |         0.88|
| dupBeta  | TMM    |   1.1159520| Olat.exprs.mean |         0.90|
| dupRnd   | TMM    |   1.1559508| Olat.exprs.mean |         0.87|
| combined | TMM    |   0.9876526| Olat.exprs.mean |         1.01|
| single   | TMM    |   1.0162575| Omyk.exprs.mean |         0.98|
| dupAlpha | TMM    |   0.7446155| Omyk.exprs.mean |         1.34|
| dupBeta  | TMM    |   0.7559922| Omyk.exprs.mean |         1.32|
| dupRnd   | TMM    |   0.7526501| Omyk.exprs.mean |         1.33|
| combined | TMM    |   0.8561394| Omyk.exprs.mean |         1.17|
| single   | TMM    |   1.3821452| Salp.exprs.mean |         0.72|
| dupAlpha | TMM    |   1.0429048| Salp.exprs.mean |         0.96|
| dupBeta  | TMM    |   1.0615125| Salp.exprs.mean |         0.94|
| dupRnd   | TMM    |   1.0327511| Salp.exprs.mean |         0.97|
| combined | TMM    |   1.1609740| Salp.exprs.mean |         0.86|
| single   | TMM    |   0.8451041| Ssal.exprs.mean |         1.18|
| dupAlpha | TMM    |   0.6962117| Ssal.exprs.mean |         1.44|
| dupBeta  | TMM    |   0.7043346| Ssal.exprs.mean |         1.42|
| dupRnd   | TMM    |   0.7090280| Ssal.exprs.mean |         1.41|
| combined | TMM    |   0.7770269| Ssal.exprs.mean |         1.29|

### BSN log2 fold change, using single TMM factor

``` r
BSN.factors %>%
  mutate(sampleID = factor(sampleID, levels = rev(unique(sampleID)))) %>%
  mutate(spc=sub("\\..*","",sampleID)) %>% 
  mutate(spc=factor(spc, levels = (species.levels))) %>%
  filter(method == "TMM", refGenes == "single") %>%
  ggplot( aes(x=sampleID, y= log2(1 / normFactor), fill=spc)) +
  geom_bar(stat = "identity") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  guides(fill = "none") +
  coord_flip(ylim = c(-1, 1)) +
  facet_grid(spc ~ ., scale = "free_y", switch = "y") +
  labs(title = "BSN effect on expression", y = "log2 fold change", x = "Species") +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), panel.grid.major.y = element_blank(), panel.grid.minor.y = element_blank(), axis.line.y = element_blank(), strip.background = element_blank()) 
```

![](2.3-expression_normalisation_files/figure-markdown_github/Plot%20BSNsgl%20TMM%20factor%20log2FC-1.png)

### Gene expression distribtion after BSN, using single TMM factor

``` r
data1 <- exprs.tables.BSN.sgl %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
data2 <- exprs.tables.WSN %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
ggplot(data = data1, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc)) +
  geom_density_ridges(size = 1, scale=6, fill = NA, colour = "red") +
  geom_density_ridges(data = data2, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc), scale = 6, alpha = 1, fill = NA, colour = "black") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  scale_colour_manual(values = c("grey60", "grey20", "grey20", rep("black", 4)), limits = species.colours$species) +
  labs(title = "Gene expression distribtion after BSN (red lines)", x = "Gene expression log2(TPM+0.01)", y = "Species") +
  guides(fill = "none", colour = "none") +
  facet_grid(spc ~ ., scales = "free_y", switch = "y") +
  coord_cartesian(xlim = c(log2(0.01), 10)) +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), strip.background = element_blank())
```

![](2.3-expression_normalisation_files/figure-markdown_github/Plot%20BSN%20gene%20expression%20distribution-1.png)

4. Log2 scale tables
--------------------

``` r
# Find minimal count possible from all expression tables
# to add to zero count values to avoid inf log values
min.counts <- list()
for (sp in species.levels) {
  exprsTable <- exprs.tables[[sp]]
  counts <- unlist(exprsTable[, -1])
  counts <- counts[counts != 0]
  min.counts[[sp]] <- min(counts)
}
min.counts <- min(unlist(min.counts)) # 0.01

# Log2 scale tables
exprs.tables.BSN.cmb.log2 <- applyLog2Scale(exprs.tables.BSN.cmb, min.counts)
exprs.tables.BSN.sgl.log2 <- applyLog2Scale(exprs.tables.BSN.sgl, min.counts)
exprs.tables.BSN.dpR.log2 <- applyLog2Scale(exprs.tables.BSN.dpR, min.counts)
exprs.tables.WSN.log2 <- applyLog2Scale(exprs.tables.WSN, min.counts)
```

Create list singleton and duplicate (A and B branches) expression tables, using different normalisation approach
----------------------------------------------------------------------------------------------------------------

``` r
norm.exprs.tables <- list(
  WSN = list(
    single = makeCommonExprTbl(clanTbls$single, exprs.tables.WSN.log2),
    dupA = makeCommonExprTbl(clanTbls$dupA, exprs.tables.WSN.log2),
    dupB = makeCommonExprTbl(clanTbls$dupB, exprs.tables.WSN.log2)
  ),
  BSNcmb = list(
    single = makeCommonExprTbl(clanTbls$single, exprs.tables.BSN.cmb.log2),
    dupA = makeCommonExprTbl(clanTbls$dupA, exprs.tables.BSN.cmb.log2),
    dupB = makeCommonExprTbl(clanTbls$dupB, exprs.tables.BSN.cmb.log2)
  ),
  BSNsgl = list(
    single = makeCommonExprTbl(clanTbls$single, exprs.tables.BSN.sgl.log2),
    dupA = makeCommonExprTbl(clanTbls$dupA, exprs.tables.BSN.sgl.log2),
    dupB = makeCommonExprTbl(clanTbls$dupB, exprs.tables.BSN.sgl.log2)
  ),
  BSNdpR = list(
    single = makeCommonExprTbl(clanTbls$single, exprs.tables.BSN.dpR.log2),
    dupA = makeCommonExprTbl(clanTbls$dupA, exprs.tables.BSN.dpR.log2),
    dupB = makeCommonExprTbl(clanTbls$dupB, exprs.tables.BSN.dpR.log2)
  )
)
```

``` r
# Remove any duplicated clans from tables
norm.exprs.tables <- lapply(norm.exprs.tables, function(i) { 
  lapply(i, function (j) { 
    j[!duplicated(j$clan), ] 
  })
})
```

Save normalised expression tables
---------------------------------

``` r
# Save list of expression tables
save(norm.exprs.tables, file = "EVE.normalised.exprs.tables.08.11.RData")
```

Combined expression normalisation plot
--------------------------------------

``` r
text.size = 8

# Number of genes with TPM > cutoff
exprs.cutoffs <- c(0, 1, 10, 100)
plot.nGenes <- data.frame(species = factor(rep(names(exprs.tables), length(exprs.cutoffs)), levels = rev(species.levels)),
           exprs.cutoff = paste("TPM >=", factor(rep(exprs.cutoffs, each = length(exprs.tables)))),
           ngenes = lapply(exprs.cutoffs, function (i) {
             lapply(exprs.tables, function (x) {
               sum(rowMeans(x[, -1]) >= i)
              }) %>% unlist()
             }) %>% unlist()
           ) %>%
  ggplot(aes(x = species, y = (ngenes/ 1000), fill = species)) +
  geom_bar(stat = "identity", position = "dodge") + 
  facet_grid(~exprs.cutoff, scales = "free") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  coord_flip() +
  guides(fill = "none") +
  labs(title = "Number of genes expressed at a given level", y = "number of genes (x1000)", x = "Species") +
  theme(text = element_text(size = text.size), strip.background = element_blank())

# Raw expression distribution of all genes
plot.raw <- exprs.tables %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>%
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate(sampleID = sub(".exprs", "", sampleID)) %>% 
  mutate(sampleID = factor(sampleID, levels = rev(unique(sampleID)))) %>%
  ggplot(aes( x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc) ) + 
  geom_density_ridges(scale=6) +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  scale_colour_manual(values = c("grey60", "grey20", "grey20", rep("black", 4)), limits = species.colours$species) +
  labs(title = "Raw TPM", x = "Gene expression log2(TPM+0.01)", y = "Sample") +
  guides(fill = "none", colour = "none") +
  facet_grid(spc ~ ., scales = "free_y") +
  coord_cartesian(xlim = c(log2(0.01), 10)) +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), axis.line.y = element_blank(), axis.title.y = element_blank(), text = element_text(size = text.size), strip.background = element_blank(), strip.text = element_blank())

# Plot WSN expression
data1 <- exprs.tables.WSN %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
data2 <- exprs.tables %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
plot.WSN <- 
  ggplot(data = data1, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc)) + 
  geom_density_ridges(scale=6, fill = NA, colour = "red") +
  geom_density_ridges(data = data2, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc), scale = 6, fill = NA, colour = "black") +
  #scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  #scale_colour_manual(values = c("grey60", "grey20", "grey20", rep("black", 4)), limits = species.colours$species) +
  labs(title = "WSN TPM (red)", x = "Gene expression log2(TPM+0.01)", y = "Sample") +
  guides(fill = "none", colour = "none") +
  facet_grid(spc ~ ., scales = "free_y") +
  coord_cartesian(xlim = c(log2(0.01), 10)) +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), axis.line.y = element_blank(), axis.title.y = element_blank(), text = element_text(size = text.size), strip.background = element_blank(), strip.text = element_blank())

# Plot factors
plot.factors <- BSN.factors %>%
  mutate(sampleID = factor(sampleID, levels = rev(unique(sampleID)))) %>%
  mutate(spc=sub("\\..*","",sampleID)) %>% 
  mutate(spc=factor(spc, levels = (species.levels))) %>%
  filter(method == "TMM", refGenes == "single") %>%
  ggplot( aes(x=sampleID, y= log2(1 / normFactor), fill=spc)) +
  geom_bar(stat = "identity") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  guides(fill = "none") +
  coord_flip(ylim = c(-1, 1)) +
  facet_grid(spc ~ ., scale = "free_y") +
  labs(title = "BSN effect on expression", y = "log2 fold change") +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), panel.grid.major.y = element_blank(), panel.grid.minor.y = element_blank(), axis.line.y = element_blank(), axis.title.y = element_blank(), text = element_text(size = text.size), strip.background = element_blank(), strip.text = element_blank()) 

# Plot BSN expression
data1 <- exprs.tables.BSN.sgl %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
data2 <- exprs.tables.WSN %>% 
  map( ~ .x %>% select( contains("exprs")) %>% gather( key="sampleID", value="exprs.") ) %>% 
  bind_rows(.id = "spc") %>% 
  mutate(spc = factor(spc, levels = species.levels)) %>%
  mutate( sampleID = sub(".exprs", "", sampleID)) %>%
  mutate(sampleID = factor(sampleID, levels = unique(sampleID)))
plot.BSN.sgl <- 
  ggplot(data = data1, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc)) + 
  geom_density_ridges(scale=6, fill = NA, colour = "red") +
  geom_density_ridges(data = data2, aes(x = log2(exprs. + 0.01), y = sampleID, fill = spc, colour = spc), scale = 6, alpha = 1, fill = NA, colour = "black") +
  scale_fill_manual(values = species.colours$colours, limits = species.colours$species) +
  scale_colour_manual(values = c("grey60", "grey20", "grey20", rep("black", 4)), limits = species.colours$species) +
  labs(title = "BSN TPM (red)", x = "Gene expression log2(TPM+0.01)", y = "Sample") +
  guides(fill = "none", colour = "none") +
  facet_grid(spc ~ ., scales = "free_y") +
  coord_cartesian(xlim = c(log2(0.01), 10)) +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), axis.line.y = element_blank(), axis.title.y = element_blank(), text = element_text(size = text.size), strip.background = element_blank(), strip.text = element_blank())

# Combined expression normalization plot
grid.arrange(plot.nGenes, plot.raw, plot.WSN, plot.factors, plot.BSN.sgl, 
            layout_matrix = matrix(c(1, 2, 1, 3, 1, 4, 1, 5), nrow = 2), heights = 1:2)
```

![](2.3-expression_normalisation_files/figure-markdown_github/Combined%20expression%20normalisation%20plot-1.png)
