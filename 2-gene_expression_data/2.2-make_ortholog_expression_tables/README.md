Make ortholog expression tables
================
Gareth Gillard
15/05/2019

``` r
knitr::opts_chunk$set(eval = TRUE, warning=FALSE, message=FALSE, error=FALSE)
options(stringsAsFactors = FALSE)

# Libraries
library(tidyverse)
```

``` r
# Set species names
species <- c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis")

# List of protein expression tables
ortholog.exprs.tables <- list()

# For each species make expression table
for (sp in species) {
  
  # Set expression type to TPM counts
  exprs.type <- "TPM"
  
  # Read in gene tissue expression and sample meta data
  exprs.dir <- paste0("../2.1-make_gene_expression_data/read_mapping/", sp)
  gene.exprs <- tbl_df(read.table(file = dir(path = exprs.dir, pattern = exprs.type, full.names = TRUE), header = TRUE, check.names = FALSE))
  sample.meta <- tbl_df(read.table(file = dir(path = exprs.dir, pattern = "meta", full.names = TRUE), header = TRUE))
  
  # Gene <-> protein linkage table
  gene2protein <- tbl_df(read.table(paste0("../../1-ortholog_data/1.1-make_orthogroups/species_gene2protein_tables/", sp ,".gene2protein.txt"), header = TRUE))
  
  # Create protein expression table
  protein.exprs <- gene.exprs %>%
    # Mutate gene_id to match gene2protein linkage table
    mutate(gene_id = sub("gene:", "", gene_id)) %>%
    # Join protein ids with gene ids
    inner_join(gene2protein, by = "gene_id") %>%
    # Remove gene_id column
    select(-gene_id) %>%
    # Rename proteins
    mutate(protein = sub("\\.\\d+$", "", protein))
  # Rename columns
  names(protein.exprs) <- c(make.unique(paste(sp, sample.meta$tissue, exprs.type, sep = ".")), sp)
  
  # Add expression table to the list
  ortholog.exprs.tables[[sp]] <- protein.exprs
}

save(ortholog.exprs.tables, file = "EVE.ortholog.exprs.tables.RData")
```
