---
title: "Read mapping for tissue data from Salp"
author: "Gareth Gillard"
date: "11/08/2018"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup_dir_and_libraries}
library(tidyr)
library(dplyr)
#library(edgeR)

# Set working directory for processing data
setwd("/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/")

# Get genome fasta and annotation gff files
setwd("genome_refs")
system("wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/910/315/GCF_002910315.2_ASM291031v2/GCF_002910315.2_ASM291031v2_genomic.fna.gz")
system("wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/910/315/GCF_002910315.2_ASM291031v2/GCF_002910315.2_ASM291031v2_genomic.gff.gz")
system("gunzip *")
# Filter gff for gene feature of RSEM (gene, mRNA, exon, CDS)
system("grep -E '\tgene|mRNA|exon|CDS|pseudogene\t' /mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/genome_refs/GCF_002910315.2_ASM291031v2_genomic.gff > /mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/genome_refs/GCF_002910315.2_ASM291031v2_genomic.filtered.gff")
setwd("/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/")

genome_fasta <- "/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/genome_refs/GCF_002910315.2_ASM291031v2_genomic.fna"
gff3 <- "/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/genome_refs/GCF_002910315.2_ASM291031v2_genomic.filtered.gff"

```


```{r RNA_fastq_setup}
system("mkdir data; mkdir data/fastq; mkdir data/fastq_trim; mkdir data/bash; mkdir data/slurm; mkdir data/RSEM; mkdir data/RSEM/index")

# Get tissue sample directories
sample.files <- dir(path = "/mnt/SeqData1/salmon/Koop-RNAseq-4-livers_201603", 
                      full.names = TRUE, pattern = "Salp.*fastq\\.gz", recursive = TRUE)
sample.names <- unique(sub("_R.\\.fastq\\.gz", "", basename(sample.files)))

# For each sample directory, cat together separated fastq.gz files from the same sample as a single file under a data folder
for (i in 1:length(sample.names)) {
  # cat R1 reads
  sample.name <- paste0(sample.names[i], "_R1_001.fastq.gz")
  cat("Copying sample", sample.name, "R1 reads to data folder\n")
  system(paste0("cat /mnt/SeqData1/salmon/Koop-RNAseq-4-livers_201603/", sample.names[i], "_R1.fastq.gz > ", "data/fastq/", sample.name))
  # cat R2 reads
  sample.name <- paste0(sample.names[i], "_R2_001.fastq.gz")
  cat("Copying sample", sample.name, "R2 reads to data folder\n")
  system(paste0("cat /mnt/SeqData1/salmon/Koop-RNAseq-4-livers_201603/", sample.names[i], "_R2.fastq.gz > ", "data/fastq/", sample.name))
}

# Create RnaMapping description file
cat("Organism: Salvelinus alpinus
Tissues: liver
Date: 2018-08-11, Gareth Gillard
Experiment: Artic char tissue panel for expression evolution project", file = "data/description.txt")

# Create a RnaMapping master file
samples <- dir(paste("data", "/fastq", sep = ""), pattern = "fastq.gz")
samples.txt <- data.frame("sample" = sapply(samples, function(i) sub("_R.\\.fastq.gz", "", i)), 
                          "base" = sapply(samples, function(i) sub("_R.\\.fastq.gz", "", i)))
samples.txt <- distinct(samples.txt)
write.table(samples.txt, paste0("data", "/samples.txt"), quote = FALSE, row.names = FALSE, col.names = TRUE)

# Write script for trimming fastq files
N <- nrow(samples.txt)
cat(
"#!/bin/sh
#SBATCH --ntasks=1",
paste0("\n#SBATCH --array=1-", N),
"#SBATCH --job-name=TRIMMER
#SBATCH --output=slurm/trim-%A_%a.out
  
module load anaconda
module list
date
  
FILEBASE=$(awk ' NR=='$SLURM_ARRAY_TASK_ID+1' { print $2 ; }' /mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/data/samples.txt)

R1=/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/data/fastq'/'$FILEBASE'_R1_001.fastq.gz'
O1='fastq_trim/'$FILEBASE'_R1_001.trim.fastq.gz'
R2=/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/data/fastq'/'$FILEBASE'_R2_001.fastq.gz'
O2='fastq_trim/'$FILEBASE'_R2_001.trim.fastq.gz'

#------READ1---------------------
adaptorR1=$(awk ' NR=='$SLURM_ARRAY_TASK_ID+1' { print $4 ; }' /mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/data/samples.txt)
adaptorR1='GATCGGAAGAGCACACGTCTGAACTCCAGTCAC'$adaptorR1'ATCTCGTATGCCGTCTTCTGCTTG'

#------READ2---------------------
adaptorR2='AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'

# Check if there are two paired read files, or one single end file
if [ -f \"$R2\" ]; then
    # Run for paired reads
    cutadapt -q 20 -O 8 --minimum-length 40 -a $adaptorR1 -A $adaptorR2 -o $O1 -p $O2 $R1 $R2
else
    # Run for single reads
    cutadapt -q 20 -O 8 --minimum-length 40 -a $adaptorR1 -o $O1 $R1
fi
", file = "data/bash/sbatch-trim.sh")
```

```{r RSEM_counting}
# Create script for preparing RSEM reference for read mapping
N <- nrow(samples.txt)
cat(
"#!/bin/sh
#SBATCH -n 1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=RSEM_setup
#SBATCH --output=slurm/RSEM_setup-%A_%a.out

module load samtools anaconda star
\n",
paste("/mnt/users/garethg/bin/RSEM-1.2.31/rsem-prepare-reference --star -p 8 --gff3", gff3, genome_fasta, "RSEM/index/Salp_RSEM_ref"), file = "data/bash/sbatch_RSEM_setup.sh")

# Create script for RSEM mapping and counting
cat(
"#!/bin/sh
#SBATCH -n 1
#SBATCH --cpus-per-task=8",
paste0("\n#SBATCH --array=1-", N),
"\n#SBATCH --job-name=RSEM
#SBATCH --output=slurm/RSEM-%A_%a.out

module load samtools anaconda star
module list
date

FILEBASE=$(awk ' NR=='$SLURM_ARRAY_TASK_ID+1' { print $2 ; }' samples.txt )
R1=fastq_trim/$FILEBASE'_R1_001.trim.fastq.gz'
R2=fastq_trim/$FILEBASE'_R2_001.trim.fastq.gz'
OUT='RSEM/'$FILEBASE

if [ -f $R2 ]; then
  /mnt/users/garethg/bin/RSEM-1.2.31/rsem-calculate-expression --star --star-gzipped-read-file --calc-ci -p 8 --time --paired-end $R1 $R2 RSEM/index/Tthy_RSEM_ref $OUT
else
  /mnt/users/garethg/bin/RSEM-1.2.31/rsem-calculate-expression --star --star-gzipped-read-file --calc-ci -p 8 --time $R1 RSEM/index/Salp_RSEM_ref $OUT
fi

echo $OUT 'FINISHED'", file = "data/bash/sbatch_RSEM.sh")
```

```{r Create_combined_sample_count_tables}
# RSEM count table
RSEM.files <- list.files("/mnt/SeqData3/GenoSysFat/processed_tissue_panel_data/Salp/data/RSEM", pattern = ".*genes.results", full.names = TRUE)
RSEM.files <- RSEM.files[order(as.numeric(sub("-.*", "", basename(RSEM.files))))]
sample.names <- sub(".genes.*", "", basename(RSEM.files))

# Raw counts
RSEM.counts <- tbl_df(read.table(RSEM.files[1], header = TRUE, sep = "\t")) %>% select(gene_id, effective_length, expected_count) %>% rename_(.dots=setNames("expected_count", sample.names[1]))
for (i in 2:length(RSEM.files)) {
  RSEM.counts <- left_join(RSEM.counts, tbl_df(read.table(RSEM.files[i], header = TRUE, sep = "\t")) %>% select(gene_id, expected_count) %>% rename_(.dots=setNames("expected_count", sample.names[i])))
}

# FPKM
RSEM.FPKM.counts <- tbl_df(read.table(RSEM.files[1], header = TRUE, sep = "\t")) %>% select(gene_id, FPKM) %>% rename_(.dots=setNames("FPKM", sample.names[1]))
for (i in 2:length(RSEM.files)) {
  RSEM.FPKM.counts <- left_join(RSEM.FPKM.counts, tbl_df(read.table(RSEM.files[i], header = TRUE, sep = "\t")) %>% select(gene_id, FPKM) %>% rename_(.dots=setNames("FPKM", sample.names[i])))
}

# TPM values
RSEM.TPM.counts <- tbl_df(read.table(RSEM.files[1], header = TRUE, sep = "\t")) %>% select(gene_id, TPM) %>% rename_(.dots=setNames("TPM", sample.names[1]))
for (i in 2:length(RSEM.files)) {
  RSEM.TPM.counts <- left_join(RSEM.TPM.counts, tbl_df(read.table(RSEM.files[i], header = TRUE, sep = "\t")) %>% select(gene_id, TPM) %>% rename_(.dots=setNames("TPM", sample.names[i])))
}

# Save count tables
write.table(select(RSEM.counts, -effective_length), "Salp_tissue.counts.txt", row.names = FALSE, quote = FALSE, sep = "\t")
write.table(RSEM.FPKM.counts, "Salp_tissue.FPKM.txt", row.names = FALSE, quote = FALSE, sep = "\t")
write.table(RSEM.TPM.counts, file = "Salp_tissue.TPM.txt", row.names = FALSE, quote = FALSE, sep = "\t")

# Write sample meta table
sample.meta <- data.frame(sample = sample.names, species = "Salp", tissue.short = "L", tissue = "liver")
write.table(sample.meta, "Salp_sample_meta.txt", row.names = FALSE, quote = FALSE, sep = "\t")
```
