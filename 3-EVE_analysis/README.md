Run EVE analysis
================
Gareth Gillard
18/05/2019

``` r
knitr::opts_chunk$set(eval = FALSE, warning=FALSE, message=FALSE, error=FALSE)
options(stringsAsFactors = FALSE)

# Libraries
library(tidyverse)
library(ape)
```

Curate input data for EVE analysis
==================================

Test using within-species normlized (WSN) TPM, and several between-species normalizated (BSN) TPM expression values. BSN aproaches used a combined table of single and random duplicate genes as reference for normalization (cmb), only single genes (sgl), or only random duplicate (dpR). All tables have had WSN and log2(x+0.01) transformed.

Test single genes, duplicate gene alpha, and beta (separate tests for each duplicate).

Test for shift anywhere in tree (any variance), on the salmonid branch (Ss4R), and on the non-salmonid out group branches (e.g. Eluc)

Load saved data for EVE
-----------------------

``` r
# Expression tables
load("../2-gene_expression_data/2.3-expression_normalisation/EVE.normalised.exprs.tables.08.11.RData")

# Newick phylogeny tree from orthofinder
EVE.phylo.tree.newick <- read.tree("../1-ortholog_data/1.2-find_singleton+duplicate_genes/EVE.phylo.tree.newick.txt")
```

Input variables for EVE run
---------------------------

``` r
date <- "November"
data.types <- names(norm.exprs.tables)
gene.types <- names(norm.exprs.tables[[1]])
species <- c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis")
non.salmonids <- c("Drer", "Olat", "Eluc")
salmonids <- c("Ssal", "Salp", "Omyk", "Okis")
```

Functions to write EVE files
----------------------------

``` r
# Newick tree with species codes
treeForEVE <- function (tree, species) {
  for (sp in species) {
    tree <- sub(sp, which(species == sp), tree)
  }
  return(tree)
}

# Make expresssion table for EVE
exprsTableForEVE <- function (table, species, test = FALSE) {
  
     # Remove rows with NA counts
    table <- table[!(rowSums(is.na(table)) > 0), ]
    
    # Remove rows with a standard deviation of 0
    table <- table[apply(table[, -1], 1, sd) != 0, ]
    
    # Replace species names with codes
    for (sp in species) {
      names(table) <- sub(paste0(sp, ".*"), which(species == sp), names(table))
    }
    
    # If test, return a subtable
    if (test == TRUE) {
      table <- table[1:10, ]
    }
  
    return(table)
}

# Test command for EVE
getTestCommand <- function (test, species) {
  command <- NULL
  if (test == "Var") {
    command <- "-S"
  }
  if (test == "Ss4R") {
    command <- paste("-O", paste("-o", sapply(salmonids, function (i) which(i == species) - 1), collapse = " "))
  }
  if (test %in% species) {
    command <- paste("-O -o", which(species == test) - 1)
  }
  return (command)
}

# Write data to EVE folder
writeEVEData <- function (common.path, tree, table, select.species, tests) {
  
  # Folder paths for files
  folders <- list(
  # Folder for data
  data.folder = paste("EVE/data", common.path, sep = "/"),
  # Folder for results
  results.folder =  paste("EVE/results", common.path, sep = "/"),
  # Folder for scripts
  scripts.folder = paste("EVE/scripts", common.path, sep = "/"),
  # Folder for logs
  logs.folder = paste("EVE/logs", common.path, sep = "/")
  )
  # Create folders if they don't exist
  for (folder in folders) {
    system(paste0("[ ! -d ", folder, " ] && mkdir -p ", folder))
  }
  
  # Write tree
  cat(paste0(length(select.species), "\n", treeForEVE(tree, select.species)), 
      file = paste(folders$data.folder, "phylo.tree", sep = "/"))
  
  # Write expression table
  table <- exprsTableForEVE(table, select.species)
  cat(paste0(nrow(table), "\n"), 
      file = paste(folders$data.folder, "expression.dat", sep = "/"))
  write.table(table, 
              file = paste(folders$data.folder, "expression.dat", sep = "/"), 
              row.names = FALSE, col.names = FALSE, quote = FALSE, append = TRUE)
  
  # Write number of species replicates in expression table
  cat(names(exprsTableForEVE(table, select.species))[-1] %>% table() %>% as.vector(),
      file = paste(folders$data.folder, "expression.nindiv", sep = "/"))
  
  # Write run script for each test
  for (test in tests) {
    
    # Inputs into script command
    n <- nrow(table)
    data.file <- sub("EVE/", "", paste(folders$data.folder, "expression.dat", sep = "/"))
    nindiv.file <- sub("EVE/", "", paste(folders$data.folder, "expression.nindiv", sep = "/"))
    tree.file <- sub("EVE/", "", paste(folders$data.folder, "phylo.tree", sep = "/"))
    EVE.results.folder <- paste0(sub("EVE/results", "", folders$results.folder), "/")
    output.name <- paste0("test", test)
    test.command <- getTestCommand(test, select.species)
    log.file <- sub("EVE/", "", paste(folders$logs.folder, paste0(output.name, "-%A_%a.log"), sep = "/"))
    script.file <- paste0(paste(folders$scripts.folder, paste0(test.index, "_", output.name), sep = "/"), ".sh")
    job.name <- paste0(test.index, "_", test)
    
# Script
cat(
"#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=", job.name, "
#SBATCH --cpus-per-task=1
#SBATCH --output=", log.file, "

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel ", 
test.command, 
" -n ",  n, 
" -t ", tree.file, 
" -i ", nindiv.file, 
" -d ", data.file, 
" -f .", output.name, 
" -p ", EVE.results.folder, 
" -v 10",
sep = "", 
file = script.file)

   
  }
}
```

List of EVE tests to run for different data types and tree structures
---------------------------------------------------------------------

``` r
test.list <-  c(
  paste("WSN", c(
    "single Var:Ss4R",
    "dupA Var:Ss4R",
    "dupB Var:Ss4R"
  )), 
  
  paste("BSNcmb", c(
    "single Var:Ss4R",
    "dupA Var:Ss4R",
    "dupB Var:Ss4R"
  )),
  
  paste("BSNdpR", c(
    "single Var:Ss4R",
    "dupA Var Ss4R",
    "dupB Var:Ss4R"
  )),
  
  paste("BSNsgl", c(
    "single Var:Ss4R:Eluc:Ssal:Salp:Omyk:Okis",
    "dupA Var:Ss4R:Eluc:Ssal:Salp:Omyk:Okis",
    "dupB Var:Ss4R:Eluc:Ssal:Salp:Omyk:Okis",
    
    "single.Sal Var",
    "dupA.Sal Var",
    "dupB.Sal Var",
    
    "single.Tel Var",
    "dupA.Tel Var",
    "dupB.Tel Var",
    
    "single.Ssal Drer:Olat:Eluc:Ssal",
    "dupA.Ssal Drer:Olat:Eluc:Ssal",
    "dupB.Ssal Drer:Olat:Eluc:Ssal",
    
    "single.Salp Salp",
    "dupA.Salp Salp",
    "dupB.Salp Salp",
    
    "single.Omyk Omyk",
    "dupA.Omyk Omyk",
    "dupB.Omyk Omyk",
    
    "single.Okis Okis",
    "dupA.Okis Okis",
    "dupB.Okis Okis"
  ))
)
```

EVE tests for expression shifts
===============================

``` r
# Delete files from previous run attempt
# system("rm -fr EVE/logs/November;
#         rm -fr EVE/data/November;
#         rm -fr EVE/scripts/November;
#         rm -fr EVE/results/November")
```

Write the EVE input files and the run scripts
---------------------------------------------

``` r
test.index <- 1
for (i in test.list) {
  # Input data variables
  var <- test.list[1]
  var <- strsplit(i, " ") %>% unlist() %>% setNames(c("data.type", "gene.type", "tests")) %>% as.list()
  var$tests <- strsplit(var$tests, ":") %>% unlist()
  
  # Select species for analysis
  if (var$gene.type %in% c("single", "dupA", "dupB")) {
    select.species <- species
    
  } else if (grepl("Sal", var$gene.type)) {
    select.species <- salmonids
    
  } else if (grepl("Tel", var$gene.type)) {
    select.species <- non.salmonids
    
  } else if (grepl("Ssal", var$gene.type)) {
    select.species <- c(non.salmonids, "Ssal")
    
  } else if (grepl("Salp", var$gene.type)) {
    select.species <- c(non.salmonids, "Salp")
    
  } else if (grepl("Omyk", var$gene.type)) {
    select.species <- c(non.salmonids, "Salp")
    
  } else if (grepl("Okis", var$gene.type)) {
    select.species <- c(non.salmonids, "Okis")
  }
  
  # Tree data
  if (all(species %in% select.species)) {
    tree <- EVE.phylo.tree.newick
  } else {
    tree <- EVE.phylo.tree.newick %>% 
      read.tree(text = .) %>%
      drop.tip(species[! species %in% select.species]) %>%
      write.tree()
  }
  
  # Gene expression data
  if (all(species %in% select.species)) {
    table <- norm.exprs.tables[[var$data.type]][[var$gene.type]]
  } else {
    table <- norm.exprs.tables[[var$data.type]][[sub("\\..*", "", var$gene.type)]] %>%
      select(clan, matches(paste(select.species, collapse = "|")))
  }
  
  # Comman path
  common.path <- paste(date, var$data.type, var$gene.type, sep = "/")
  
  # Write expression data for EVE
  writeEVEData(
    # Common folder path to write to
    common.path = common.path,
    # Phylogenetic tree
    tree = tree,
    # Gene expression data
    table = table,
    # List of species names
    select.species = select.species,
    # List of tests
    tests = var$tests
  )
  test.index <- test.index + 1
}
```

Run EVE scripts
---------------

``` r
# Get a list of EVE scripts
scripts.list <- dir(paste0("EVE/scripts/", date), pattern = "\\.sh", recursive = TRUE)
scripts.list <- scripts.list[order(as.numeric(sub("_.*", "", basename(scripts.list))))]
scripts.list <- paste0("sbatch scripts/", date, "/", scripts.list)

# Save script run commands to bash file
cat("#!/bin/sh", scripts.list, file = paste0("EVE/run.EVE.scripts.", date, ".sh"), sep = "\n")
```
