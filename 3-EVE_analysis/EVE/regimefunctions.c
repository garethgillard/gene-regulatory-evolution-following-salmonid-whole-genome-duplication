#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include"mystat.h"
#include"myio.h"

//node definition
struct node {
  int down;
  int up[2];
  double bl;
  int regime;
  int nindiv;
  int level;
};

//global vars
struct node *tree; //phylogeny
double **expr; // expression data: expr[gene number][individual number]  
int ngenes; //total number of genes in dataset
int nspecies; //total number of species in dataset 
int totnindiv; //total number of individuals in all species
int findmaxgenei; //gene index considered in findmax
double findmaxbeta; //var for fixed beta val to be used in findmax

/////////////////////////////////////////////////////////////////////////
// malloc, setting, and freeing regimes

void setStableRegime(double ***regimes, int rooti) {
  int i;

  //set up regimes for single regime
  regimes[0] = (double**) malloc(1*sizeof(double*));
  regimes[0][0] = (double*) malloc(4*sizeof(double));
  regimes[0][0][0] = 100.0;
  regimes[0][0][1] = 10.0;
  regimes[0][0][2] = 3.0;
  regimes[0][0][3] = 2.0;


  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;
}

void freeStableRegime(double **regimes) {
  free(regimes[0]);
  free(regimes);
}

void setBranchRegRecurse(int nodei, int reg) {
  tree[nodei].regime = reg;

  if(tree[nodei].up[0] > -1) { //internal node
    setBranchRegRecurse(tree[nodei].up[0], reg);
    setBranchRegRecurse(tree[nodei].up[1], reg);
  }
}

void setShiftRegime(double ***regimes, int rooti) {
  int i;

  //setup regimes for shift regime
  regimes[0] = (double**) malloc(2*sizeof(double*));
  for(i=0; i<2; i++) 
    regimes[0][i] = (double*) malloc(4*sizeof(double));
  regimes[0][0][0] = 100.0;
  regimes[0][0][1] = 10.0;
  regimes[0][0][2] = .10;
  regimes[0][0][3] = 2.0;
  regimes[0][1][0] = 120.0;
  regimes[0][1][1] = 10.0;
  regimes[0][1][2] = .10;
  regimes[0][1][3] = 2.0;

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;

  //set branch to reg1
  setBranchRegRecurse(13, 1);
}

void freeShiftRegime(double **regimes) {
  int j;
  for(j=0; j<2; j++) 
    free(regimes[j]);
  free(regimes);
}

void setShiftRegimeEG(double ****regimes, int rooti, int nregimes) {
  int i, j;

  //setup regimes for shift regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(nregimes*sizeof(double));
    for(j=0; j<nregimes; j++) {
      regimes[0][i][j] = (double*) malloc(4*sizeof(double));
    }
    regimes[0][i][0][0] = 100.0;
    regimes[0][i][0][1] = 1.0*i +.01;
    regimes[0][i][0][2] = .10;
    regimes[0][i][0][3] = 2.0;
    regimes[0][i][1][0] = 120.0;
    regimes[0][i][1][1] = 1.0*i+.01;
    regimes[0][i][1][2] = .10;
    regimes[0][i][1][3] = 2.0;
  }

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;

  //set branch to reg1
  //setBranchRegRecurse(13, 1);
}

void setStableRegimeEG(double ****regimes, int rooti) {
  int i, j;

  //setup regimes for shift regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(1*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));

    regimes[0][i][0][0] = 100.0;
    regimes[0][i][0][1] = 10.0;//10.0;//1.0*i +.01;
    regimes[0][i][0][2] = .10;
    regimes[0][i][0][3] = 2.0;
  }

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;
}

void freeStableRegimeEG(double ***regimes) {
  int i;
  for(i=0; i<ngenes; i++) {
    free(regimes[i][0]);
    free(regimes[i]);
  }
  free(regimes);
}


void freeShiftRegimeEG(double ***regimes) {
  int i;
  for(i=0; i<ngenes; i++) {
    free(regimes[i][0]);
    free(regimes[i][1]);
    free(regimes[i]);
  }
  free(regimes);
}

void readSetStableRegimes(char *filename, double ****regimes, int rooti) {
  int i, j;

  //malloc regimes[genei][reg (0)][parameter]
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(j=0; j<ngenes; j++) {
    regimes[0][j] = (double**) malloc(1*sizeof(double*));
    regimes[0][j][0] = (double*) malloc(4*sizeof(double));
  }

  //read in regimes from file
  read3dDouArr(regimes[0], ngenes, 1, 4, filename);

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;
}

void setStableRegimes(double ****regimes, double theta, double sigma2, double alpha, double beta) {
  int i, j;

  //malloc regimes[genei][reg (0)][parameter]
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(j=0; j<ngenes; j++) {
    regimes[0][j] = (double**) malloc(1*sizeof(double*));
    regimes[0][j][0] = (double*) malloc(4*sizeof(double));
  }

  //set all regimes to given params
  for(i=0; i<ngenes; i++) {
    regimes[0][i][0][0] = theta;
    regimes[0][i][0][1] = sigma2;
    regimes[0][i][0][2] = alpha;
    regimes[0][i][0][3] = beta;
  }

  //put reg0 all over tree
  for(i=0; i<=(2*nspecies-2); i++) 
    tree[i].regime = 0;
}

void freeStableRegimes(double ****regimes) {
  int i;

  for(i=0; i<ngenes; i++) {
    free(regimes[0][i][0]);
    free(regimes[0][i]);
  }
  free(regimes[0]);
}

void setShiftRegimes(double ****regimes, double theta1, double theta2, double sigma2, double alpha, double beta) {
  int i, j;

  //malloc shift regimes[genei][reg (0,1)][parameter]
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(2*sizeof(double));
    for(j=0; j<2; j++) {
      regimes[0][i][j] = (double*) malloc(4*sizeof(double));
    }
  }

  //set all regimes to given params
  for(i=0; i<ngenes; i++) {
    regimes[0][i][0][0] = theta1;
    regimes[0][i][0][1] = sigma2;
    regimes[0][i][0][2] = alpha;
    regimes[0][i][0][3] = beta;
    regimes[0][i][1][0] = theta2;
    regimes[0][i][1][1] = sigma2;
    regimes[0][i][1][2] = alpha;
    regimes[0][i][1][3] = beta;
  }

  //put reg0 all over tree
  for(i=0; i<=(2*nspecies-2); i++) 
    tree[i].regime = 0;

  //set branch to reg1
  setBranchRegRecurse(18, 1); //setting anthropoids to regime1
  //setBranchRegRecurse(14, 1); //setting humans to regime1
  //setBranchRegRecurse(0, 1); //setting opossums to regime1
}


void setExcludedGeneRegimeEG(double *****regimes, int rooti) {
  int i, j;

  //setup regimes for betas2 regimes [excluded gene][gene][reg (0)][parameter]
  regimes[0] = (double****) malloc(ngenes*sizeof(double***));
  for(j=0; j<ngenes; j++) {
    regimes[0][j] = (double***) malloc(ngenes*sizeof(double**));
    for(i=0; i<ngenes; i++) {
      regimes[0][j][i] = (double**) malloc(1*sizeof(double));
      regimes[0][j][i][0] = (double*) malloc(4*sizeof(double));
      
      regimes[0][j][i][0][0] = 100.0;
      regimes[0][j][i][0][1] = 10.0;//1.0*i +.01;
      regimes[0][j][i][0][2] = .10;
      regimes[0][j][i][0][3] = 2.0;
    }
  }

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;
}

void freeExcludedGeneRegimeEG(double ****regimes) {
  int i, j;
  for(i=0; i<ngenes; i++) {
    for(j=0; j<ngenes; j++) {
      free(regimes[i][j][0]);
      free(regimes[i][j]);
    }
    free(regimes[i]);
  }
  free(regimes);
}

void setPerGeneRegimeEG(double *****regimes, int rooti) {
  int i, j;

  //setup regimes for betas2 regimes [gene][gene (0)][reg (0)][parameter]
  regimes[0] = (double****) malloc(ngenes*sizeof(double***));
  for(j=0; j<ngenes; j++) {
    regimes[0][j] = (double***) malloc(1*sizeof(double**));
    regimes[0][j][0] = (double**) malloc(1*sizeof(double));
    regimes[0][j][0][0] = (double*) malloc(4*sizeof(double));
      
    regimes[0][j][0][0][0] = 100.0;
    regimes[0][j][0][0][1] = 10.0;//1.0*i +.01;
    regimes[0][j][0][0][2] = .10;
    regimes[0][j][0][0][3] = 2.0;
  }

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;

  //printf("setPerGeneRegimeEG for %d genes\n", ngenes);
}

void freePerGeneRegimeEG(double ****regimes) {
  int i, j;
  for(i=0; i<ngenes; i++) {
    free(regimes[i][0][0]);
    free(regimes[i][0]);
    free(regimes[i]);
  }
  free(regimes);
}

void setPerGeneRegimeEGShift(double *****regimes, int rooti) {
  int i, j;

  //setup regimes for betas2 regimes [gene][gene (0)][reg (0)][parameter]
  regimes[0] = (double****) malloc(ngenes*sizeof(double***));
  for(j=0; j<ngenes; j++) {
    regimes[0][j] = (double***) malloc(1*sizeof(double**));
    regimes[0][j][0] = (double**) malloc(2*sizeof(double));
    regimes[0][j][0][0] = (double*) malloc(4*sizeof(double));
    regimes[0][j][0][1] = (double*) malloc(4*sizeof(double));
      
    regimes[0][j][0][0][0] = 100.0;
    regimes[0][j][0][0][1] = 10.0;//1.0*i +.01;
    regimes[0][j][0][0][2] = .10;
    regimes[0][j][0][0][3] = 2.0;

    regimes[0][j][0][1][0] = 100.0;
    regimes[0][j][0][1][1] = 10.0;//1.0*i +.01;
    regimes[0][j][0][1][2] = .10;
    regimes[0][j][0][1][3] = 2.0;
  }

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;

  //printf("setPerGeneRegimeEG for %d genes\n", ngenes);
}

void freePerGeneRegimeEGShift(double ****regimes) {
  int i, j;
  for(i=0; i<ngenes; i++) {
    free(regimes[i][0][0]);
    free(regimes[i][0][1]);
    free(regimes[i][0]);
    free(regimes[i]);
  }
  free(regimes);
}


void readSetPerGeneStableRegimes(char *filename, double *****regimes, int rooti) {
  int i, j;

  //malloc regimes[genei][[gene (0)][reg (0)][parameter]
  regimes[0] = (double****) malloc(ngenes*sizeof(double***));
  for(j=0; j<ngenes; j++) {
    regimes[0][j] = (double***) malloc(1*sizeof(double**));
    regimes[0][j][0] = (double**) malloc(1*sizeof(double));
    regimes[0][j][0][0] = (double*) malloc(4*sizeof(double));
  }

  //read in regimes from file
  read4dDouArr(regimes[0], ngenes, 1, 1, 4, filename);

  //put reg0 all over tree
  for(i=0; i<=rooti; i++) 
    tree[i].regime = 0;
}




/////////////////////////////////////////////////////////////////////////
// functions to set regimes over tree

setReg0onTree() {
  int i;
  for(i=0; i<=(2*nspecies-2); i++) 
    tree[i].regime = 0;
}

setShiftRegOnTree(int reg1root) {
  int i; 

  //put reg0 all over tree
  for(i=0; i<=(2*nspecies-2); i++) 
    tree[i].regime = 0;

  //set branch to reg1
  setBranchRegRecurse(reg1root, 1);
}


/////////////////////////////////////////////////////////////////////////
// functions to switch between parameters (for findmax) and regimes (for calculations)

//regimes for 0,1 shift to params
void regShift2par(double **regimes, double *params) {
  params[0] = regimes[0][3]; //beta
  params[1] = regimes[0][0]; //theta0
  params[2] = regimes[1][0]; //theta1
  params[3] = regimes[0][1]; //sigma2
  params[4] = regimes[0][2]; //alpha
}

void parShift2reg(double *params, double **regimes) {
  regimes[0][0] = params[1]; //theta0
  regimes[0][1] = params[3]; //sigma2
  regimes[0][2] = params[4]; //alpha
  regimes[0][3] = params[0]; //beta
  regimes[1][0] = params[2]; //theta1
  regimes[1][1] = params[3]; //sigma2
  regimes[1][2] = params[4]; //alpha
  regimes[1][3] = params[0]; //beta
}

void regEGShift2par(double ***regimes, double *params) {
  int i;

  params[0] = regimes[0][0][3]; //beta
  for(i=0; i<ngenes; i++) {
    params[1+4*i] = regimes[i][0][0]; //theta0
    params[2+4*i] = regimes[i][1][0]; //theta1
    params[3+4*i] = regimes[i][0][1]; //sigma2
    params[4+4*i] = regimes[i][0][2]; //alpha
  }
}

void parEGShift2reg(double *params, double ***regimes) {
  int i;

  for(i=0; i<ngenes; i++) {
    regimes[i][0][0] = params[1+4*i]; //theta0
    regimes[i][0][1] = params[3+4*i]; //sigma2
    regimes[i][0][2] = params[4+4*i]; //alpha
    regimes[i][0][3] = params[0]; //beta
    regimes[i][1][0] = params[2+4*i]; //theta1
    regimes[i][1][1] = params[3+4*i]; //sigma2
    regimes[i][1][2] = params[4+4*i]; //alpha
    regimes[i][1][3] = params[0]; //beta
  }
}

void parShiftFixBeta2reg(double *params, double **regimes) {
  regimes[0][0] = params[0]; //theta0
  regimes[0][1] = params[2]; //sigma2
  regimes[0][2] = params[3]; //alpha
  regimes[0][3] = findmaxbeta; //beta
  regimes[1][0] = params[1]; //theta1
  regimes[1][1] = params[2]; //sigma2
  regimes[1][2] = params[3]; //alpha
  regimes[1][3] = findmaxbeta; //beta
}


void regShiftFixBeta2par(double **regimes, double *params) {
  params[0] = regimes[0][0]; //theta0
  params[1] = regimes[1][0]; //theta1
  params[2] = regimes[0][1]; //sigma2
  params[3] = regimes[0][2]; //alpha
}


//stable par/reg conversions
void parStableFixBeta2reg(double *params, double **regimes) {
  regimes[0][0] = params[0]; //theta0
  regimes[0][1] = params[1]; //sigma2
  regimes[0][2] = params[2]; //alpha
  regimes[0][3] = findmaxbeta; //beta
}

void regStableFixBeta2par(double **regimes, double *params) {
  params[0] = regimes[0][0]; //theta0
  params[1] = regimes[0][1]; //sigma2
  params[2] = regimes[0][2]; //alpha
}


/////////////////////////////////////////////////////////////////////////
// functions to estimate parameters

void parEstShiftSharedBeta(double ***regimes) {
  int i, j;
  double speciesmeans, speciesvars, theta0means, theta1means, stvars;
  
  //separate data by species

}


/*
shiftParamsSharedBetaEst <- function(tree, data) {
  ngenes = dim(data)[1]
  
  #separate data by species
  speciesfactor = rep(NA, dim(data)[2])
  sfi = 1
  for(i in 1:length(tree$nindivs)) {
    speciesfactor[sfi:(sfi+tree$nindivs[i]-1)] = rep(i, tree$nindivs[i])
    sfi = sfi + tree$nindivs[i]
  }
  speciesdata = apply(data, 1, function(x) { split(x, speciesfactor)})
  
  #get species means and vars and stationary var
  speciesmeans = lapply(speciesdata, function(x) { as.numeric(lapply(x,mean)) } )
  speciesvars = lapply(speciesdata, function(x) { as.numeric(lapply(x,var)) } )
  theta1means = lapply(1:ngenes, function(x) { speciesmeans[[x]][which(mamtree$regs[1:length(speciesdata[[x]])]==1)] } )
  theta2means = lapply(1:ngenes, function(x) { speciesmeans[[x]][which(mamtree$regs[1:length(speciesdata[[x]])]==2)] } )
  stvars = apply(cbind(unlist(lapply(theta1means, var)), unlist(lapply(theta2means, var))), 1, mean)
  
  #estimate params
  alphas = rep(1, ngenes)
  sigma2s = 2*stvars
  theta1s = unlist(lapply(theta1means, mean))
  theta2s = unlist(lapply(theta2means, mean))
  beta = mean(as.numeric(lapply(speciesvars, mean))/stvars)

  return(c(beta, as.numeric(rbind(alphas, sigma2s, theta1s, theta2s))))
}
*/

/*
shiftParamsSharedBetaRand <- function(tree, data) {
  ngenes = dim(data)[1]
  
  #separate data by species
  speciesfactor = rep(NA, dim(data)[2])
  sfi = 1
  for(i in 1:length(tree$nindivs)) {
    speciesfactor[sfi:(sfi+tree$nindivs[i]-1)] = rep(i, tree$nindivs[i])
    sfi = sfi + tree$nindivs[i]
  }
  speciesdata = apply(data, 1, function(x) { split(x, speciesfactor)})
  theta1data = lapply(1:ngenes, function(x) { unlist(speciesdata[[x]][which(mamtree$regs[1:length(speciesdata[[x]])]==1)]) } )
  theta2data = lapply(1:ngenes, function(x) { unlist(speciesdata[[x]][which(mamtree$regs[1:length(speciesdata[[x]])]==2)]) } )
  
  #get species means and vars and stationary var
  speciesmeans = lapply(speciesdata, function(x) { as.numeric(lapply(x,mean)) } )
  speciesvars = lapply(speciesdata, function(x) { as.numeric(lapply(x,var)) } )
  stvars = as.numeric(lapply(speciesmeans, var))
  
  #estimate params
  alphas = runif(ngenes, min=0, max=20)
  sigma2s = 2*alphas*stvars
  theta1s = unlist(lapply(1:ngenes, function(x) { runif(1, min=min(theta1data[[x]]), max=max(theta1data[[x]])) } ))
  theta2s = unlist(lapply(1:ngenes, function(x) { runif(1, min=min(theta1data[[x]]), max=max(theta1data[[x]])) } ))
  beta = runif(1, min=0, max=(10*mean(as.numeric(lapply(speciesvars, mean))/stvars)))

  return(c(beta, as.numeric(rbind(alphas, sigma2s, theta1s, theta2s))))
}
*/
