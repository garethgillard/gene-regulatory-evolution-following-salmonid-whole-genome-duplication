#include <stdio.h>
#include <gsl/gsl_matrix.h>

//print 1d arrays
//void printStrArr(char **arr, int length);
void printIntArr(int *arr, int length);
void printFloArr(float *arr, int length);
void printDouArr(double *arr, int length);
void printDouArrR(double *arr, int length);
void printLDouArr(long double *arr, int length);

//print 2d arrays
void print2dIntArr(int **arr, int nrows, int ncols);
void print2dDouArr(double **arr, int nrows, int ncols);
void print2dDouArrR(double **arr, int nrows, int ncols);
void print2dPDouArr(double **arr, int nrows, int ncols);
void print2dFloArr(float **arr, int nrows, int ncols);
void print2dCharArr(char **arr, int nrow);//, int ncol);
void print2dLDouArr(long double **arr, int nrows, int ncols);
void printRag2dPDouArr(double **arr, int nrows, int *ncols);
void printR2dDouArr(double **arr, int nrows, int ncols);
void printStringArr(char **arr, int length);
void print4dDouArr(double**** arr, int imax, int jmax, int kmax, int lmax);
void printGSLMatrix(gsl_matrix *m);

//print 3d arrays
void print3dDouArr(double*** arr, int imax, int jmax, int kmax);

//write arrays to file
void writeDouArr(double* arr, int nrows, char* filename);
void writeLRTArr(double* nulls, double* alts, int nrows, char* filename);
void write2dDouArr(double** arr, int nrows, int ncols, char* filename);
void write2dPDouArr(double** arr, int nrows, int ncols, char* filename);  
void write2dIntArr(int** arr, int nrows, int ncols, char* filename);
void write3dDouArr(double*** arr, int imax, int jmax, int kmax, char* filename);
void writeRag2dPDouArr(double** arr, int nrows, int ncols, char* filename);
void writeR3dDouArr(double ***arr, int imax, int jmax, int kmax, char* filename);
void write4dDouArr(double**** arr, int imax, int jmax, int kmax, int lmax, char* filename);
void read4dDouArrTo3dArr(double ***arr, int imax, int jmax, int kmax, char *filename);

//read arrays from file
void readStrArr(char **arr, int n, char *filename);
void readDouArr(double *arr, int n, char *filename);
void read2dIntArr(int** arr, int nrows, int ncols, char* filename);
void readRag2dPDouArr(double **arr, int nrows, int *ncols, char *filename);
void read2dDouArr(double **arr, int ncols, char *filename);
void read3dDouArr(double ***arr, int imax, int jmax, int kmax, char *filename);
void read4dDouArr(double ****arr, int imax, int jmax, int kmax, int lmax, char *filename);
void read2dDouArrRowNames(double **arr, char **linenames, int ncols, char *filename);
void read2dDouArrNoHeaders(double **arr, int nrows, int ncols, char *filename);
