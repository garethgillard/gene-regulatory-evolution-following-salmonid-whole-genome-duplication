/* header file for mystat */

//my own simple math functions
//float fmin(float a, float b);
double dmin(double a, double b);
int imin(int a, int b);
int maxIntArr(int *arr, int len);
long double factorial(int n);
long double logfactorial(int n);
int trinomial(int n, int k1, int k2, int k3);
long double logtrinomial(int n, int k1, int k2, int k3);
int choose(int n, int k);
long double logchoose(int n, int k);
int mylonground(long double a);
int myround(double a);

//quick sorts
void ldsort(long double arr[], int arr2[], int beg, int end);
void isort(int arr[], long double arr2[], long double arr3[], int beg, int end);
void ldldsort(long double arr[], long double arr2[], int beg, int end);
void ldldldsort(long double arr[], long double arr2[], long double arr3[], int beg, int end);
void ldldldldsort(long double arr[], long double arr2[], long double arr3[], long double arr4[], int beg, int end);

//shuffles
void shuffle(int *arr, int imax);

//matrix functions
int zeroCount2dDouArr(double **mat, int nrow, int ncol);
double matrixSum(double **m, int nrow, int ncol);

//list inqueries
int inList(double t, double *arr, double arrlen);
int indexOf(double t, double *arr, double arrlen);
int indexOfIntArr(int t, int *arr, int arrlen);
int vectorEquals(double *arr1, double *arr2, int arrlen);
int inStrList(char *t, char **arr, int arrlen);

//converting triangle matrices and arrays
void tri2arrDou(double **mat, int matlen, double *arr);
void arr2triDou(double **mat, int matlen, double *arr);
void tri2arrInt(int **mat, int matlen, unsigned int *arr);
void arr2triInt(int **mat, int matlen, unsigned int *arr);

//matrix operations
void removeMatrixRow(double **fromMat, double **toMat, int rowi, int nrow, int ncol);
void matrixRow(double **fromMat, double **toMat, int rowi, int ncol);

//basic sample stats
double mymean(double* arr, int n);
double myvar(double* arr, int n);

//pdf stuff
double logdnorm(double x, double mu, double sigma2);
double dnorm(double x, double mu, double sigma2);

//some convenient log scale math
double logAddition(double x, double y);
void normLogProbs(double *arr, int n);
double sumLogProbs(double *arr, int n);
void normProbs(double *arr, int n);

//courtesy of Ralph dos Santos Silva
int rmvnorm(const gsl_rng *r, const int n, const gsl_vector *mean, const gsl_matrix *var, gsl_vector *result);
double dmvnorm(const int n, const gsl_vector *x, const gsl_vector *mean, const gsl_matrix *var);

//gsl data type conversion functions
void douArr2gslVec(double *arr, gsl_vector *gvec, int n);
void dou2DArr2gslMat(double **arr, gsl_matrix *gmat, int m, int n);

//time delay
void delay(unsigned int secs);
