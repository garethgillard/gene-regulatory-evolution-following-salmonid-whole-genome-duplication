#include <stdio.h>
#include <gsl/gsl_matrix.h>

/* i/o functions i've written, mostly printing different arrays */

/*
//prints char* array
void printStrArr(char **arr, int length) {
  int i;
  for(i=0; i<length; i++) {
    printf("%s ", &arr[i]);
  }
  printf("\n");
}
*/

//prints char** array
void print2dCharArr(char **arr, int nrow) {
  int i;
  for(i=0; i<nrow; i++) {
      printf("%s\n", &arr[i][0]);
  }
}


//prints int array
void printIntArr(int *arr, int length) {
  int i;
  for(i=0; i<length; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n");
}

//prints float array
void printFloArr(float *arr, int length) {
  int i;
  for(i=0; i<length; i++) {
    printf("%f ", arr[i]);
  }
  printf("\n");
}

//prints double array
void printDouArr(double *arr, int length) {
  int i;
  for(i=0; i<length; i++) {
    printf("%e ", arr[i]);
  }
  printf("\n");
}

//prints double array for R
void printDouArrR(double *arr, int length) {
  int i;
  printf("arr = array(c(");
  for(i=0; i<length; i++) {
    printf("%e, ", arr[i]);
  }
  printf("), dim=c(%d))\n", length);
}


//prints long double array
void printLDouArr(long double *arr, int length) {
  int i;
  for(i=0; i<length; i++) {
    printf("%Le ", arr[i]);
  }
  printf("\n");
}

//prints string array
void printStringArr(char **arr, int length) {
  int i;
  for(i=0; i<length; i++) {
    printf("%s ", arr[i]);
  }
  printf("\n");
}

//prints 2-d int array
void print2dIntArr(int **arr, int nrows, int ncols) {
  int i,j;
  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      printf("%d ", arr[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//prints 2-d double array
void print2dDouArr(double **arr, int nrows, int ncols) {
  int i,j;
  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      printf("%f ", arr[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//prints 2-d double array in format for R
void print2dDouArrR(double **arr, int nrows, int ncols) {
  int i,j;
  printf("arr = t(array(c(");
  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      printf("%le, ", arr[i][j]);
    }
  }
  printf("), dim=c(%d, %d)))\n", ncols, nrows);
}

//prints 2-d double array
void print2dPDouArr(double **arr, int nrows, int ncols) {
  int i,j;
  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      printf("%le ", arr[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//prints 2-d double array for R
void printR2dDouArr(double **arr, int nrows, int ncols) {
  int i,j;
  printf("array(c(");
  for(j=0; j<ncols; j++) {
    for(i=0; i<nrows; i++) {
      printf("%.70le, ", arr[i][j]);
    }
  }
  printf("), dim=c(%d, %d))\n", nrows, ncols);
}

//prints 2-d long double array
void print2dLDouArr(long double **arr, int nrows, int ncols) {
  int i,j;
  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      printf("%Lf ", arr[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//prints 2-d float array
void print2dFloArr(float **arr, int nrows, int ncols) {
  int i,j;
  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      printf("%f ", arr[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//prints a ragged 2d Double array
void printRag2dPDouArr(double **arr, int nrows, int *ncols) {
  int i, j;

  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols[i]; j++) 
      printf("%le ", arr[i][j]);
    printf("\n");
  }
  printf("\n");
}


//prints a 3d Double array
void print3dDouArr(double*** arr, int imax, int jmax, int kmax) {
  int i, j, k;

  for(i=0; i<imax; i++) {
    for(j=0; j<jmax; j++) {
      for(k=0; k<kmax; k++) 
	printf("%le ", arr[i][j][k]);
      printf("\n");
    }
    printf("\n");
  }
}

void print4dDouArr(double**** arr, int imax, int jmax, int kmax, int lmax) {
  int i, j, k, l;

  for(i=0; i<imax; i++) {
    for(j=0; j<jmax; j++) {
      for(k=0; k<kmax; k++) {
	for(l=0; l<lmax; l++) 
	  printf("%.20e ", arr[i][j][k][l]);
	printf("\n");
      }
      printf("\n");
    }
    printf("\n");
  }
}

void printGSLMatrix(gsl_matrix *m) {
  size_t i, j; 

  for(i = 0; i < m->size1; i++) {
    for(j = 0; j < m->size2; j++) 
      printf("%g ", gsl_matrix_get(m, i, j));
    printf("\n");
  }

}



/*
//parses a .ped file
void parsePED(char *filename) {
  FILE *fin;
  int i;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    exit(1);
  }
  
  fscanf(fin, "WTCCC%d WTCCC%d %d %d %d %d", &indivID, &indivID, &par1, &par2, &sex, &pheno);
  while(curSNP
  fscanf(fin, "%d%d", &npeople, &nSNPs);

}
*/

//prints a Double array to file
void writeDouArr(double* arr, int nrows, char* filename) {
  int i;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<nrows; i++) 
    fprintf(fout, "%.20e ", arr[i]);
  fprintf(fout, "\n");

  fclose(fout);
}

//prints a Double array to file of LRTs given null and alt log likelihoods
void writeLRTArr(double* nulls, double* alts, int nrows, char* filename) {
  int i;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<nrows; i++) 
    fprintf(fout, "%.20e ", (2.0*alts[i] - 2.0*nulls[i]));
  fprintf(fout, "\n");

  fclose(fout);
}


//writes a 1d str array to file
void writeStrArr(char **arr, int n, char *filename) {
  int i;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<n; i++)
    fprintf(fout, "%s\n", arr[i]);
  
  fclose(fout);
}



//prints a 2d Double array to file
void write2dDouArr(double** arr, int nrows, int ncols, char* filename) {
  int i, j;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) 
      fprintf(fout, "%.20e ", arr[i][j]);
    fprintf(fout, "\n");
  }

  fclose(fout);
}

//prints a 2d Double array to file, but with more precision
void write2dPDouArr(double** arr, int nrows, int ncols, char* filename) {
  int i, j;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) 
      fprintf(fout, "%lf ", arr[i][j]);
    fprintf(fout, "\n");
  }

  fclose(fout);
}

//prints a 2d Int array to file
void write2dIntArr(int** arr, int nrows, int ncols, char* filename) {
  int i, j;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) 
      fprintf(fout, "%d ", arr[i][j]);
    fprintf(fout, "\n");
  }

  fclose(fout);
}

//prints a 3d Double array to file
void write3dDouArr(double*** arr, int imax, int jmax, int kmax, char* filename) {
  int i, j, k;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<imax; i++) {
    for(j=0; j<jmax; j++) {
      for(k=0; k<kmax; k++) 
	fprintf(fout, "%.20e ", arr[i][j][k]);
      fprintf(fout, "\n");
    }
    fprintf(fout, "\n");
  }

  fclose(fout);
}

//prints a 4d Double array to file
void write4dDouArr(double**** arr, int imax, int jmax, int kmax, int lmax, char* filename) {
  int i, j, k, l;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<imax; i++) {
    for(j=0; j<jmax; j++) {
      for(k=0; k<kmax; k++) {
	for(l=0; l<lmax; l++) 
	  fprintf(fout, "%.20e ", arr[i][j][k][l]);
	fprintf(fout, "\n");
      }
      fprintf(fout, "\n");
    }
    fprintf(fout, "\n");
  }
  
  fclose(fout);
}

//writes to file a ragged 2d Double array to file with more precision, non-entries are 0.0
void writeRag2dPDouArr(double** arr, int nrows, int ncols, char* filename) {
  int i, j;
  FILE *fout;

  fout = fopen(filename, "w");

  for(i=0; i<nrows; i++) {
    for(j=0; j<ncols; j++) {
      if(arr[i][j] != 0.0)
	fprintf(fout, "%.100e ", arr[i][j]);
    }
    fprintf(fout, "\n");
  }

  fclose(fout);
}

//writes a 3d double array for R
void writeR3dDouArr(double ***arr, int imax, int jmax, int kmax, char* filename) {
  int i,j,k;
  FILE *fout;

  fout = fopen(filename, "w");
  
  //fprintf(fout, "array(c(");
  for(k=0; k<kmax; k++) {
    for(j=0; j<jmax; j++) {
      for(i=0; i<imax; i++) {
	fprintf(fout, "%le ", arr[i][j][k]);
      }
    }
  }
  //fprintf(fout, "), dim=c(%d, %d, %d))\n", imax, jmax, kmax);
  fprintf(fout, "\n");

  fclose(fout);
}

//reads a 1d str array from file
void readStrArr(char **arr, int n, char *filename) {
  int i;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<n; i++)
    fscanf(fin, "%s", arr[i]);
  
  fclose(fin);
}


//reads a 1d dou array from file, non-entries are NA and nrows is first line of file
void readDouArr(double *arr, int n, char *filename) {
  int i;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<n; i++)
    fscanf(fin, "%lf", &arr[i]);
  
  fclose(fin);
}

//reads a ragged 2d Double array from file with more precision, non-entries are 0.0
void readRag2dPDouArr(double **arr, int nrows, int *ncols, char *filename) {
  int i, j;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<nrows; i++) 
    for(j=0; j<ncols[i]; j++) 
      fscanf(fin, "%le ", &arr[i][j]);

  fclose(fin);
}

//reads a 2d int array from file, non-entries are 0.0
void read2dIntArr(int **arr, int nrows, int ncols, char *filename) {
  int i, j;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<nrows; i++)
    for(j=0; j<ncols; j++)
      fscanf(fin, "%d", &arr[i][j]);
  
  fclose(fin);
}

//reads a 2d dou array from file, non-entries are NA and nrows is first line of file
void read2dDouArr(double **arr, int ncols, char *filename) {
  int i, j, nrows;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  fscanf(fin, "%d", &nrows);
  for(i=0; i<nrows; i++)
    for(j=0; j<ncols; j++)
      fscanf(fin, "%lf", &arr[i][j]);
  
  fclose(fin);
}

//reads a 2d dou array from file, non-entries are NA and nrows is first line of file
void read2dDouArrNoHeaders(double **arr, int nrows, int ncols, char *filename) {
  int i, j;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<nrows; i++)
    for(j=0; j<ncols; j++)
      fscanf(fin, "%lf", &arr[i][j]);
  
  fclose(fin);
}

//reads a 3d dou array from file
void read3dDouArr(double ***arr, int imax, int jmax, int kmax, char *filename) {
  int i, j, k, l;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<imax; i++)
    for(j=0; j<jmax; j++)
      for(k=0; k<kmax; k++)
	fscanf(fin, "%lf", &arr[i][j][k]);
  
  fclose(fin);
}

//reads a 4d dou array from file
void read4dDouArr(double ****arr, int imax, int jmax, int kmax, int lmax, char *filename) {
  int i, j, k, l;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<imax; i++)
    for(j=0; j<jmax; j++)
      for(k=0; k<kmax; k++)
	for(l=0; l<lmax; l++)
	  fscanf(fin, "%lf", &arr[i][j][k][l]);
  
  fclose(fin);
}


//reads a 4d dou array from file, puts in a 3d dou arr erases dimension one (index zero)
void read4dDouArrTo3dArr(double ***arr, int imax, int jmax, int kmax, char *filename) {
  int i, j, k, l;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }

  for(i=0; i<imax; i++)
    for(j=0; j<jmax; j++)
      for(k=0; k<kmax; k++)
	fscanf(fin, "%lf", &arr[i][j][k]);
  
  fclose(fin);
}


//reads a 2d dou array from file, non-entries are NA, nrows is first line of file, each line has label
void read2dDouArrRowNames(double **arr, char **linenames, int ncols, char *filename) {
  int i, j, nrows;
  FILE *fin;

  fin = fopen(filename, "r");
  if(fin == NULL) {
    printf("Unable to locate input file: %s\n", filename);
    return;
  }
  //printf("opeend file %s\n", filename);

  fscanf(fin, "%d", &nrows);
  //printf("nrows is %d\n", nrows);
  for(i=0; i<nrows; i++) {
    //printf("reading row %d: ", i);
    fscanf(fin, "%s", linenames[i]);
    for(j=0; j<ncols; j++) {
      //printf("col %d ", j);
      fscanf(fin, "%lf", &arr[i][j]);
      //printf("%lf ", arr[i][j]);
    }
    //printf("\n");
  }

  fclose(fin);
}
