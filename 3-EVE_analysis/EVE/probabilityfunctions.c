#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_randist.h>

#include"myio.h"
#include"mystat.h"
#include "bfgs.h"


//node definition
struct node {
  int down;
  int up[2];
  double bl;
  int regime;
  int nindiv;
  int level;
};

//global vars
struct node *tree; //phylogeny
double **expr; // expression data: expr[gene number][individual number]  
int ngenes; //total number of genes in dataset
int nspecies; //total number of species in dataset 
int totnindiv; //total number of individuals in all species
int findmaxgenei; //gene index considered in findmax
double findmaxbeta; //var for fixed beta val to be used in findmax
gsl_rng *grng; //random number generator
double ***simRegimes; //regimes used for simulations
int simRegFlag;
double *gmlLs; //max logLs for each gene
int randStartFlag; //1:start findmax_bfgs multiple times with random startparam vals  0:don't
int multiStartFlag; //1:start findmax_bfgs with paramfactor * and / alpha and sigma2   0:don't
int verbose; //0:no in-process stdout, 100:max in-process stdout


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// given params, getting expression expectation and covariance 

void leafExpectationRecurse(int nodei, double parE, double *Es, double **regimes) {
  int curregi = tree[nodei].regime;
  double eAT = exp(-1.0 * regimes[curregi][2] * tree[nodei].bl);
  double curE = parE*eAT + regimes[curregi][0]*(1.0-eAT);

  if(tree[nodei].up[0] == -1) { //leaf
    Es[nodei] = curE;
  }
  else { //internal node
    leafExpectationRecurse(tree[nodei].up[0], curE, Es, regimes);
    leafExpectationRecurse(tree[nodei].up[1], curE, Es, regimes);
  }
}

//calculate expectation at each leaf in tree, store in exp
void leafExpectation(double *Es, double **regimes) {
  int rooti = 2*nspecies-2;
  double rootE = regimes[tree[rooti].regime][0];   //init root E to theta (be it longrun theta (in OU) or theta_root (in non-evol, BM)

  //run leaf expecation on daughter branches
  leafExpectationRecurse(tree[rooti].up[0], rootE, Es, regimes);
  leafExpectationRecurse(tree[rooti].up[1], rootE, Es, regimes);
}

void varRecurse(int nodei, double *Vars, double **regimes) {
  int curregi = tree[nodei].regime;
  double e2AT = exp(-2.0 * regimes[curregi][2] * tree[nodei].bl);

  //printf("varrec on node %d\n", nodei);

  Vars[nodei] = (regimes[curregi][1]/(2.0*regimes[curregi][2]))*(1.0 - e2AT) + Vars[tree[nodei].down]*e2AT;
  
  if(tree[nodei].up[0] > -1) { //internal node
    varRecurse(tree[nodei].up[0], Vars, regimes);  
    varRecurse(tree[nodei].up[1], Vars, regimes);  
  }
}

int mrcaRecurse(int nodei, int nodej) {
  //printf("mrca of %d and %d", nodei, nodej);
  if(tree[nodei].down == tree[nodej].down)  //same parent
    return(tree[nodei].down);
  else //diff parent
    return(mrcaRecurse(tree[nodei].down, tree[nodej].down));
}

int mrca(int nodei, int nodej) {
  //printf("getting mrca of %d and %d\n", nodei, nodej);
  //get nodei and j to same level
  while(tree[nodei].level > tree[nodej].level) {
    nodei = tree[nodei].down;
    //printf("getting mrca of %d and %d\n", nodei, nodej);
  }
  while(tree[nodei].level < tree[nodej].level) {
    nodej = tree[nodej].down;
    //printf("getting mrca of %d and %d\n", nodei, nodej);
  }
  //get mrca
  //printf("mrcaRecurse on %d and %d\n", nodei, nodej);
  return(mrcaRecurse(nodei, nodej));
}

double *gVars; 
//calculate covariance between all leaves in tree
void leafCovariance(double **Cov, double **regimes) {
  int i, j, leafi, leafj, mrcai, curi;
  int rooti = 2*nspecies-2;
  //double *Vars; //noteforRori: if it's slow, try something that doesn't use malloc so much
  double sumAT;

  //malloc Vars, eATs
  //Vars = (double*) malloc((rooti+1)*sizeof(double));

  //set rootVar 
  if(regimes[tree[rooti].regime][2] > 0) //to stationary var if alpha>0
    gVars[rooti] = regimes[tree[rooti].regime][1]/(2.0*regimes[tree[rooti].regime][2]);
  else //to 0 otherwise
    gVars[rooti] = 0.0;

  //calculate variance up through tree
  varRecurse(tree[rooti].up[0], gVars, regimes);
  varRecurse(tree[rooti].up[1], gVars, regimes);
  //printf("leaf vars are: "); printDouArr(gVars, (2*nspecies-1));

  //copy Vars to Cov diag
  for(i=0; i<nspecies; i++)
    Cov[i][i] = gVars[i];

  //calc cov between all leaf pairs
  for(leafi=0; leafi<nspecies; leafi++) {
    for(leafj=(leafi+1); leafj<nspecies; leafj++) {
      //get mrca
      mrcai = mrca(leafi, leafj);

      //get sum of alpha*branchlength for non-shared branches
      sumAT = 0.0;
      curi = leafi;
      while(curi != mrcai) {
	sumAT += regimes[tree[curi].regime][2] * tree[curi].bl;
	curi = tree[curi].down;
	//printf("added onto sumAT for node %d with bl %f\n", curi, tree[curi].bl);
      }
      curi = leafj;
      while(curi != mrcai) {
	sumAT += regimes[tree[curi].regime][2] * tree[curi].bl;
	curi = tree[curi].down;
      }
      //printf("%d, %d sumAT is %f\n", leafi, leafj, sumAT);

      Cov[leafi][leafj] = gVars[mrcai]*exp(-1.0*sumAT);
      //printf("cov[%d][%d] from var_par %lf and exp(-at) %lf\n", leafi, leafj, gVars[mrcai], exp(-1.0*sumAT));
    }
  }
  //print2dDouArr(Cov, nspecies, nspecies);

  //free
  //free(Vars);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// given leaf distribution stuff, expand to indivs

void expandLeafE(double *lE, double *iE) {
  int i, j, iEi=0;

  for(i=0; i<nspecies; i++) {
    for(j=0; j<(tree[i].nindiv); j++) {
      iE[iEi] = lE[i];
      iEi++;
    }
  }
}

void expandLeafCov(double **lCov, double **iCov, double **regimes) {
  int i, j, ii, ij, iCovi, iCovj;
  double inpopvar;

  iCovi=0;
  for(i=0; i<nspecies; i++) {
    iCovj=iCovi;
    for(j=i; j<nspecies; j++) {
      for(ii=0; ii<(tree[i].nindiv); ii++) {
	for(ij=0; ij<(tree[j].nindiv); ij++) {
	  iCov[(iCovi+ii)][(iCovj+ij)] = lCov[i][j];
	}
      }
      iCovj += tree[j].nindiv;
    }
    iCovi += tree[i].nindiv;
  }

  //add in variance within pops
  iCovi=0;
  for(i=0; i<nspecies; i++) {
    inpopvar = 1.0 * regimes[tree[i].regime][1]/(2.0*regimes[tree[i].regime][2]) * regimes[tree[i].regime][3];
    for(ii=0; ii<tree[i].nindiv; ii++) {
      iCov[iCovi][iCovi] += inpopvar;
      iCovi++;
    }
  }
}

////////////////////////////////////////////////////////////////////
// total E and Cov functions

double *glEs;
void Expectation(double *Exp, double **regimes) {
  //lEs = (double*) malloc(nspecies*sizeof(double));
  leafExpectation(glEs, regimes);
  expandLeafE(glEs, Exp);

  //printDouArr(Exp, totnindiv);

  //free(lEs);
}

double **glCov;
void Covariance(double **Cov, double **regimes) {
  int i, j;
  //double **lCov;

  /*
  lCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    lCov[i] = (double*) malloc(nspecies*sizeof(double));
  */

  leafCovariance(glCov, regimes);
  expandLeafCov(glCov, Cov, regimes);

  for(i=0; i<totnindiv; i++) 
    for(j=0; j<i; j++) 
      Cov[i][j] = Cov[j][i];

  //print2dDouArr(Cov, totnindiv, totnindiv);
  /*
  for(i=0; i<nspecies; i++)
    free(lCov[i]);
  free(lCov);
  */
}

void ExternCovariance(double **Cov, double **regimes) {
  int i, j;
  //double **lCov;

  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));


  leafCovariance(glCov, regimes);
  expandLeafCov(glCov, Cov, regimes);

  for(i=0; i<totnindiv; i++) 
    for(j=0; j<i; j++) 
      Cov[i][j] = Cov[j][i];


  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);
}

////////////////////////////////////////////////////////////////////
// simulation functions

void simExpr(double **regimes) {
  int i, j;
  double *Es, **Cov; 
  gsl_vector *gslE, *gslexpr;
  gsl_matrix *gslCov;
  
  //mallocs
  Es = (double*) malloc(totnindiv*sizeof(double));
  Cov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    Cov[i] = (double*) malloc(totnindiv*sizeof(double));
  gslE = gsl_vector_calloc(totnindiv);
  gslexpr = gsl_vector_calloc(totnindiv);
  gslCov = gsl_matrix_calloc(totnindiv, totnindiv);

  //inner mallocs
  glEs = (double*) malloc(nspecies*sizeof(double));
  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));

  //get exp and cov
  Expectation(Es, regimes);
  Covariance(Cov, regimes);
  
  //convert to gsl data types
  douArr2gslVec(Es, gslE, totnindiv);
  dou2DArr2gslMat(Cov, gslCov, totnindiv, totnindiv);

  //generate expr
  for(i=0; i<ngenes; i++) {
    rmvnorm(grng, totnindiv, gslE, gslCov, gslexpr);
    for(j=0; j<totnindiv; j++) 
      expr[i][j] = gsl_vector_get(gslexpr, j);
  }

  //free
  free(Es);
  for(i=0; i<totnindiv; i++)
    free(Cov[i]);
  free(Cov);
  gsl_vector_free(gslE);
  gsl_vector_free(gslexpr);
  gsl_matrix_free(gslCov);

  //inner frees
  free(glEs);
  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);
}

//simulates expr vals for each gene based on param vals in simRegimes
void simExprEachGene() {
  int i, j;
  double *Es, **Cov; 
  gsl_vector *gslE, *gslexpr;
  gsl_matrix *gslCov;
  
  //mallocs
  Es = (double*) malloc(totnindiv*sizeof(double));
  Cov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    Cov[i] = (double*) malloc(totnindiv*sizeof(double));
  gslE = gsl_vector_calloc(totnindiv);
  gslexpr = gsl_vector_calloc(totnindiv);
  gslCov = gsl_matrix_calloc(totnindiv, totnindiv);

  //inner mallocs
  glEs = (double*) malloc(nspecies*sizeof(double));
  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));

  //printf("tree for sim:  "); printTreeRegimes(2*nspecies-2); printf("\n");

  //generate expr
  for(i=0; i<ngenes; i++) {
    //get exp and cov
    Expectation(Es, simRegimes[i]);
    Covariance(Cov, simRegimes[i]);

    //printf("expectation:\n"); printDouArr(Es, totnindiv);
    //printf("covariance:\n"); print2dDouArr(Cov, totnindiv, totnindiv);
    //delay(5);
    /*
    if(i==0) {   
      printf("leaf vars are: "); printDouArr(gVars, (2*nspecies-1));
      printf("glCov diag is: "); for(j=0; j<(nspecies); j++) { printf("%lf ", glCov[j][j]); } printf("\n");
      printf("Cov diag is: "); for(j=0; j<totnindiv; j++) { printf("%lf ", Cov[j][j]); } printf("\n");
      printf("gene %d cov:\n", i); print2dDouArr(Cov, totnindiv, totnindiv);
    } 
    */

    //convert to gsl data types
    douArr2gslVec(Es, gslE, totnindiv);
    dou2DArr2gslMat(Cov, gslCov, totnindiv, totnindiv);
    
    //do {
      rmvnorm(grng, totnindiv, gslE, gslCov, gslexpr);
      //} while (gsl_vector_ispos(gslexpr) != 1);
    for(j=0; j<totnindiv; j++) 
      expr[i][j] = gsl_vector_get(gslexpr, j);

    //if(i<20) {  printf("simulated gene %d expr with statvar %lf  popvar %lf  params  ", i, simRegimes[i][0][1]/(2.0*simRegimes[i][0][2]), simRegimes[i][0][3]*simRegimes[i][0][1]/(2.0*simRegimes[i][0][2]));  printDouArr(simRegimes[i][0], 4); }
  }

  //free
  free(Es);
  for(i=0; i<totnindiv; i++)
    free(Cov[i]);
  free(Cov);
  gsl_vector_free(gslE);
  gsl_vector_free(gslexpr);
  gsl_matrix_free(gslCov);

  //inner frees
  free(glEs);
  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// parameter method-of-moments-ish estimate functions

//estimates theta0, theta1, sigma2, and alpha for genei given beta
//set parmaxmin
double gparmin = 0.00000000000000001;
double gparmax = 10000000000000000.0;
double gparminmin = 0.00000000000000000000001;
double gparmaxmax = 10000000000000000000000.0;
double *gspmeans, *gspvars;
int *gspregs;
void estSingleGeneShiftParamFixBeta(int genei, double beta, double *theta0, double *theta1, double *sigma2, double *alpha) {
  int i, expri;
  //double *spmeans, *spvars;
  //int *spregs; 
  int n0=0, n1=0;
  double var0=0.0, var1=0.0, stvar;

  //malloc and init
  /*
  spmeans = (double*) malloc(nspecies*sizeof(double));
  spvars = (double*) malloc(nspecies*sizeof(double));
  spregs = (int*) malloc(nspecies*sizeof(int));
  */

  //run through expr over species tracking each means and var and reg
  expri=0;
  for(i=0; i<nspecies; i++) {
    //printf("gunna get mean at gene %d species %d expri %d\n", genei, i, expri);
    gspmeans[i] = mymean(&expr[genei][expri], tree[i].nindiv);
    gspvars[i] = myvar(&expr[genei][expri], tree[i].nindiv);
    gspregs[i] = tree[i].regime;

    expri += tree[i].nindiv;
  }
  //printDouArr(spmeans, nspecies);  
  //printDouArr(spvars, nspecies);  

  //calc ballpark estimates
  theta0[0] = 0.0;
  theta1[0] = 0.0;
  for(i=0; i<nspecies; i++) {
    if(gspregs[i] == 0) {
      theta0[0] += gspmeans[i];
      n0++;
    } else {
      theta1[0] += gspmeans[i];
      n1++;
    }
  }
  theta0[0] /= n0;  
  theta1[0] /= n1;  
  for(i=0; i<nspecies; i++) {
    if(gspregs[i] == 0) {
      var0 += (theta0[0]-gspmeans[i])*(theta0[0]-gspmeans[i]);
      n0++;
    } else {
      var1 += (theta1[0]-gspmeans[i])*(theta1[0]-gspmeans[i]);
      n1++;
    }
  }
  var0 /= (n0-1.0);
  var1 /= (n1-1.0);
  //printf("theta0 %lf  theta1 %lf  ", theta0[0], theta1[0]);
  //printf("var0 %lf  var1 %lf  ", var0, var1);


  //est stvar as average of est from inpopvar and betweenpopvar
  stvar = ((mymean(gspvars, nspecies)/beta) + ((var0 + var1)/2.0))/2.0;
  //printf("meanvar %lf  beta %lf  ", mymean(gspvars, nspecies), beta);
  //printf("stvar1 is %lf   stvar2 is %lf  ", (mymean(gspvars, nspecies)/beta), ((var0 + var1)/2.0));
  //printf("stvar is %lf\n", stvar);

  //breakdown to sigma2 and alpha estimates
  if(stvar < 20) {
    sigma2[0] = stvar*stvar;
    alpha[0] = 0.5*stvar;
  } else if(stvar < 200) {
    sigma2[0] = stvar;
    alpha[0] = 0.5;
  } else {
    if(stvar*2.0*gparmin < gparmax)
      sigma2[0] = stvar * 2.0* gparmin;
    else 
      sigma2[0] = gparmax;
    alpha[0] = gparmin;
  }
  //printf("with beta %lf: theta0 %lf  theta1 %lf  sigma2 %lf  alpha %lf\n", beta, theta0[0], theta1[0], sigma2[0], alpha[0]);
}



void estSingleGeneStableBeta1(int genei, double beta, double *theta0, double *theta1) {
  int i, expri;
  int n0=0, n1=0;

  //run through expr over species tracking cumulating means
  theta0[0] = 0.0;
  theta1[0] = 0.0;
  expri=0;
  for(i=0; i<nspecies; i++) {
    if(tree[i].regime == 0) {
      theta0[0] += mymean(&expr[genei][expri], tree[i].nindiv);
      n0++;
    } else {
      theta1[0] += mymean(&expr[genei][expri], tree[i].nindiv);
      n1++;
    }
    expri += tree[i].nindiv;
  }
  theta0[0] /= n0;  
  theta1[0] /= n1;  

  //setting both thetas to total average
  /*
  theta0[0] = 0.0;
  expri=0; 
  for(i=0; i<totnindiv; i++) {
    theta0[0] += mymean(&expr[genei][expri], tree[i].nindiv);
    expri += tree[i].nindiv;
  }
  theta0[0] /= nspecies;  
  theta1[0] = theta0[0];
  */

  //printf("with beta %lf: theta0 %lf  theta1 %lf\n", beta, theta0[0], theta1[0]);
}



//estimates beta shared across genes
double estShiftSharedBeta() {
  int i, expri, genei;
  int n0=0, n1=0;
  double mean0=0.0, mean1=0.0, var0=0.0, var1=0.0, beta=0.0;

  //run over genes
  for(genei=0; genei<ngenes; genei++) {
    //run through expr over species tracking each means and var and reg
    expri=0;
    for(i=0; i<nspecies; i++) {
      gspmeans[i] = mymean(&expr[genei][expri], tree[i].nindiv);
      gspvars[i] = myvar(&expr[genei][expri], tree[i].nindiv);
      gspregs[i] = tree[i].regime;
      
      expri += tree[i].nindiv;
    }
    
    //calc ballpark estimates
    for(i=0; i<nspecies; i++) {
      if(gspregs[i] == 0) {
	mean0 += gspmeans[i];
	n0++;
      } else {
	mean1 += gspmeans[i];
	n1++;
      }
  }
    mean0 /= n0;  
    mean1 /= n1;  
    for(i=0; i<nspecies; i++) {
      if(gspregs[i] == 0) {
      var0 += (mean0-gspmeans[i])*(mean0-gspmeans[i]);
      n0++;
      } else {
	var1 += (mean1-gspmeans[i])*(mean1-gspmeans[i]);
	n1++;
      }
    }
    var0 /= (n0-1.0);
    var1 /= (n1-1.0);

    //est stvar as average of est from inpopvar and betweenpopvar
    beta += (mymean(gspvars, nspecies)) / ((var0 + var1)/2.0);
  }
  
  beta /= ngenes;
  if(verbose > 40) { printf("estimated beta as %lf\n", beta); }
  return(beta);
}

//estimates beta shared across genes
double estStableSharedBeta() {
  int i, expri, genei;
  double invar=0.0, curvar=0.0, beta=0.0;
  int nnonmiss;

  //malloc
  gspmeans = (double*) malloc(nspecies*sizeof(double));

  //run over genes
  for(genei=0; genei<ngenes; genei++) {
    //run through expr over species tracking each means and var and reg
    expri=0;
    invar = 0.0;
    nnonmiss = 0;
    for(i=0; i<nspecies; i++) {
      gspmeans[i] = mymean(&expr[genei][expri], tree[i].nindiv);
      curvar = myvar(&expr[genei][expri], tree[i].nindiv);
      if(curvar != -1.0) {
	invar  += curvar;
	nnonmiss++;
      }
      expri += tree[i].nindiv;
      //printf("at species %d, invar=%lf\n", i, invar);
    }
    
    invar /= nspecies;

    //est stvar as average of est from inpopvar and betweenpopvar
    //printf("gene %d invar %lf and betvar %lf diff is %lf\n", genei, invar, myvar(gspmeans, nspecies), (myvar(gspmeans, nspecies)-invar));
    beta += (myvar(gspmeans, nspecies) / invar); 
  }
  
  beta /= ngenes;
  if(verbose > 40) { printf("estimated beta as %lf\n", beta); }

  //free
  free(gspmeans);

  return(beta);
}


//estimate stable regime parameters, assuming beta is shared, less accurate
void estStableRegimesSharedBetaFast(double ****regimes) {
  int i;
  double beta;

  //estimate beta first
  beta = estStableSharedBeta();
  //printf("estimated beta as %lf\n", beta);


  //setup regimes for stable regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(1*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));

    regimes[0][i][0][0] = mymean(expr[i], totnindiv);
    regimes[0][i][0][1] = myvar(expr[i], totnindiv);
    regimes[0][i][0][2] = 0.5;
    regimes[0][i][0][3] = beta;
  }

  //put reg0 all over tree
  for(i=0; i<(2*nspecies-1); i++) 
    tree[i].regime = 0;

  //free
  for(i=0; i<ngenes; i++) {
    free(regimes[0][i][0]);
    free(regimes[0][i]);
  }
  free(regimes[0]);
}



//estimate beta shift regime parameters 
void estBetaShiftRegimes(double ****regimes) { //regimes[gene i][regime i][parameter i]
  int i, j, expri;
  double beta, statvar;
  double *means;

  //mallocs
  means = (double*) malloc(nspecies*sizeof(double));
  //???????START HERE

  //estimate beta first
  beta = estStableSharedBeta();
  //printf("estimated beta as %lf\n", beta);

  //setup regimes for shift regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(2*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));
    regimes[0][i][1] = (double*) malloc(4*sizeof(double));

    expri=0;
    for(j=0; j<nspecies; j++) {
      means[j] = mymean(&expr[i][expri], tree[j].nindiv);
      expri += tree[j].nindiv;
    }
    statvar = myvar(means, nspecies);

    regimes[0][i][0][0] = mymean(means, nspecies);
    regimes[0][i][0][1] = statvar*statvar;
    regimes[0][i][0][2] = statvar/2.0;
    regimes[0][i][0][3] = beta;
    regimes[0][i][1][0] = mymean(means, nspecies);
    regimes[0][i][1][1] = statvar*statvar;
    regimes[0][i][1][2] = statvar/2.0;
    regimes[0][i][1][3] = beta;
  }

  //free
  free(means);
}


//estimate shift regime parameters (really, just same theta estimate twice), assuming beta is shared
void estShiftRegimesSharedBeta(double ****regimes) { //regimes[gene i][regime i][parameter i]
  int i, j, expri;
  double beta, statvar;
  double *means;

  //mallocs
  means = (double*) malloc(nspecies*sizeof(double));

  //estimate beta first
  beta = estStableSharedBeta();
  //printf("estimated beta as %lf\n", beta);

  //setup regimes for shift regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(2*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));
    regimes[0][i][1] = (double*) malloc(4*sizeof(double));

    expri=0;
    for(j=0; j<nspecies; j++) {
      means[j] = mymean(&expr[i][expri], tree[j].nindiv);
      expri += tree[j].nindiv;
    }
    statvar = myvar(means, nspecies);

    regimes[0][i][0][0] = mymean(means, nspecies);
    regimes[0][i][0][1] = statvar*statvar;
    regimes[0][i][0][2] = statvar/2.0;
    regimes[0][i][0][3] = beta;
    regimes[0][i][1][0] = mymean(means, nspecies);
    regimes[0][i][1][1] = statvar*statvar;
    regimes[0][i][1][2] = statvar/2.0;
    regimes[0][i][1][3] = beta;
  }

  //free
  free(means);
}


//estimate shift regime parameters (really, just same theta estimate twice), assuming beta is shared
void estShiftRegimesFixedBeta(double ****regimes, double beta) { //regimes[gene i][regime i][parameter i]
  int i, j, expri;
  double statvar;
  double *means;

  //mallocs
  means = (double*) malloc(nspecies*sizeof(double));

  //setup regimes for shift regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(2*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));
    regimes[0][i][1] = (double*) malloc(4*sizeof(double));

    expri=0;
    for(j=0; j<nspecies; j++) {
      means[j] = mymean(&expr[i][expri], tree[j].nindiv);
      expri += tree[j].nindiv;
    }
    statvar = myvar(means, nspecies);

    regimes[0][i][0][0] = mymean(means, nspecies);
    regimes[0][i][0][1] = statvar*statvar;
    regimes[0][i][0][2] = statvar/2.0;
    regimes[0][i][0][3] = beta;
    regimes[0][i][1][0] = mymean(means, nspecies);
    regimes[0][i][1][1] = statvar*statvar;
    regimes[0][i][1][2] = statvar/2.0;
    regimes[0][i][1][3] = beta;
  }

  //free
  free(means);
}





//estimate stable regime parameters, assuming beta is shared
void estStableRegimesSharedBeta(double ****regimes) {
  int i, j, expri;
  double beta, statvar;
  double *means;

  //mallocs
  means = (double*) malloc(nspecies*sizeof(double));

  //estimate beta first
  beta = estStableSharedBeta();
  //printf("estimated beta as %lf\n", beta);

  //setup regimes for stable regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(1*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));

    expri=0;
    for(j=0; j<nspecies; j++) {
      means[j] = mymean(&expr[i][expri], tree[j].nindiv);
      //means[i] = mymean(&expr[i][expri], tree[j].nindiv);
      expri += tree[j].nindiv;
    }
    statvar = myvar(means, nspecies);
    //printf("for gene %d statvar: %lf  mymean: %lf\n", i, statvar, mymean(means, nspecies));
    //printf("means are: "); printDouArr(means, nspecies);

    regimes[0][i][0][0] = mymean(means, nspecies);
    regimes[0][i][0][1] = statvar*statvar;
    regimes[0][i][0][2] = statvar/2.0;
    regimes[0][i][0][3] = beta;
  }

  //put reg0 all over tree
  for(i=0; i<(2*nspecies-1); i++) 
    tree[i].regime = 0;

  //free
  free(means);
}




//estimate stable regime parameters, assuming beta is shared
void estStableRegimesFixedBeta(double ****regimes, double beta) {
  int i, j, expri;
  double statvar;
  double *means;

  //mallocs
  means = (double*) malloc(nspecies*sizeof(double));

  //setup regimes for stable regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(1*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));

    expri=0;
    for(j=0; j<nspecies; j++) {
      means[j] = mymean(&expr[i][expri], tree[j].nindiv);
      expri += tree[j].nindiv;
    }
    statvar = myvar(means, nspecies);

    regimes[0][i][0][0] = mymean(means, nspecies);
    regimes[0][i][0][1] = statvar;
    regimes[0][i][0][2] = 0.5;
    regimes[0][i][0][3] = beta;
  }

  //put reg0 all over tree
  for(i=0; i<(2*nspecies-1); i++) 
    tree[i].regime = 0;

  //free
  free(means);
  for(i=0; i<ngenes; i++) {
    free(regimes[0][i][0]);
    free(regimes[0][i]);
  }
  free(regimes[0]);
}

//estimate stable regime parameters, assuming beta varies across genes
void estStableRegimesVaryBeta(double ****regimes) {
  int i, j, expri;
  double beta, statvar;
  double *means, *vars;

  //mallocs
  means = (double*) malloc(nspecies*sizeof(double));
  vars = (double*) malloc(nspecies*sizeof(double));

  //setup regimes for stable regime
  regimes[0] = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    regimes[0][i] = (double**) malloc(1*sizeof(double));
    regimes[0][i][0] = (double*) malloc(4*sizeof(double));

    expri=0;
    for(j=0; j<nspecies; j++) {
      means[j] = mymean(&expr[i][expri], tree[j].nindiv);
      vars[j] = myvar(&expr[i][expri], tree[j].nindiv);
      expri += tree[j].nindiv;
    }
    statvar = myvar(means, nspecies);
    //printf("means "); printDouArr(means, nspecies);
    //printf("vars  "); printDouArr(vars, nspecies);

    regimes[0][i][0][0] = mymean(means, nspecies);
    regimes[0][i][0][1] = statvar;//statvar*statvar;
    regimes[0][i][0][2] = 0.5;//statvar/2.0;
    regimes[0][i][0][3] = mymean(vars, nspecies)/statvar;
    //printf("gene%d reg: %lf %lf %lf %lf\n", i, regimes[0][i][0][0], regimes[0][i][0][1], regimes[0][i][0][2], regimes[0][i][0][3]);
  }

  //put reg0 all over tree
  for(i=0; i<(2*nspecies-1); i++) 
    tree[i].regime = 0;

  //free
  free(means);
  free(vars);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// likelihood functions

double* gEs;
double** gCov;
gsl_vector* ggslE;
gsl_vector* ggslexpr;
gsl_matrix* ggslCov;


//return the logL for a single gene number findmaxgenei
double logLsingleGene(double **regimes) {
  int i, j;
  //double *Es, **Cov, 
  double lL; 
  //gsl_vector *gslE, *gslexpr;
  //gsl_matrix *gslCov;

  //mallocs
  /*
  Es = (double*) malloc(totnindiv*sizeof(double));
  Cov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    Cov[i] = (double*) malloc(totnindiv*sizeof(double));
  gslE = gsl_vector_calloc(totnindiv);
  gslexpr = gsl_vector_calloc(totnindiv);
  gslCov = gsl_matrix_calloc(totnindiv, totnindiv);
  */

  //printf("  finding logLsingleGene for gene %d\n", findmaxgenei);

  //get exp and cov
  Expectation(gEs, regimes);
  Covariance(gCov, regimes);
  /*
  if(verbose > 99) {
    printf("expectation:\n"); printDouArr(gEs, totnindiv);
    printf("covariance:\n"); print2dDouArr(gCov, totnindiv, totnindiv);
    }
  */

  //check for zero cov
  if(matrixSum(gCov, totnindiv, totnindiv) <= 0.0) {
    if(matrixSum(gCov, totnindiv, totnindiv) < 0.0) {
      if(verbose > 90) { printf("got less than zero covariance matrix with gparmin %le and regime ", gparmin); printDouArr(regimes[0], 4); }
      if(vectorEquals(expr[findmaxgenei], gEs, totnindiv) == 1) 
	return(0.0);
      else
	return(-1000000.0);//-10000000000000000.0);
    }
    if(verbose > 90) { printf("got zero covariance matrix with gparmin %le and regime ", gparmin); printDouArr(regimes[0], 4); }
    if(vectorEquals(expr[findmaxgenei], gEs, totnindiv) == 1) 
      return(0.0);
    else
      return(-1000000.0);//-10000000000000000.0);
  }

  //convert to gsl data types
  douArr2gslVec(gEs, ggslE, totnindiv);
  dou2DArr2gslMat(gCov, ggslCov, totnindiv, totnindiv);  
  douArr2gslVec(expr[findmaxgenei], ggslexpr, totnindiv);

  //get logL
  lL = dmvnorm(totnindiv, ggslexpr, ggslE, ggslCov);
  /*
  if(lL ==  -100000000000000000.0) {
    printf("invert matrix failed on regime "); printDouArr(regimes[0], 4);
  }
  */
  //free
  /*
  free(Es);
  for(i=0; i<totnindiv; i++)
    free(Cov[i]);
  free(Cov);
  gsl_vector_free(gslE);
  gsl_vector_free(gslexpr);
  gsl_matrix_free(gslCov);
  */

  //check for probability of 0.0
  if(lL == -INFINITY || lL == INFINITY) {
    if(lL == INFINITY) {
      //printf("got inf lL for gene %d, so returning low number on regime ", findmaxgenei); printDouArr(regimes[0], 4);
      write2dDouArr(gCov, totnindiv, totnindiv, "covmat.tmp");
      //delay(100);
    }
    //printf("got inf lL for gene %d, so returning low number on regime ", findmaxgenei); printDouArr(regimes[0], 4);
    //lL = -1000000000000000.0;//-1.0*DBL_MAX;
    lL = -1000000.0;

 }

  //if(lL > 0) { printDouArrR(expr[findmaxgenei], totnindiv); printDouArrR(gEs, totnindiv); printf("pos lL for cov\n"); write2dDouArr(gCov, totnindiv, totnindiv, "tmpcov"); delay(30);}
  //  if(verbose > 99) { printf("single gene logL is %lf for regime  ", lL); printDouArr(regimes[0], 4); //}

  return(lL);
}


//to give findmax_bfgs when optimizing over pars for a single gene with beta fixed
double **gsgregimes;
double logLfixBetaShiftfindmax(double *params) {
  double lL; 

  //convert from params to regimes
  parShiftFixBeta2reg(params, gsgregimes);

  //printf("gunna calc single gene lL for params  "); printDouArr(params, 4);
  //get logL
  lL = logLsingleGene(gsgregimes);

  //printf("returning single gene lL as %lf\tfor params  ", (-1.0*lL)); printDouArr(params, 4);
  return(-1.0*lL);
}

//to give findmax_bfgs when optimizing over pars for a single gene with beta fixed for stable regime
double **gsgregimes;
double logLfixBetaStablefindmax(double *params) {
  double lL; 

  //convert from params to regimes
  parStableFixBeta2reg(params, gsgregimes);

  //get logL
  lL = logLsingleGene(gsgregimes);

  //printf("returning single gene %d lL as %lf\tfor params  %.10lf %.10lf %.10lf\n", findmaxgenei, (-1.0*lL), params[0], params[1], params[2]); //printDouArr(params, 3); 
  return(-1.0*lL);
}

//to give findmax_bfgs when optimizing over pars for a many genes with beta fixed for stable regime
double logLMultiGeneFixBetaStablefindmax(double *params) {
  int i;
  double curlL, totlL; 
  double **regimes;

  //malloc/init regimes
  setStableRegime(&regimes, 2*nspecies-2);

  //convert from params to regimes
  parStableFixBeta2reg(params, regimes);

  //get cumulative logL under these params
  totlL = 0.0;
  for(i=0; i<ngenes; i++) {
    findmaxgenei = i;
    curlL = logLsingleGene(regimes);

    totlL += curlL;
  }

  //free
  freeStableRegime(regimes);

  //printf("returning single gene lL as %lf\tfor params  ", (-1.0*lL)); printDouArr(params, 3);
  return(-1.0*totlL);
}


int checkParamAtBounds(double *pars, double *lbounds, double *ubounds, int n) {
  int i;
  //printDouArr(lbounds, n);
  for(i=0; i<n; i++) 
    if(pars[i] <= lbounds[i] || pars[i] >= ubounds[i])
      return 1;
  return 0;
}

//run Rasmus' findmax on each gene separately given params ([0]: beta, [...] other params per gene) returning cumulative log L set up as gsl_function
double logLnestedSharedBetaStableGSLmin(double beta, void *consts) { 
  int i, j, k; 
  double lL, curlL;
  double ubounds[4], lbounds[4];
  int whichbounds[4], npars=3;  //[theta0, sigma2, alpha]
  double ***regimes = (double***) consts;
  double geneparams[4];
  double tmpparams[4];
  double paramfactor = 100000.0;

  //set findmax inputs
  for(i=0; i<npars; i++) {
    lbounds[i] = gparmin;
    ubounds[i] = gparmax;
    whichbounds[i] = 2;
  }

  //set fixed beta for findmax
  findmaxbeta = beta;
  //printf("set findmaxbeta to %lf\n", findmaxbeta);

  lL = 0.0;
  //optimize over each gene
  for(i=0; i<ngenes; i++) {
    //'estimate' thetas as sim vals
    /*if(simRegFlag >= 3) 
      geneparams[0] = ((simRegimes[i][0][0] + simRegimes[i][1][0])/2.0);
      else*/
    geneparams[0] = simRegimes[i][0][0];
    geneparams[1] = simRegimes[i][0][1]; //sigma2
    geneparams[2] = simRegimes[i][0][2]; //alpha


    //set narrower findmax bounds for theta
    //lbounds[0] = geneparams[0]/100.0;
    //ubounds[0] = geneparams[0]*100.0;

    //set non-positive findmax bounds for theta
    lbounds[0] = -1.0*ubounds[0];

    tmpparams[0] = geneparams[0];
    tmpparams[1] = geneparams[1];
    tmpparams[2] = geneparams[2];

    //get logL 
    findmaxgenei = i;//1455;

    gmlLs[i] = findmax_bfgs(npars, geneparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
    /*    
    if(gmlLs[i] > 1000000000.0 ) {
      printf("to start, under beta %lf returning gene %d cur mlL as %lf for params  with start vals %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2]);  printDouArr(geneparams, npars);
      verbose = 100;
      geneparams[0] = simRegimes[i][0][0];
      geneparams[1] = simRegimes[i][0][1];
      geneparams[2] = simRegimes[i][0][2];
      
      gmlLs[i] = findmax_bfgs(npars, geneparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
    }
    */
	  
    //printf("under beta %lf returning gene %d cur mlL as %lf for params with start vals %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2]);  printDouArr(geneparams, npars);
    //some trys at specific low/high startpoints 
    if(multiStartFlag > 0) {
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][0][1]/paramfactor;
      tmpparams[2] = simRegimes[i][0][2];
      //printf("for gene %d checkParamsAtBounds is %d\n", findmaxgenei, checkParamAtBounds(tmpparams, lbounds, ubounds, npars));
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	//printf("gunn run for params  ");  printDouArr(tmpparams, npars);
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  low sigma2 seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];
	  
	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = simRegimes[i][0][1]/(pow(paramfactor,0.5));
	  tmpparams[2] = simRegimes[i][0][2];
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = simRegimes[i][0][1]/(pow(paramfactor,2));
	  tmpparams[2] = simRegimes[i][0][2];
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  //memcpy(geneparams, tmpparams, 3);
	  multiStartFlag++;
	}
      }
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][0][1];
      tmpparams[2] = simRegimes[i][0][2]/paramfactor;
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	//printf("gunn run for params  ");  printDouArr(tmpparams, npars);
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  low alpha seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = simRegimes[i][0][1];
	  tmpparams[2] = simRegimes[i][0][2]/(pow(paramfactor,0.5));
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = simRegimes[i][0][1];
	  tmpparams[2] = simRegimes[i][0][2]/(pow(paramfactor,2.0));
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  //memcpy(geneparams, tmpparams, 3);
	  multiStartFlag++;
	}
      }
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = paramfactor*simRegimes[i][0][1];
      tmpparams[2] = simRegimes[i][0][2];
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	//printf("gunn run for params  ");  printDouArr(tmpparams, npars);
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  high sigma2 seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = (pow(paramfactor,0.5))*simRegimes[i][0][1];
	  tmpparams[2] = simRegimes[i][0][2];
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = (pow(paramfactor,2.0))*simRegimes[i][0][1];
	  tmpparams[2] = simRegimes[i][0][2];
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  //memcpy(geneparams, tmpparams, 3);
	  multiStartFlag++;
	}
      }
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][0][1];
      tmpparams[2] = paramfactor*simRegimes[i][0][2]; 
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	//printf("gunn run for params  ");  printDouArr(tmpparams, npars);
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  high alpha seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = simRegimes[i][0][1];
	  tmpparams[2] = (pow(paramfactor,2.0))*simRegimes[i][0][2]; 
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  tmpparams[0] = simRegimes[i][0][0];
	  tmpparams[1] = simRegimes[i][0][1];
	  tmpparams[2] = (pow(paramfactor,0.5))*simRegimes[i][0][2]; 
	  if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	    curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	    if((curlL-gmlLs[i]) > .001) {
	      gmlLs[i] = curlL;
	      geneparams[0] = tmpparams[0];
	      geneparams[1] = tmpparams[1];
	      geneparams[2] = tmpparams[2];
	    }
	  }

	  //memcpy(geneparams, tmpparams, 3);
	  multiStartFlag++;
	}
      }
    }
    //printf("under beta %lf returning gene %d cur mlL as %lf for params  with ml vals ", beta, i, gmlLs[i]);  printDouArr(geneparams, npars);

    //check for params stuck at bounds and widen bounds if improves likelihood much
    if(checkParamAtBounds(geneparams, lbounds, ubounds, npars) == 1) {
      tmpparams[0] = geneparams[0];
      tmpparams[1] = geneparams[1];
      tmpparams[2] = geneparams[2];
      do {
	//widen bounds
	for(j=0; j<npars; j++) {
	  lbounds[j] /= 10.0;
	  ubounds[j] *= 10.0;
	}

	//printf("WARNING: prev curlL %lf redoing gene %d findmax under stable param est at bound: ", gmlLs[i], i); printDouArr(geneparams, npars); 
	//printf("    WARNING: param est at bounds, starting again with params "); printDouArr(tmpparams, npars);
	curlL  = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	//printf("returning gene %d cur mlL as %lf for params and ml vals ", i, curlL);  printDouArr(tmpparams, npars); delay(1);
      } while((checkParamAtBounds(tmpparams, lbounds, ubounds, npars) == 1) && ((curlL-gmlLs[i]) > 0.001)); 

      //reset bounds
      for(j=0; j<npars; j++) {
	lbounds[j] = gparmin;
	ubounds[j] = gparmax;
      }
      lbounds[0] = simRegimes[i][0][0]/100.0;
      ubounds[0] = simRegimes[i][0][0]*100.0;

      //save ml and params
      gmlLs[i] = curlL;
      memcpy(geneparams, tmpparams, 3);	
    }

    //check some extra random parameter start points
    if(randStartFlag > 0) {
      for(k=0; k<5; k++) {
	for(j=0; j<npars; j++) 
	  tmpparams[j] = gsl_ran_flat(grng, lbounds[j], ubounds[j]);
	
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  if(verbose>80) {printf("    WARNING: gene %d  rand param seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); }
	  gmlLs[i] = curlL;
	  memcpy(geneparams, tmpparams, 3);
	  randStartFlag++;
	}
      }
    }

    //cumulate logL over genes 
    lL += gmlLs[i];//curlL;

    if(gmlLs[i] == -1000000.0) {
      //printf("gene %d discLsingleGene L=%lf  for regime  ", findmaxgenei, L); printDouArr(regimes[0], 4);
      printf("ERROR: Problem with optimization.  Consider fiddling with paramfactor value.\n");
      if(verbose>10) {
	printf("under beta %lf returning gene %d cur mlL as %lf for params with final start vals %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2]);  printDouArr(geneparams, npars);
	printf("original start params: "); printDouArr(simRegimes[i][0], 3);
	printf("for expression profile: "); printDouArr(expr[i], totnindiv);
      }
      exit(0);
    }

    //printf("under beta %lf returning gene %d cur mlL as %lf for params with final start vals %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2]);  printDouArr(geneparams, npars);

    //store findmax geneparams
    parStableFixBeta2reg(geneparams, regimes[i]);
    //delay(10); //remove
 }
  
  if(verbose > 32) { printf("for beta is %lf logLnestedSharedBetaStableGSLmin logL is %lf\n", findmaxbeta, lL); }
  return(-1.0*lL);
}


//run Rasmus' findmax on each gene separately given params ([0]: beta, [...] other params per gene) returning cumulative log L set up as gsl_function
double logLnestedSharedBetaShiftGSLmin(double beta, void *consts) { 
  int i, j, k; 
  double lL, curlL;
  double ubounds[5], lbounds[5];
  int whichbounds[5], npars=4;  //[theta0, theta1, sigma2, alpha]
  double ***regimes = (double***) consts;
  double geneparams[5];
  double tmpparams[5];
  double paramfactor = 10000000.0;//100000.0;

  //set findmax inputs
  for(i=0; i<npars; i++) {
    lbounds[i] = gparmin;
    ubounds[i] = gparmax;
    whichbounds[i] = 2;
  }

  //set fixed beta for findmax
  findmaxbeta = beta;
  //printf("set findmaxbeta to %lf\n", findmaxbeta);

  lL = 0.0;
  //optimize over each gene
  for(i=0; i<ngenes; i++) {
    //'estimate' thetas as sim vals
    geneparams[0] = simRegimes[i][0][0];
    geneparams[1] = simRegimes[i][1][0];
    geneparams[2] = simRegimes[i][0][1]; //sigma2
    geneparams[3] = simRegimes[i][0][2]; //alpha

    //set narrower findmax bounds for thetas
    lbounds[0] = geneparams[0]/100.0;
    ubounds[0] = geneparams[0]*100.0;
    lbounds[1] = geneparams[1]/100.0;
    ubounds[1] = geneparams[1]*100.0;

    //set tmpparams
    tmpparams[0] = geneparams[0];
    tmpparams[1] = geneparams[1];
    tmpparams[2] = geneparams[2];
    tmpparams[3] = geneparams[3];

    //set discritization
    findmaxgenei = i;

    //get logL 
    gmlLs[i] = findmax_bfgs(npars, geneparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);

    //printf("gene %d discLsingleGene L=%lf  for regime  ", findmaxgenei, L); printDouArr(regimes[0], 4);
    //printf("under beta %lf returning gene %d cur mlL as %lf for params  with start vals %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2]);  printDouArr(geneparams, npars);

    //some trys at specific low/high startpoints 
    if(multiStartFlag > 0) {
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][1][0];
      tmpparams[2] = simRegimes[i][0][1]/paramfactor;
      tmpparams[3] = simRegimes[i][0][2];
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  low sigma2 seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];
	  geneparams[3] = tmpparams[3];
	  multiStartFlag++;
	}
      }
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][1][0];
      tmpparams[2] = simRegimes[i][0][1];
      tmpparams[3] = simRegimes[i][0][2]/paramfactor;
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  low alpha seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];
	  geneparams[3] = tmpparams[3];
	  multiStartFlag++;
	}
      }
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][1][0];
      tmpparams[2] = paramfactor*simRegimes[i][0][1];
      tmpparams[3] = simRegimes[i][0][2];
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  high sigma2 seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];
	  geneparams[3] = tmpparams[3];
	  multiStartFlag++;
	}
      }
      tmpparams[0] = simRegimes[i][0][0];
      tmpparams[1] = simRegimes[i][1][0];
      tmpparams[2] = simRegimes[i][0][1];
      tmpparams[3] = paramfactor*simRegimes[i][0][2]; 
      if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  //printf("    WARNING: gene %d  high alpha seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); delay(1);
	  gmlLs[i] = curlL;
	  geneparams[0] = tmpparams[0];
	  geneparams[1] = tmpparams[1];
	  geneparams[2] = tmpparams[2];
	  geneparams[3] = tmpparams[3];
	  multiStartFlag++;
	}
      }
    }
    //printf("under beta %lf returning gene %d cur mlL as %lf for params  with ml vals ", beta, i, gmlLs[i]);  printDouArr(geneparams, npars);

    //check for params stuck at bounds and widen bounds if improves likelihood much
    if(checkParamAtBounds(geneparams, lbounds, ubounds, npars) == 1) {
      tmpparams[0] = geneparams[0];
      tmpparams[1] = geneparams[1];
      tmpparams[2] = geneparams[2];
      tmpparams[3] = geneparams[3];
      do {
	//widen bounds
	for(j=0; j<npars; j++) {
	  lbounds[j] /= 10.0;
	  ubounds[j] *= 10.0;
	}

	//printf("WARNING: prev curlL %lf redoing gene %d findmax under stable param est at bound: ", gmlLs[i], i); printDouArr(geneparams, npars); 
	//printf("    WARNING: param est at bounds, starting again with params "); printDouArr(tmpparams, npars);
	curlL  = findmax_bfgs(npars, tmpparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);
	//printf("returning gene %d cur mlL as %lf for params and ml vals ", i, curlL);  printDouArr(tmpparams, npars); delay(1);
      } while((checkParamAtBounds(tmpparams, lbounds, ubounds, npars) == 1) && ((curlL-gmlLs[i]) > 0.001)); 

      //reset bounds
      for(j=0; j<npars; j++) {
	lbounds[j] = gparmin;
	ubounds[j] = gparmax;
      }
      lbounds[0] = simRegimes[i][0][0]/100.0;
      ubounds[0] = simRegimes[i][0][0]*100.0;
      lbounds[1] = simRegimes[i][1][0]/100.0;
      ubounds[1] = simRegimes[i][1][0]*100.0;

      //save ml and params
      gmlLs[i] = curlL;
      memcpy(geneparams, tmpparams, 4);
    }

    //check some extra random parameter start points
    if(randStartFlag > 0) {
      for(k=0; k<5; k++) {
	for(j=0; j<npars; j++)
	  tmpparams[j] = gsl_ran_flat(grng, lbounds[j], ubounds[j]);
	
	curlL = findmax_bfgs(npars, tmpparams, &logLfixBetaShiftfindmax, NULL, lbounds, ubounds, whichbounds, -1);
	if((curlL-gmlLs[i]) > .001) {
	  if(verbose>80) { printf("    WARNING: gene %d  rand param seed got better logL=%f  by=%lf and beta %lf and ml vals ", i, curlL, (curlL-gmlLs[i]), beta);  printDouArr(tmpparams, npars); }
	  gmlLs[i] = curlL; 
	  memcpy(geneparams, tmpparams, 4);
	  randStartFlag++;
	}
      }
    }

    //cumulate logL over genes 
    lL += gmlLs[i];//curlL;

    if(gmlLs[i] == -1000000.0) {
      //printf("gene %d discLsingleGene L=%lf  for regime  ", findmaxgenei, L); printDouArr(regimes[0], 4);
      printf("ERROR: Problem with optimization.  Consider fiddling with paramfactor value.\n");
      if(verbose>10) {
	printf("under beta %lf returning gene %d cur mlL as %lf for params with final start vals %lf %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2], tmpparams[3]);  printDouArr(geneparams, npars);
	printf("for expression profile: "); printDouArr(expr[i], totnindiv);
      }
      exit(0);
    }
    //printf("under beta %lf returning gene %d cur mlL as %lf for params with final start vals %lf %lf %lf %lf and ml vals ", beta, i, gmlLs[i], tmpparams[0], tmpparams[1], tmpparams[2], tmpparams[3]);  printDouArr(geneparams, npars);

    //store findmax geneparams
    parShiftFixBeta2reg(geneparams, regimes[i]);
 }
  
  if(verbose > 32) { printf("for beta is %lf logLnestedSharedBetaShiftGSLmin logL is %lf\n", findmaxbeta, lL); }//GOOD ONE THAT I CAN USE
  return(-1.0*lL);
}


//returns max logL with one set of params over all genes by calling logLMultiGeneFixBetaStablefindmax
double logLMultiGeneSharedBetaStableGSLmin(double *regimes) { 
  int i, j, k; 
  double lL, curlL;
  double ubounds[4], lbounds[4];
  int whichbounds[4], npars=4;  //[theta0, sigma2, alpha, beta]
  double geneparams[4];
  double tmpparams[4];
  double paramfactor = 2500.0;

  //set findmax inputs
  for(i=0; i<npars; i++) {
    lbounds[i] = gparmin;
    ubounds[i] = gparmax;
    whichbounds[i] = 2;
  }

  //'estimate' params as sim vals
  /*if(simRegFlag >= 3) 
    geneparams[0] = ((simRegimes[i][0][0] + simRegimes[i][1][0])/2.0);
    else*/
  geneparams[0] = simRegimes[i][0][0];
  geneparams[1] = simRegimes[i][0][1]; //sigma2
  geneparams[2] = simRegimes[i][0][2]; //alpha
  geneparams[3] = simRegimes[i][0][3]; //beta
      
  //set narrower findmax bounds for theta
  lbounds[0] = geneparams[0]/100.0;
  ubounds[0] = geneparams[0]*100.0;

  for(i=0; i<4; i++)
    tmpparams[i] = geneparams[i];

  //get logL 
  //printf("gunn run gene %d for params  ", i);  printDouArr(geneparams, npars);
  if(verbose>7) { printf("for gene %d starting params ", findmaxgenei); printDouArr(geneparams, npars); }
  lL = findmax_bfgs(npars, geneparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
  //printf("returning gene %d cur mlL as %lf for params  with start vals %lf %lf %lf and ml vals ", i, (lL), tmpparams[0], tmpparams[1], tmpparams[2], tmpparams[3]);  printDouArr(geneparams, npars);
  
  //some trys at specific low/high startpoints 
  if(multiStartFlag > 0) {
    tmpparams[0] = simRegimes[i][0][0];
    tmpparams[1] = simRegimes[i][0][1]/paramfactor;
    tmpparams[2] = simRegimes[i][0][2];
    tmpparams[3] = simRegimes[i][0][3];
    if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
      //printf("gunn run for params  ");  printDouArr(geneparams, npars);
      curlL = findmax_bfgs(npars, tmpparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
      if((curlL-lL) > .001) {
	//printf("    WARNING: low sigma2 seed got better logL=%f  by=%lf and ml vals ", curlL, (curlL-lL));  printDouArr(tmpparams, npars);
	lL = curlL;
	memcpy(geneparams, tmpparams, npars);
	multiStartFlag++;
      }
    }
    tmpparams[0] = simRegimes[i][0][0];
    tmpparams[1] = simRegimes[i][0][1];
    tmpparams[2] = simRegimes[i][0][2]/paramfactor;
    tmpparams[3] = simRegimes[i][0][3];
    if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
      //printf("gunn run for params  ");  printDouArr(geneparams, npars);
      curlL = findmax_bfgs(npars, tmpparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
      if((curlL-lL) > .001) {
	//printf("    WARNING: low alpha seed got better logL=%f  by=%lf and ml vals ", curlL, (curlL-lL));  printDouArr(tmpparams, npars);
	lL = curlL;
	memcpy(geneparams, tmpparams, npars);
	multiStartFlag++;
      }
    }
    tmpparams[0] = simRegimes[i][0][0];
    tmpparams[1] = paramfactor*simRegimes[i][0][1];
    tmpparams[2] = simRegimes[i][0][2];
    tmpparams[3] = simRegimes[i][0][3];
    if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
      //printf("gunn run for params  ");  printDouArr(geneparams, npars);
      curlL = findmax_bfgs(npars, tmpparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
      if((curlL-lL) > .001) {
	//printf("    WARNING: high sigma2 seed got better logL=%f  by=%lf and ml vals ", curlL, (curlL-lL));  printDouArr(tmpparams, npars);
	lL = curlL;
	memcpy(geneparams, tmpparams, npars);
	multiStartFlag++;
      }
    }
    tmpparams[0] = simRegimes[i][0][0];
    tmpparams[1] = simRegimes[i][0][1];
    tmpparams[2] = paramfactor*simRegimes[i][0][2]; 
    tmpparams[3] = simRegimes[i][0][3];
    if(checkParamAtBounds(tmpparams, lbounds, ubounds, npars) < 1) {
      //printf("gunn run for params  ");  printDouArr(geneparams, npars);
      curlL = findmax_bfgs(npars, tmpparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
      if((curlL-lL) > .001) {
	//printf("    WARNING: high alpha seed got better logL=%f  by=%lf and ml vals ", curlL, (curlL-lL));  printDouArr(tmpparams, npars);
	lL = curlL;
	memcpy(geneparams, tmpparams, npars);
	multiStartFlag++;
      }
    }
  }
  
  //check for params stuck at bounds and widen bounds if improves likelihood much
  if(checkParamAtBounds(geneparams, lbounds, ubounds, npars) == 1) {
    tmpparams[0] = geneparams[0];
    tmpparams[1] = geneparams[1];
    tmpparams[2] = geneparams[2];
    do {
      //widen bounds
      for(j=0; j<npars; j++) {
	lbounds[j] /= 10.0;
	ubounds[j] *= 10.0;
      }
      
      //printf("WARNING: prev curlL %lf redoing gene %d findmax under stable param est at bound: ", curlL, i); printDouArr(geneparams, npars);
      //printf("    WARNING: param est at bounds, starting again with params "); printDouArr(tmpparams, npars);
      curlL  = findmax_bfgs(npars, tmpparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
      //printf("returning gene %d cur mlL as %lf for params and ml vals ", i, curlL);  printDouArr(tmpparams, npars);
    } while((checkParamAtBounds(tmpparams, lbounds, ubounds, npars) == 1) && ((curlL-lL) > 0.001)); 
    
    //narrow bounds
    for(j=0; j<npars; j++) {
      lbounds[j] *= 10.0;
      ubounds[j] /= 10.0;
    }
    
    //save ml and params
    lL = curlL;
    memcpy(geneparams, tmpparams, npars);	
  }

  //check some extra random parameter start points
  if(randStartFlag > 0) {
    for(k=0; k<5; k++) {
      for(j=0; j<npars; j++) 
	tmpparams[j] = gsl_ran_flat(grng, lbounds[j], ubounds[j]);
	
      curlL = findmax_bfgs(npars, tmpparams, &logLMultiGeneFixBetaStablefindmax, NULL, lbounds, ubounds, whichbounds, -1);
      if((curlL-gmlLs[i]) > .001) {
	if(verbose>80) { printf("    WARNING: rand param seed got better logL=%f  by=%lf and ml vals ", curlL, (curlL-gmlLs[i]));  printDouArr(tmpparams, npars); }
	lL = curlL;
	memcpy(geneparams, tmpparams, npars);
	randStartFlag++;
      }
    }
  }
  
  if(verbose > 32) { printf("for beta is %lf logLMultiGeneSharedBetaStableGSLmin logL is %lf\n", findmaxbeta, lL); }
  return(-1.0*lL);
}

//returns log L computed with nested (Brent(bfgs)) under stable model
double logLnestedSharedBetaStable(double ***regimes) {
  int i, j;
  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m_expected = M_PI;
  gsl_function F;
  double beta, startlL;
  double minbeta = 0.00001, maxbeta = 1000.0;
  double minlL;

  //inner mallocs for logL
  gEs = (double*) malloc(totnindiv*sizeof(double));
  gCov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    gCov[i] = (double*) malloc(totnindiv*sizeof(double));
  ggslE = gsl_vector_calloc(totnindiv);
  ggslexpr = gsl_vector_calloc(totnindiv);
  ggslCov = gsl_matrix_calloc(totnindiv, totnindiv);

  glEs = (double*) malloc(nspecies*sizeof(double));
  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));
 
  gsgregimes = (double**) malloc(1*sizeof(double*));
  gsgregimes[0] = (double*) malloc(4*sizeof(double));

  gspmeans = (double*) malloc(nspecies*sizeof(double));
  gspvars = (double*) malloc(nspecies*sizeof(double));
  gspregs = (int*) malloc(nspecies*sizeof(int));

  //estimate beta and set bounds on it
  beta = simRegimes[0][0][3];
  minbeta = beta/10.0;
  maxbeta = beta*10.0;

  if(verbose > 30) { printf("    first attempt beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta); }
  startlL = logLnestedSharedBetaStableGSLmin(beta, regimes);
  //printf("varybeta startlL: %lf  \n", startlL);
  while(logLnestedSharedBetaStableGSLmin(minbeta, regimes) < startlL) {
    startlL = logLnestedSharedBetaStableGSLmin(minbeta, regimes);
    beta = minbeta;
    minbeta /= 10.0; 
    if(verbose > 30) { printf("    shifted beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta); }
  }
  while(logLnestedSharedBetaStableGSLmin(maxbeta, regimes) < startlL) {
    startlL = logLnestedSharedBetaStableGSLmin(maxbeta, regimes);
    beta = maxbeta;
    maxbeta *= 10.0;
    if(verbose > 30) { printf("    shifted beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta); }
  }
  if(verbose > 30) { printf("    running gslmin stable for beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta); }


  //set up gsl_function F
  F.function = &logLnestedSharedBetaStableGSLmin;
  F.params = regimes;
  
  //set up gsl_min_fminimizer
  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set(s, &F, beta, minbeta, maxbeta);

  //loop over gsl_minimizer
  do {
    iter++;
    status = gsl_min_fminimizer_iterate (s);

    beta = gsl_min_fminimizer_x_minimum (s);
    minbeta = gsl_min_fminimizer_x_lower (s);
    maxbeta = gsl_min_fminimizer_x_upper (s);
     
    status = gsl_min_test_interval(minbeta, maxbeta, 0.001, 0.001);
  } while (status == GSL_CONTINUE && iter < max_iter);

  //save min logL
  minlL = logLnestedSharedBetaStableGSLmin(beta, regimes);
  //printf("stable minlL is %lf\n", minlL);

  //set regimes beta to min logL beta
  for(i=0; i<ngenes; i++) {
    regimes[i][0][3] = beta;
  }

  //free
  gsl_min_fminimizer_free(s);

  //free globals for logL
  free(gEs);
  for(i=0; i<totnindiv; i++)
    free(gCov[i]);
  free(gCov);
  gsl_vector_free(ggslE);
  gsl_vector_free(ggslexpr);
  gsl_matrix_free(ggslCov);

  free(glEs);
  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);

  free(gsgregimes[0]);
  free(gsgregimes);

  free(gspmeans);
  free(gspvars);
  free(gspregs);

  //return min logL
  return(-1.0*minlL);
}

//returns log L computed with nested (Brent(bfgs)) under stable model
double logLnestedSharedBetaShift(double ***regimes) {
  int i, j;
  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m_expected = M_PI;
  gsl_function F;
  double beta, startlL;
  double minbeta = 0.00001, maxbeta = 1000.0;
  double minlL;

  //inner mallocs for logL
  gEs = (double*) malloc(totnindiv*sizeof(double));
  gCov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    gCov[i] = (double*) malloc(totnindiv*sizeof(double));
  ggslE = gsl_vector_calloc(totnindiv);
  ggslexpr = gsl_vector_calloc(totnindiv);
  ggslCov = gsl_matrix_calloc(totnindiv, totnindiv);

  glEs = (double*) malloc(nspecies*sizeof(double));
  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));
 
  gsgregimes = (double**) malloc(2*sizeof(double*));
  gsgregimes[0] = (double*) malloc(4*sizeof(double));
  gsgregimes[1] = (double*) malloc(4*sizeof(double));

  gspmeans = (double*) malloc(nspecies*sizeof(double));
  gspvars = (double*) malloc(nspecies*sizeof(double));
  gspregs = (int*) malloc(nspecies*sizeof(int));

  //estimate beta and set bounds on it
  beta = simRegimes[0][0][3];
  minbeta = beta/10.0;
  maxbeta = beta*10.0;

  //printf("    first attempt beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta);
  startlL = logLnestedSharedBetaShiftGSLmin(beta, regimes);
  //printf("varybeta startlL: %lf  \n", startlL);
  while(logLnestedSharedBetaShiftGSLmin(minbeta, regimes) < startlL) {
    startlL = logLnestedSharedBetaShiftGSLmin(minbeta, regimes);
    beta = minbeta;
    minbeta /= 10.0; 
    //printf("    shifted beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta);
  }
  while(logLnestedSharedBetaShiftGSLmin(maxbeta, regimes) < startlL) {
    startlL = logLnestedSharedBetaShiftGSLmin(maxbeta, regimes);
    beta = maxbeta;
    maxbeta *= 10.0;
    //printf("    shifted beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta);
  }
  //I'VE USED THIS GOOD ONE A LOT  printf("    running gslmin stable for beta %lf in bounds (%lf %lf)\n", beta, minbeta, maxbeta);


  //set up gsl_function F
  F.function = &logLnestedSharedBetaShiftGSLmin;
  F.params = regimes;
  
  //set up gsl_min_fminimizer
  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set(s, &F, beta, minbeta, maxbeta);

  //loop over gsl_minimizer
  do {
    iter++;
    status = gsl_min_fminimizer_iterate (s);

    beta = gsl_min_fminimizer_x_minimum (s);
    minbeta = gsl_min_fminimizer_x_lower (s);
    maxbeta = gsl_min_fminimizer_x_upper (s);
     
    status = gsl_min_test_interval(minbeta, maxbeta, 0.001, 0.001);
  } while (status == GSL_CONTINUE && iter < max_iter);

  //save min logL
  minlL = logLnestedSharedBetaShiftGSLmin(beta, regimes);
  //printf("stable minlL is %lf\n", minlL);

  //set regimes beta to min logL beta
  for(i=0; i<ngenes; i++) {
    regimes[i][0][3] = beta;
  }

  //free
  gsl_min_fminimizer_free(s);

  //free globals for logL
  free(gEs);
  for(i=0; i<totnindiv; i++)
    free(gCov[i]);
  free(gCov);
  gsl_vector_free(ggslE);
  gsl_vector_free(ggslexpr);
  gsl_matrix_free(ggslCov);

  free(glEs);
  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);

  free(gsgregimes[0]);
  free(gsgregimes[1]);
  free(gsgregimes);

  free(gspmeans);
  free(gspvars);
  free(gspregs);

  //return min logL
  return(-1.0*minlL);
}

//returns log L computed for fixed beta
double logLFixedBetaStable(double beta, double ***regimes) {
  int i, j;
  double lL, tmplL, scalar;

  //inner mallocs
  gEs = (double*) malloc(totnindiv*sizeof(double));
  gCov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    gCov[i] = (double*) malloc(totnindiv*sizeof(double));
  ggslE = gsl_vector_calloc(totnindiv);
  ggslexpr = gsl_vector_calloc(totnindiv);
  ggslCov = gsl_matrix_calloc(totnindiv, totnindiv);

  glEs = (double*) malloc(nspecies*sizeof(double));
  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));
 
  gsgregimes = (double**) malloc(1*sizeof(double*));
  gsgregimes[0] = (double*) malloc(4*sizeof(double));

  gspmeans = (double*) malloc(nspecies*sizeof(double));
  gspvars = (double*) malloc(nspecies*sizeof(double));
  gspregs = (int*) malloc(nspecies*sizeof(int));

  //get logL with fixed beta
  //for(i=0; i<10; i++)
  //lL = logLnestedSharedBetaStableGSLmin(beta+(.01*i-.05), regimes);
  lL = logLnestedSharedBetaStableGSLmin(beta, regimes);
  //printf("fix beta only lL: %lf  \n", lL);

  /*
  //get logL given fixed beta under various alternative par start vals
  for(i=0; i<20; i++) {
    simRegimes[0][0][1] = gsl_ran_exponential(grng, 1.0);
    simRegimes[0][0][2] = gsl_ran_exponential(grng, 0.5);

    tmplL = logLnestedSharedBetaStableGSLmin(beta, regimes);
    //printf("    sameish lL with sigma2=%lf and alpha=%lf\n", simRegimes[0][0][1], simRegimes[0][0][2]);
    if((tmplL - lL) > .001) {
      printf("!!!rand start did better! old lL=%lf new lL=%lf with sigma2=%lf and alpha=%lf", lL, tmplL, simRegimes[0][0][1], simRegimes[0][0][2]);
      lL = tmplL;
    }
  }

  scalar = 1.0;//gsl_ran_exponential(grng, 1.0);
  simRegimes[0][0][1] = varypopvar*beta*scalar;
  simRegimes[0][0][2] = 0.5*scalar;
  tmplL = logLnestedSharedBetaStableGSLmin(beta, regimes);
  if((tmplL - lL) > .001) {
    printf("!!!vary popvar start did better! old lL=%lf new lL=%lf with sigma2=%lf and alpha=%lf", lL, tmplL, simRegimes[0][0][1], simRegimes[0][0][2]);
    lL = tmplL;
  }

  simRegimes[0][0][1] = varyevolvar*scalar;
  simRegimes[0][0][2] = 0.5*scalar;
  tmplL = logLnestedSharedBetaStableGSLmin(beta, regimes);
  if((tmplL - lL) > .001) {
    printf("!!!vary evolvar start did better! old lL=%lf new lL=%lf with sigma2=%lf and alpha=%lf", lL, tmplL, simRegimes[0][0][1], simRegimes[0][0][2]);
    lL = tmplL;
  }
  */


  //free globals
  free(gEs);
  for(i=0; i<totnindiv; i++)
    free(gCov[i]);
  free(gCov);
  gsl_vector_free(ggslE);
  gsl_vector_free(ggslexpr);
  gsl_matrix_free(ggslCov);

  free(glEs);
  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);

  free(gsgregimes[0]);
  free(gsgregimes);

  free(gspmeans);
  free(gspvars);
  free(gspregs);

  //return min logL
  return(-1.0*lL);
}



//returns log L computed for fixed beta
double logLFixedBetaShift(double beta, double ***regimes) {
  int i, j;
  double lL, tmplL, scalar;

  //inner mallocs
  gEs = (double*) malloc(totnindiv*sizeof(double));
  gCov = (double**) malloc(totnindiv*sizeof(double*));
  for(i=0; i<totnindiv; i++)
    gCov[i] = (double*) malloc(totnindiv*sizeof(double));
  ggslE = gsl_vector_calloc(totnindiv);
  ggslexpr = gsl_vector_calloc(totnindiv);
  ggslCov = gsl_matrix_calloc(totnindiv, totnindiv);

  glEs = (double*) malloc(nspecies*sizeof(double));
  glCov = (double**) malloc(nspecies*sizeof(double*));
  for(i=0; i<nspecies; i++)
    glCov[i] = (double*) malloc(nspecies*sizeof(double));
  gVars = (double*) malloc((2*nspecies-1)*sizeof(double));
 
  gsgregimes = (double**) malloc(2*sizeof(double*));
  gsgregimes[0] = (double*) malloc(4*sizeof(double));
  gsgregimes[1] = (double*) malloc(4*sizeof(double));

  gspmeans = (double*) malloc(nspecies*sizeof(double));
  gspvars = (double*) malloc(nspecies*sizeof(double));
  gspregs = (int*) malloc(nspecies*sizeof(int));

  //get logL with fixed beta
  lL = logLnestedSharedBetaShiftGSLmin(beta, regimes);
  //printf("in logLFixedBetaShift, returning %lf\n", (-1.0*lL));


  //free globals
  free(gEs);
  for(i=0; i<totnindiv; i++)
    free(gCov[i]);
  free(gCov);
  gsl_vector_free(ggslE);
  gsl_vector_free(ggslexpr);
  gsl_matrix_free(ggslCov);

  free(glEs);
  for(i=0; i<nspecies; i++)
    free(glCov[i]);
  free(glCov);
  free(gVars);

  free(gsgregimes[0]);
  free(gsgregimes[1]);
  free(gsgregimes);

  free(gspmeans);
  free(gspvars);
  free(gspregs);

  //return min logL
  return(-1.0*lL);
}




