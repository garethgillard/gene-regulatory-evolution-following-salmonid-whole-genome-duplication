/* stat functions i've written (or taken from nr) */

#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<float.h>
#include "myio.h"


#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>


int findmaxgenei; //gene index considered in findmax
//int ndnorm; //tmp var to count number of times dnorm's called

//my own min function
double dmin(double a, double b) {
  if(a<b)
    return a; 
  else
    return b;
}

//my own min function
int imin(int a, int b) {
  if(a<b)
    return a; 
  else
    return b;
}

//my own array max function (works for positive ints)
int maxIntArr(int *arr, int len) {
  int i, max=0;
  
  for(i=0; i<len; i++) 
    if(arr[i] > max)
      max = arr[i];
  
  return max;
}

//my own factorial function
long double factorial(int n) {
  int zero=0;
  if(n==zero) return 1;
  long double k = n;
  while(--n > zero) k*=n;
  return k;
}

//my own log factorial function
long double logfactorial(int n) {
  int zero=0;
  if(n==zero) return 0.0;
  long double k = log(n);
  while(--n > zero) k+=log(n);
  return k;
}

//returns multinomial of n choose k1, k2, k3
int trinomial(int n, int k1, int k2, int k3) {
  int ans = factorial(n)/(factorial(k1)*factorial(k2)*factorial(k3));
  return ans;  
}

//returns multinomial of n choose k1, k2, k3
long double logtrinomial(int n, int k1, int k2, int k3) {
  long double ans;

  ans = logfactorial(n) - (logfactorial(k1)+logfactorial(k2)+logfactorial(k3));
  
  return ans;
}

//returns binomial n choose k
int choose(int n, int k) {
  int ans = factorial(n)/(factorial(k)*factorial(n-k));
  return ans;
}

//returns log of binomial n choose k
long double logchoose(int n, int k) {
  long double ans = logfactorial(n) - (logfactorial(k)+logfactorial(n-k));
  return ans;
}

//rounds to the nearest integer
long long int mylonground(long double a) {
  long long int ans;

  if(a - floorl(a) < 0.5)
    ans = floorl(a);
  else 
    ans = ceill(a);

  return ans;
}

//rounds to the nearest integer
int myround(double a) {
  int ans;

  if(a - floor(a) < 0.5)
    ans = (int) floor(a);
  else 
    ans = (int) ceil(a);

  return ans;
}

void ldswap(long double *a, long double *b) {
  long double t=*a;
  *a=*b;
  *b=t;
}

void iswap(int *a, int *b) {
  int t=*a;
  *a=*b;
  *b=t;
}

void ldsort(long double arr[], int arr2[], int beg, int end) {
  if (end > beg + 1) {
    long double piv = arr[beg];
    int l = beg + 1, r = end;

    while (l < r) {
      if (arr[l] <= piv)
	l++;
      else {
	ldswap(&arr[l], &arr[--r]);
	iswap(&arr2[l], &arr2[r]);
      }
    }
    ldswap(&arr[--l], &arr[beg]);
    iswap(&arr2[l], &arr2[beg]);
    ldsort(arr, arr2, beg, l);
    ldsort(arr, arr2, r, end);
  }
}

void ldldsort(long double arr[], long double arr2[], int beg, int end) {
  if (end > beg + 1) {
    long double piv = arr[beg];
    int l = beg + 1, r = end;
    
    while (l < r) {
      if (arr[l] <= piv)
	l++;
      else {
	ldswap(&arr[l], &arr[--r]);
	ldswap(&arr2[l], &arr2[r]);
      }
    }
    ldswap(&arr[--l], &arr[beg]);
    ldswap(&arr2[l], &arr2[beg]);
    ldldsort(arr, arr2, beg, l);
    ldldsort(arr, arr2, r, end);
  }
}


void ldldldsort(long double arr[], long double arr2[], long double arr3[], int beg, int end) {
  if (end > beg + 1) {
    //printLDouArr(arr, end-beg);

    long double piv = arr[beg];
    int l = beg + 1, r = end;
    
    while (l < r) {
      if (arr[l] <= piv)
	l++;
      else {
	ldswap(&arr[l], &arr[--r]);
	ldswap(&arr2[l], &arr2[r]);
	ldswap(&arr3[l], &arr3[r]);
      }
    }
    ldswap(&arr[--l], &arr[beg]);
    ldswap(&arr2[l], &arr2[beg]);
    ldswap(&arr3[l], &arr3[beg]);
    ldldldsort(arr, arr2, arr3, beg, l);
    ldldldsort(arr, arr2, arr3, r, end);
  }
}

void ldldldldsort(long double arr[], long double arr2[], long double arr3[], long double arr4[], int beg, int end) {
  if (end > beg + 1) {
    //printLDouArr(arr, end-beg);

    long double piv = arr[beg];
    int l = beg + 1, r = end;
    
    while (l < r) {
      if (arr[l] <= piv)
	l++;
      else {
	ldswap(&arr[l], &arr[--r]);
	ldswap(&arr2[l], &arr2[r]);
	ldswap(&arr3[l], &arr3[r]);
  	ldswap(&arr4[l], &arr4[r]);
    }
    }
    ldswap(&arr[--l], &arr[beg]);
    ldswap(&arr2[l], &arr2[beg]);
    ldswap(&arr3[l], &arr3[beg]);
    ldswap(&arr4[l], &arr4[beg]);
    ldldldldsort(arr, arr2, arr3, arr4, beg, l);
    ldldldldsort(arr, arr2, arr3, arr4, r, end);
  }
}

void isort(int arr[], long double arr2[], long double arr3[], int beg, int end) {
  if (end > beg + 1) {
    int piv = arr[beg];
    int l = beg + 1, r = end;
    
    while (l < r) {
      if (arr[l] <= piv)
	l++;
      else {
	iswap(&arr[l], &arr[--r]);
	ldswap(&arr2[l], &arr2[r]);
	ldswap(&arr3[l], &arr3[r]);
      }
    }
    iswap(&arr[--l], &arr[beg]);
    ldswap(&arr2[l], &arr2[beg]);
    ldswap(&arr3[l], &arr3[beg]);
    isort(arr, arr2, arr3, beg, l);
    isort(arr, arr2, arr3, r, end);
  }
}

//shuffles elements of an int array
void shuffle(int *arr, int imax) {
  int i, swapi;
  int tmp;

  for(i=0; i<imax; i++) {
    swapi = i + (int)((imax-i)*(random()/((double)RAND_MAX+1.0)));

    tmp = arr[i];
    arr[i] = arr[swapi];
    arr[swapi] = tmp;
  }
}

//returns the number of zero entries in the 2d double matrix
int zeroCount2dDouArr(double **mat, int nrow, int ncol) {
  int i, j;
  int nzeros = 0;

  for(i=0; i<nrow; i++) {
    for(j=0; j<ncol; j++) {
      if(mat[i][j] == 0.0) {
	nzeros++;
      }
    }
  }

  return nzeros;
}

//returns the sum of the matrix
double matrixSum(double **m, int nrow, int ncol) {
  int i, j;
  double sum = 0.0;

  for(i=0; i<nrow; i++) {
    for(j=0; j<ncol; j++) {
      sum += m[i][j];
    }
  }

  return sum;
}


//returns 1 when t is in array a, 0 otherwise
int inList(double t, double *arr, int arrlen) {
  int i; 
  
  for(i=0; i<arrlen; i++) {
    if(arr[i] == t) 
      return 1;
  }
  return 0;
}

//returns index of t in arr
int indexOf(double t, double *arr, int arrlen) {
  int i; 
  for(i=0; i<arrlen; i++) {
    if(arr[i] == t) 
      return i;
  }
  return -1;
}

//returns index of t in arr
int indexOfIntArr(int t, int *arr, int arrlen) {
  int i; 
  for(i=0; i<arrlen; i++) {
    if(arr[i] == t) 
      return i;
  }
  return -1;
}

//returns 1 if the vectors are equal, 0 otherwise
int vectorEquals(double *arr1, double *arr2, int arrlen) {
  int i; 

  for(i=0; i<arrlen; i++) 
    if(arr1[i] != arr2[i])
      return 0;
  
  return 1;
}

//returns 1 when t is in array a, 0 otherwise
int inStrList(char *t, char **arr, int arrlen) {
  int i; 
  
  for(i=0; i<arrlen; i++) {
    if(strcmp(t, arr[i]) == 0) 
      return 1;
  }
  return 0;
}



//takes an upper triangle matrix and puts it in an array by rows 
void tri2arrDou(double **mat, int matlen, double *arr) {
  int i, j, arri;
  
  arri = 0;
  for(i=0; i<matlen; i++) {
    for(j=0; j<(matlen-i); j++) {
      arr[arri] = mat[i][j];
      arri++;
    }
  }
}

//takes a vector and puts it in an upper triangle matrix by rows 
void arr2triDou(double **mat, int matlen, double *arr) {
  int i, j, arri;
  
  arri = 0;
  for(i=0; i<matlen; i++) {
    for(j=0; j<(matlen-i); j++) {
      mat[i][j] = arr[arri];
      arri++;
    }
  }
}

//takes an upper triangle matrix and puts it in an array by rows 
void tri2arrInt(int **mat, int matlen, unsigned int *arr) {
  int i, j, arri;
  
  arri = 0;
  for(i=0; i<matlen; i++) {
    for(j=0; j<(matlen-i); j++) {
      arr[arri] = mat[i][j];
      arri++;
    }
  }
}

//takes a vector and puts it in an upper triangle matrix by rows 
void arr2triInt(int **mat, int matlen, unsigned int *arr) {
  int i, j, arri;
  
  arri = 0;
  for(i=0; i<matlen; i++) {
    for(j=0; j<(matlen-i); j++) {
      mat[i][j] = arr[arri];
      arri++;
    }
  }
}

void removeMatrixRow(double **fromMat, double **toMat, int rowi, int nrow, int ncol) {
  int i, j;
  
  for(i=0; i<rowi; i++) {
    //printf("setting from row %d to row %d\n", i, i);
    for(j=0; j<ncol; j++) 
      toMat[i][j] = fromMat[i][j];
  }

  for(i=(rowi+1); i<nrow; i++) {
    //printf("setting from row %d to row %d\n", i, i-1);
    for(j=0; j<ncol; j++) 
      toMat[i-1][j] = fromMat[i][j];
  }
}

void matrixRow(double **fromMat, double **toMat, int rowi, int ncol) {
  int j;
  for(j=0; j<ncol; j++) 
    toMat[0][j] = fromMat[rowi][j];
}






//returns the mean of arr, -1.0 is missing data symbol
double mymean(double* arr, int n) {
  int i;
  double mean = 0.0;
  int nonmissn = 0;

  for(i=0; i<n; i++)
    if(arr[i] != -1.0) {
      mean += arr[i];
      nonmissn++;
    }

  if(nonmissn > 0)
    return(mean/nonmissn);
  else
    return(-1.0);
}

//returns the sample variance of arr, -1.0 is missing data symbol
double myvar(double* arr, int n) {
  int i;
  double mean, var=0.0;
  int nonmissn = 0;

  mean = mymean(arr, n);

  if(mean == -1.0)
    return(-1.0);

  for(i=0; i<n; i++) 
    if(arr[i] != -1.0) {
      var += (arr[i]-mean)*(arr[i]-mean);
      nonmissn++;
    }

  /*
  if((var/(nonmissn-1.0)) < 0.0) {
    printf("got negative var for "); printDouArr(arr, n);
  }
  printf("gunna return var=%lf for  ", (var/(nonmissn-1.0))); printDouArr(arr, n);
  */
  return(var/(nonmissn-1.0));
}





//returns the log probability density function at x for a normal distribution (mu, sigma2)
double logdnorm(double x, double mu, double sigma2) {
  return(-0.5*log(sigma2*2.0*M_PI) - 1.0*(x-mu)*(x-mu)/(2.0*sigma2));
}

//returns the probability density function at x for a normal distribution (mu, sigma2)
double dnorm(double x, double mu, double sigma2) {
  //  ndnorm++;
  return((1.0/sqrt(sigma2*2.0*M_PI)) *  exp(-1.0*(x-mu)*(x-mu)/(2.0*sigma2)));
}



//given x=log(a), y=log(b), returns log(a+b)
double logAddition(double x, double y) {
  //printf("logAddition: %lf + %lf = %lf\n", x, y, log(exp(x)+exp(y)));
  //return(log(exp(x) + exp(y)));

  
  double sum;

  if(x > y) 
    sum = x + log(1.0 + exp(y - x));
  else 
    sum = y + log(1.0 + exp(x - y));
  
  //printf("logAddition: %lf + %lf = %lf\n", x, y, sum);
  return sum;
  
}

//normalizes a vector of logProbabilities so they (in non-log space) add up to 1.0
void normLogProbs(double *arr, int n) {
  int j;
  double tot;

  tot = arr[0];
  for(j=1; j<n; j++) 
    tot = logAddition(tot, arr[j]);

  for(j=0; j<n; j++) 
    arr[j] -= tot;
}

//sum of logProbs in non-log space
double sumLogProbs(double *arr, int n) {
  int j;
  double tot;

  tot = exp(arr[0]);
  for(j=1; j<n; j++) 
    tot += exp(arr[j]);

  return tot;
}


//normalizes a vector of Probabilities so they add up to 1.0
void normProbs(double *arr, int n) {
  int j;
  double tot;

  tot = arr[0];
  for(j=1; j<n; j++) 
    tot += arr[j];

  for(j=0; j<n; j++) 
    arr[j] /= tot;
}


/***************************************************************************************
 *  Multivariate Normal density function and random number generator
 *  Multivariate Student t density function and random number generator
 *  Wishart random number generator
 *  Using GSL -> www.gnu.org/software/gsl
 *
 *  Copyright (C) 2006  Ralph dos Santos Silva
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  AUTHOR 
 *     Ralph dos Santos Silva,  [EMAIL PROTECTED]
 *     March, 2006       
***************************************************************************************/
/*
#include <math.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
*/
/*****************************************************************************************************************/
/*****************************************************************************************************************/
int rmvnorm(const gsl_rng *r, const int n, const gsl_vector *mean, const gsl_matrix *var, gsl_vector *result){
  /* multivariate normal distribution random number generator */
  /*
   *	n	dimension of the random vetor
   *	mean	vector of means of size n
   *	var	variance matrix of dimension n x n
   *	result	output variable with a sigle random vector normal distribution generation
   */
  int k;
  gsl_matrix *work = gsl_matrix_alloc(n,n);
  
  gsl_matrix_memcpy(work,var);
  gsl_linalg_cholesky_decomp(work);
  
  for(k=0; k<n; k++)
    gsl_vector_set( result, k, gsl_ran_ugaussian(r) );
  
  gsl_blas_dtrmv(CblasLower, CblasNoTrans, CblasNonUnit, work, result);
  gsl_vector_add(result,mean);
  
  gsl_matrix_free(work);
  
  return 0;
}

/*****************************************************************************************************************/
/*****************************************************************************************************************/
//rori modified to return log probability
double dmvnorm(const int n, const gsl_vector *x, const gsl_vector *mean, gsl_matrix *var) {
  /* multivariate normal density function    */
  /*    n	dimension of the random vetor
   *	mean	vector of means of size n
   *	var	variance matrix of dimension n x n   */
  int s, inv_err;
  double ldet, ay;
  gsl_vector *ym = gsl_vector_alloc(n), //for speedup, could declar these alloced types globally
    *xm= gsl_vector_alloc(n);
  gsl_matrix *work = gsl_matrix_alloc(n,n), //scrap matrix
    *varinv = gsl_matrix_alloc(n,n); //var matrix inverted
  gsl_permutation *p = gsl_permutation_alloc(n);
  gsl_matrix_memcpy( work, var );

  //get matrix determinant and  inversion 
  gsl_linalg_LU_decomp( work, p, &s );
  //det = gsl_linalg_LU_det( work, s );
  ldet = gsl_linalg_LU_lndet( work );
  //if matrix isn't invertible, return really negative number
  //if(det == 0.0) { return -10000000000000000000000000000.0; }
  inv_err = gsl_linalg_LU_invert( work, p, varinv );
  //if(inv_err > 0) { printf("failed to invert matrix (error number %d) for gene %d, returning low prob for cov matrix:\n", inv_err, findmaxgenei); printGSLMatrix(var); return -100000000000000000.0; }

  //do matrix math for density  
  gsl_vector_memcpy(xm, x);
  gsl_vector_sub(xm, mean );
  gsl_blas_dsymv(CblasUpper, 1.0, varinv, xm, 0.0, ym);
  gsl_blas_ddot(xm, ym, &ay);

  //printf("det is %le inv section is %le n is %d  ", det, ay, n);
  //printf("part a %lf partb %lf  ", (-0.5*ay), 0.5*(n*log(2.0*M_PI) + log(det)));
  //ay = (-0.5*ay) - 0.5*(n*log(2.0*M_PI) + log(det));
  ay = (-0.5*ay) - 0.5*(n*log(2.0*M_PI) + ldet);
  //printf(" for total logL %le   \n", ay);
  //if(ay > 0)     printf("pos dmvnorm for det %le\n", det);

  //free
  gsl_matrix_free(varinv);
  gsl_matrix_free(work);
  gsl_permutation_free(p);
  gsl_vector_free(xm);
  gsl_vector_free(ym);

  /*
  if(ay == INFINITY) {
    printf("ldet is %le  n is %d  ", ldet, n);
    printf("partb %lf  \n", 0.5*(n*log(2.0*M_PI) + ldet));
  }
  */

  return ay;
}

////////////////////////////////////////////////////////
// converting gsl and regular data types

void douArr2gslVec(double *arr, gsl_vector *gvec, int n) {
  int i;
  for(i=0; i<n; i++) {
    gsl_vector_set(gvec, i, arr[i]);
  }
}

void dou2DArr2gslMat(double **arr, gsl_matrix *gmat, int m, int n) {
  int i, j;
  for(i=0; i<m; i++) {
    for(j=0; j<n; j++) {
      gsl_matrix_set(gmat, i, j, arr[i][j]);
    }
  }
}

////////////////////////////////////////////////////////
// time delay

void delay(unsigned int secs) {
  unsigned int retTime = time(0) + secs;
  while(time(0) < retTime);
}
