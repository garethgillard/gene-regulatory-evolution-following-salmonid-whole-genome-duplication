//declare useful global vars
extern struct node *tree; //phylogeny
extern double **expr; // expression data: expr[gene number][individual number]  
extern int ngenes; //total number of genes in dataset
extern int nspecies; //total number of species in dataset 
extern int *nindivs; //number of individuals in each species in dataset
extern int totnindiv; //total number of individuals in all species


// malloc, setting, and freeing regimes
void setStableRegime(double ***regimes, int rooti);
void freeStableRegime(double **regimes);

void setShiftRegime(double ***regimes, int rooti);
void freeShiftRegime(double **regimes);

void setShiftRegimeEG(double ****regimes, int rooti, int nregimes);
void freeShiftRegimeEG(double ***regimes);


// functions to switch between parameters (for findmax) and regimes (for calculations)
void regShift2par(double **regimes, double *params);
void parShift2reg(double *params, double **regimes);

void regEGShift2par(double ***regimes, double *params);
void parEGShift2reg(double *params, double ***regimes);

void parShiftFixBeta2reg(double *params, double **regimes);
void regShiftFixBeta2par(double **regimes, double *params);
