#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=2_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/WSN/dupA/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2823 -t data/November/WSN/dupA/phylo.tree -i data/November/WSN/dupA/expression.nindiv -d data/November/WSN/dupA/expression.dat -f .testVar -p /November/WSN/dupA/ -v 10