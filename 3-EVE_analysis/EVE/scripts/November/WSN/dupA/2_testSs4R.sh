#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=2_Ss4R
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/WSN/dupA/testSs4R-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -o 4 -o 5 -o 6 -n 2823 -t data/November/WSN/dupA/phylo.tree -i data/November/WSN/dupA/expression.nindiv -d data/November/WSN/dupA/expression.dat -f .testSs4R -p /November/WSN/dupA/ -v 10