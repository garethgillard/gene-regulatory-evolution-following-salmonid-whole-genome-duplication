#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=1_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/WSN/single/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2182 -t data/November/WSN/single/phylo.tree -i data/November/WSN/single/expression.nindiv -d data/November/WSN/single/expression.dat -f .testVar -p /November/WSN/single/ -v 10