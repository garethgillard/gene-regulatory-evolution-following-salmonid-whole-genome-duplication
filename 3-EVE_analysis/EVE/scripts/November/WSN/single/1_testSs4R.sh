#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=1_Ss4R
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/WSN/single/testSs4R-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -o 4 -o 5 -o 6 -n 2182 -t data/November/WSN/single/phylo.tree -i data/November/WSN/single/expression.nindiv -d data/November/WSN/single/expression.dat -f .testSs4R -p /November/WSN/single/ -v 10