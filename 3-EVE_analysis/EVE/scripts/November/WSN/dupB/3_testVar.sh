#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=3_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/WSN/dupB/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2824 -t data/November/WSN/dupB/phylo.tree -i data/November/WSN/dupB/expression.nindiv -d data/November/WSN/dupB/expression.dat -f .testVar -p /November/WSN/dupB/ -v 10