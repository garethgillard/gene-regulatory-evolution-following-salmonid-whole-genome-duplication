#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=3_Ss4R
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/WSN/dupB/testSs4R-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -o 4 -o 5 -o 6 -n 2824 -t data/November/WSN/dupB/phylo.tree -i data/November/WSN/dupB/expression.nindiv -d data/November/WSN/dupB/expression.dat -f .testSs4R -p /November/WSN/dupB/ -v 10