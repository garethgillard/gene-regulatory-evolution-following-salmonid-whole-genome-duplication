#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=11_Salp
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA/testSalp-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 4 -n 2823 -t data/November/BSNsgl/dupA/phylo.tree -i data/November/BSNsgl/dupA/expression.nindiv -d data/November/BSNsgl/dupA/expression.dat -f .testSalp -p /November/BSNsgl/dupA/ -v 10