#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=30_Okis
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB.Okis/testOkis-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -n 2807 -t data/November/BSNsgl/dupB.Okis/phylo.tree -i data/November/BSNsgl/dupB.Okis/expression.nindiv -d data/November/BSNsgl/dupB.Okis/expression.dat -f .testOkis -p /November/BSNsgl/dupB.Okis/ -v 10