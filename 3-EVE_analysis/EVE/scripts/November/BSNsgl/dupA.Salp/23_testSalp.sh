#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=23_Salp
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Salp/testSalp-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 1 -n 2753 -t data/November/BSNsgl/dupA.Salp/phylo.tree -i data/November/BSNsgl/dupA.Salp/expression.nindiv -d data/November/BSNsgl/dupA.Salp/expression.dat -f .testSalp -p /November/BSNsgl/dupA.Salp/ -v 10