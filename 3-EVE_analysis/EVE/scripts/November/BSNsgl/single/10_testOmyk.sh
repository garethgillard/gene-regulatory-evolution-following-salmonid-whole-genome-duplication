#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=10_Omyk
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single/testOmyk-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 5 -n 2182 -t data/November/BSNsgl/single/phylo.tree -i data/November/BSNsgl/single/expression.nindiv -d data/November/BSNsgl/single/expression.dat -f .testOmyk -p /November/BSNsgl/single/ -v 10