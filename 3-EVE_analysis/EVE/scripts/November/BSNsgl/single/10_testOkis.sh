#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=10_Okis
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single/testOkis-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 6 -n 2182 -t data/November/BSNsgl/single/phylo.tree -i data/November/BSNsgl/single/expression.nindiv -d data/November/BSNsgl/single/expression.dat -f .testOkis -p /November/BSNsgl/single/ -v 10