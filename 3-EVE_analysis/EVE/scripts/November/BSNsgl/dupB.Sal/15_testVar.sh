#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=15_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB.Sal/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2756 -t data/November/BSNsgl/dupB.Sal/phylo.tree -i data/November/BSNsgl/dupB.Sal/expression.nindiv -d data/November/BSNsgl/dupB.Sal/expression.dat -f .testVar -p /November/BSNsgl/dupB.Sal/ -v 10