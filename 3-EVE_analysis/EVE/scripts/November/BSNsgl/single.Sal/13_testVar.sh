#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=13_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single.Sal/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2173 -t data/November/BSNsgl/single.Sal/phylo.tree -i data/November/BSNsgl/single.Sal/expression.nindiv -d data/November/BSNsgl/single.Sal/expression.dat -f .testVar -p /November/BSNsgl/single.Sal/ -v 100