#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=27_Omyk
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB.Omyk/testOmyk-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel  -n 2812 -t data/November/BSNsgl/dupB.Omyk/phylo.tree -i data/November/BSNsgl/dupB.Omyk/expression.nindiv -d data/November/BSNsgl/dupB.Omyk/expression.dat -f .testOmyk -p /November/BSNsgl/dupB.Omyk/ -v 10