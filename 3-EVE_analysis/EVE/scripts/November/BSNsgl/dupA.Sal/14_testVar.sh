#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=14_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Sal/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2753 -t data/November/BSNsgl/dupA.Sal/phylo.tree -i data/November/BSNsgl/dupA.Sal/expression.nindiv -d data/November/BSNsgl/dupA.Sal/expression.dat -f .testVar -p /November/BSNsgl/dupA.Sal/ -v 10