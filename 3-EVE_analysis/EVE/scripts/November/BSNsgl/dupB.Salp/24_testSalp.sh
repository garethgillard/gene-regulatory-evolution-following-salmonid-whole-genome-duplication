#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=24_Salp
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB.Salp/testSalp-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 1 -n 2756 -t data/November/BSNsgl/dupB.Salp/phylo.tree -i data/November/BSNsgl/dupB.Salp/expression.nindiv -d data/November/BSNsgl/dupB.Salp/expression.dat -f .testSalp -p /November/BSNsgl/dupB.Salp/ -v 10