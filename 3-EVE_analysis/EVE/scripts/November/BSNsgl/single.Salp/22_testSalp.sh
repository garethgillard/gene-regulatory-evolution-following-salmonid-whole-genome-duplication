#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=22_Salp
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single.Salp/testSalp-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 1 -n 2173 -t data/November/BSNsgl/single.Salp/phylo.tree -i data/November/BSNsgl/single.Salp/expression.nindiv -d data/November/BSNsgl/single.Salp/expression.dat -f .testSalp -p /November/BSNsgl/single.Salp/ -v 10