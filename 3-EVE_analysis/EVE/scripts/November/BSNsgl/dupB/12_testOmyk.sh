#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=12_Omyk
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB/testOmyk-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 5 -n 2824 -t data/November/BSNsgl/dupB/phylo.tree -i data/November/BSNsgl/dupB/expression.nindiv -d data/November/BSNsgl/dupB/expression.dat -f .testOmyk -p /November/BSNsgl/dupB/ -v 10