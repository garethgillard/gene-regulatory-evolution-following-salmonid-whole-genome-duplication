#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=12_Salp
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB/testSalp-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 4 -n 2824 -t data/November/BSNsgl/dupB/phylo.tree -i data/November/BSNsgl/dupB/expression.nindiv -d data/November/BSNsgl/dupB/expression.dat -f .testSalp -p /November/BSNsgl/dupB/ -v 10