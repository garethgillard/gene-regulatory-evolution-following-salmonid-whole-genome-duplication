#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=28_Okis
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single.Okis/testOkis-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -n 2180 -t data/November/BSNsgl/single.Okis/phylo.tree -i data/November/BSNsgl/single.Okis/expression.nindiv -d data/November/BSNsgl/single.Okis/expression.dat -f .testOkis -p /November/BSNsgl/single.Okis/ -v 10