#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=20_Ssal
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Ssal/testSsal-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -n 2809 -t data/November/BSNsgl/dupA.Ssal/phylo.tree -i data/November/BSNsgl/dupA.Ssal/expression.nindiv -d data/November/BSNsgl/dupA.Ssal/expression.dat -f .testSsal -p /November/BSNsgl/dupA.Ssal/ -v 10