#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=20_Drer
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Ssal/testDrer-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 0 -n 2809 -t data/November/BSNsgl/dupA.Ssal/phylo.tree -i data/November/BSNsgl/dupA.Ssal/expression.nindiv -d data/November/BSNsgl/dupA.Ssal/expression.dat -f .testDrer -p /November/BSNsgl/dupA.Ssal/ -v 10