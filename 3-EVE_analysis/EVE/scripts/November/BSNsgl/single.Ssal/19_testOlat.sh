#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=19_Olat
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single.Ssal/testOlat-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 1 -n 2179 -t data/November/BSNsgl/single.Ssal/phylo.tree -i data/November/BSNsgl/single.Ssal/expression.nindiv -d data/November/BSNsgl/single.Ssal/expression.dat -f .testOlat -p /November/BSNsgl/single.Ssal/ -v 10