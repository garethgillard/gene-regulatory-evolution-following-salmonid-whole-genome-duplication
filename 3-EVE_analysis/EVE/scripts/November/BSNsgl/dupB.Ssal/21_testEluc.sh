#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=21_Eluc
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupB.Ssal/testEluc-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 2 -n 2809 -t data/November/BSNsgl/dupB.Ssal/phylo.tree -i data/November/BSNsgl/dupB.Ssal/expression.nindiv -d data/November/BSNsgl/dupB.Ssal/expression.dat -f .testEluc -p /November/BSNsgl/dupB.Ssal/ -v 10