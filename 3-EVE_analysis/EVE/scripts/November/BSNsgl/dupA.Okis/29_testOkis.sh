#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=29_Okis
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Okis/testOkis-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -n 2810 -t data/November/BSNsgl/dupA.Okis/phylo.tree -i data/November/BSNsgl/dupA.Okis/expression.nindiv -d data/November/BSNsgl/dupA.Okis/expression.dat -f .testOkis -p /November/BSNsgl/dupA.Okis/ -v 10