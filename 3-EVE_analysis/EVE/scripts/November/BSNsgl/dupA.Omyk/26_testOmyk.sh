#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=26_Omyk
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Omyk/testOmyk-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel  -n 2810 -t data/November/BSNsgl/dupA.Omyk/phylo.tree -i data/November/BSNsgl/dupA.Omyk/expression.nindiv -d data/November/BSNsgl/dupA.Omyk/expression.dat -f .testOmyk -p /November/BSNsgl/dupA.Omyk/ -v 10