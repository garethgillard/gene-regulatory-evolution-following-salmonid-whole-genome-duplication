#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=25_Omyk
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single.Omyk/testOmyk-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel  -n 2178 -t data/November/BSNsgl/single.Omyk/phylo.tree -i data/November/BSNsgl/single.Omyk/expression.nindiv -d data/November/BSNsgl/single.Omyk/expression.dat -f .testOmyk -p /November/BSNsgl/single.Omyk/ -v 10