#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=16_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/single.Tel/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2177 -t data/November/BSNsgl/single.Tel/phylo.tree -i data/November/BSNsgl/single.Tel/expression.nindiv -d data/November/BSNsgl/single.Tel/expression.dat -f .testVar -p /November/BSNsgl/single.Tel/ -v 10