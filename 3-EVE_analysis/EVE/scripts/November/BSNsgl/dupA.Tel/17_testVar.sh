#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=17_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNsgl/dupA.Tel/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2802 -t data/November/BSNsgl/dupA.Tel/phylo.tree -i data/November/BSNsgl/dupA.Tel/expression.nindiv -d data/November/BSNsgl/dupA.Tel/expression.dat -f .testVar -p /November/BSNsgl/dupA.Tel/ -v 10