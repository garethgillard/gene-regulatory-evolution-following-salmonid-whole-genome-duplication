#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=4_Ss4R
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNcmb/single/testSs4R-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -o 4 -o 5 -o 6 -n 2182 -t data/November/BSNcmb/single/phylo.tree -i data/November/BSNcmb/single/expression.nindiv -d data/November/BSNcmb/single/expression.dat -f .testSs4R -p /November/BSNcmb/single/ -v 10