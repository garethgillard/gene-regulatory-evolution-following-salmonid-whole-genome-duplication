#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=4_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNcmb/single/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2182 -t data/November/BSNcmb/single/phylo.tree -i data/November/BSNcmb/single/expression.nindiv -d data/November/BSNcmb/single/expression.dat -f .testVar -p /November/BSNcmb/single/ -v 10