#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=6_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNcmb/dupB/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2824 -t data/November/BSNcmb/dupB/phylo.tree -i data/November/BSNcmb/dupB/expression.nindiv -d data/November/BSNcmb/dupB/expression.dat -f .testVar -p /November/BSNcmb/dupB/ -v 10