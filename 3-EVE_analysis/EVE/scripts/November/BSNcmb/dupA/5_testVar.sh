#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=5_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNcmb/dupA/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2823 -t data/November/BSNcmb/dupA/phylo.tree -i data/November/BSNcmb/dupA/expression.nindiv -d data/November/BSNcmb/dupA/expression.dat -f .testVar -p /November/BSNcmb/dupA/ -v 10