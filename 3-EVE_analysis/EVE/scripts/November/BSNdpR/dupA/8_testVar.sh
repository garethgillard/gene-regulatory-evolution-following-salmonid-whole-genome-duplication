#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=8_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNdpR/dupA/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2823 -t data/November/BSNdpR/dupA/phylo.tree -i data/November/BSNdpR/dupA/expression.nindiv -d data/November/BSNdpR/dupA/expression.dat -f .testVar -p /November/BSNdpR/dupA/ -v 10