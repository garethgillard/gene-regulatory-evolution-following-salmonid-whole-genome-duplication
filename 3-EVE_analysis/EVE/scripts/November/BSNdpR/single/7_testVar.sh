#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=7_Var
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNdpR/single/testVar-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -S -n 2182 -t data/November/BSNdpR/single/phylo.tree -i data/November/BSNdpR/single/expression.nindiv -d data/November/BSNdpR/single/expression.dat -f .testVar -p /November/BSNdpR/single/ -v 10