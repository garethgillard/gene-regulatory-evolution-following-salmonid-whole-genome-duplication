#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=7_Ss4R
#SBATCH --cpus-per-task=1
#SBATCH --output=logs/November/BSNdpR/single/testSs4R-%A_%a.log

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/users/garethg/lib/lib

./EVEmodel -O -o 3 -o 4 -o 5 -o 6 -n 2182 -t data/November/BSNdpR/single/phylo.tree -i data/November/BSNdpR/single/expression.nindiv -d data/November/BSNdpR/single/expression.dat -f .testSs4R -p /November/BSNdpR/single/ -v 10