//declare useful global vars
extern struct node *tree; //phylogeny
extern double **expr; // expression data: expr[gene number][individual number]  
extern int ngenes; //total number of genes in dataset
extern int nspecies; //total number of species in dataset 
extern int *nindivs; //number of individuals in each species in dataset
extern int totnindiv; //total number of individuals in all species

// tree making(ish) functions
void alloc_tree();
void alloc_expr_data();
void free_expr_data();
void copytree(struct node *totree, struct node *fromtree);

// tree reading functions
int readtree(char *filename);
void writeTree_newick(char *filename, int root);
int readNIndivs(char *filename);

// tree making functions (hardcodestyle)
int threeleaftree(double bl0, double bl1, double bl2, double bl3, int nind0, int nind1, int nind2);

// tree writing functions
void printTreeRegimes(int node);

// tree checking functions
int check_tree(struct node *tree, int root, int nspecies);
double totaltreelength(int node);
