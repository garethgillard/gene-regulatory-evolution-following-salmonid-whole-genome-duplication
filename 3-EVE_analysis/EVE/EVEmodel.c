#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>
#include<time.h>
#include<string.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

//#include"discreteprobabilityfunctions.h"
#include"probabilityfunctions.h"
#include"myio.h"
#include"mystat.h"
#include"bfgs.h"
#include"treefunctions.h"
#include"regimefunctions.h"



//notice that my trees have their root in the ground
struct node {
  int down;
  int up[2];
  double bl;
  int regime;
  int nindiv;
  int level;
};

//some nice globals
struct node *tree; //phylogeny
double **expr; // expression data: expr[gene number][individual number]  
int ngenes; //total number of genes in dataset
int nspecies; //total number of species in dataset 
int totnindiv; //total number of individuals in all species
int findmaxgenei; //gene considered in findmax
double findmaxbeta; //var for fixed beta val to be used in findmax
//double **findmaxgeneparams; //params for all genes for nested findmax
gsl_rng *grng; //random number generator
double ***simRegimes; //regimes used for simulations
int simRegFlag;
double *gmlLs; //max logLs for each gene
int randStartFlag; //1:start findmax_bfgs multiple times with random startparam vals  0:don't
int multiStartFlag; //1:start findmax_bfgs with paramfactor * and / alpha and sigma2   0:don't
int verbose; //0:no in-process stdout, 100:max in-process stdout

/////////////////////////////////////////////////////////////////////////
// run ml functions

//get ml estimate for shared beta
double estMLSharedBeta(double ***oneBetaMLregimes) {

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  setReg0onTree();

  //rough estimate beta
  //simRegimes[0][0][3] = 0.3;

  //under one shared beta  
  logLnestedSharedBetaStable(oneBetaMLregimes);
  //printf("shared beta estimated at %20lf\n", oneBetaMLregimes[0][0][3]);

  //free
  free(gmlLs);

  return(oneBetaMLregimes[0][0][3]);
}


//get ml per gene with fixed shared beta vs varing beta 
void fixBetaVaryBetaML(double fixBeta, double ****oneBetaMLregimes, double ****varyBetaMLregimes, double *fixBetamlLs, double *varyBetamlLs) {
  int i, j, k;
  double maxLogL;
  double **trueexpr, ***trueSimRegimes; 
  int truengenes;

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  //copy expr to trueexpr;
  trueexpr = (double**) malloc(ngenes*sizeof(double*));
  for(i=0; i<ngenes; i++) {
    trueexpr[i] = (double*) malloc(totnindiv*sizeof(double));
    for(j=0; j<totnindiv; j++)
      trueexpr[i][j] = expr[i][j];
  }
  truengenes = ngenes;
  //printf("truengenes is %d\n", truengenes);
  trueSimRegimes = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    trueSimRegimes[i] = (double**) malloc(1*sizeof(double*));
    trueSimRegimes[i][0] = (double*) malloc(4*sizeof(double));
    for(j=0; j<4; j++)
      trueSimRegimes[i][0][j] = simRegimes[i][0][j];
  }

  setReg0onTree();
  
  //for each gene
  findmaxgenei = 0;
  for(i=0; i<truengenes; i++) {
    //set expr to gene i
    matrixRow(trueexpr, expr, i, totnindiv);
    //printf("expr is "); printDouArrR(expr[0], totnindiv);
    ngenes = 1;
    simRegimes[0][0][0] = trueSimRegimes[i][0][0];  simRegimes[0][0][1] = trueSimRegimes[i][0][1];  simRegimes[0][0][2] = trueSimRegimes[i][0][2];  simRegimes[0][0][3] = trueSimRegimes[i][0][3];  
    if(verbose > 99) 
      printf("gene %d starting params: %lf %lf %lf %lf\n", i, simRegimes[0][0][0], simRegimes[0][0][1], simRegimes[0][0][2], simRegimes[0][0][3]);

    //logL for varying and fixedbeta
    //estStableRegimesVaryBeta(&simRegimes);
    varyBetamlLs[i] = logLnestedSharedBetaStable(varyBetaMLregimes[i]);
    //printf("varybeta start pars: "); printDouArr(simRegimes[0][0], 4);
    //estStableRegimesFixedBeta(&simRegimes, fixBeta);
    fixBetamlLs[i] = logLFixedBetaStable(fixBeta, oneBetaMLregimes[i]);
    //printf("fix beta start pars: "); printDouArr(simRegimes[0][0], 4);

    //check for negative log LRs
    if((varyBetamlLs[i] - fixBetamlLs[i]) < -0.001) {
      if(verbose > 90) 
	printf("for gene %d got negative log LR, so redid\n", i);

      simRegimes[0][0][0] = oneBetaMLregimes[i][0][0][0];  
      simRegimes[0][0][1] = oneBetaMLregimes[i][0][0][1];  
      simRegimes[0][0][2] = oneBetaMLregimes[i][0][0][2];  
      simRegimes[0][0][3] = oneBetaMLregimes[i][0][0][3];  
      
      varyBetamlLs[i] = logLnestedSharedBetaStable(varyBetaMLregimes[i]);
    }

    if(verbose > 5) {
      printf("  gene %d LRtest stat is <<%lf>>\n", i, (-2.0*fixBetamlLs[i] + 2.0*varyBetamlLs[i]));
      if(verbose > 20) {
	printf("  fixbeta logL=%lf   vary beta logL=%lf  ", fixBetamlLs[i], varyBetamlLs[i]);
	printf("  fixbeta theta %lf sigma2 %lf alpha %lf stationary var %lf pop var %lf", oneBetaMLregimes[i][0][0][0], oneBetaMLregimes[i][0][0][1], oneBetaMLregimes[i][0][0][2], oneBetaMLregimes[i][0][0][1]/(2.0*oneBetaMLregimes[i][0][0][2]), oneBetaMLregimes[i][0][0][3]*oneBetaMLregimes[i][0][0][1]/(2.0*oneBetaMLregimes[i][0][0][2]) );
	printf("  varybeta theta %lf sigma2 %lf alpha %lf beta %lf stationary var %lf pop var %lf\n", varyBetaMLregimes[i][0][0][0], varyBetaMLregimes[i][0][0][1], varyBetaMLregimes[i][0][0][2], varyBetaMLregimes[i][0][0][3], varyBetaMLregimes[i][0][0][1]/(2.0*varyBetaMLregimes[i][0][0][2]), varyBetaMLregimes[i][0][0][3]*varyBetaMLregimes[i][0][0][1]/(2.0*varyBetaMLregimes[i][0][0][2]) );
      }
    }
  }

  //copy trueexpr back to expr
  for(i=0; i<ngenes; i++) {
    for(j=0; j<totnindiv; j++)
      expr[i][j] = trueexpr[i][j];
  }
  ngenes = truengenes;
  for(i=0; i<ngenes; i++) {
    for(j=0; j<4; j++)
      simRegimes[i][0][j] = trueSimRegimes[i][0][j];
  }

  //free
  for(i=0; i<ngenes; i++) 
    free(trueexpr[i]);
  free(trueexpr);
  for(i=0; i<ngenes; i++) {
    free(trueSimRegimes[i][0]);
    free(trueSimRegimes[i]);
  }
  free(trueSimRegimes);

  free(gmlLs);
}




void twoThetaML(double ****oneThetaMLregimes, double ****twoThetaMLregimes, double *oneThetamlLs, double *twoThetamlLs, int nshiftnodes, int *shiftnodes) {
  int i, j, k;
  double maxLogL;
  double **trueexpr, ***trueSimRegimes; 
  int truengenes;

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  //copy expr to trueexpr
  trueexpr = (double**) malloc(ngenes*sizeof(double*));
  for(i=0; i<ngenes; i++) {
    trueexpr[i] = (double*) malloc(totnindiv*sizeof(double));
    for(j=0; j<totnindiv; j++)
      trueexpr[i][j] = expr[i][j];
  }
  truengenes = ngenes;

  //make simRegimes and copy to trueSimRegimes
  estStableRegimesSharedBeta(&simRegimes);
  trueSimRegimes = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    trueSimRegimes[i] = (double**) malloc(1*sizeof(double*));
    trueSimRegimes[i][0] = (double*) malloc(4*sizeof(double));
    for(j=0; j<4; j++)
      trueSimRegimes[i][0][j] = simRegimes[i][0][j];
  }
  setReg0onTree();

  //for each gene one theta
  if(verbose > 4) { printf("Getting MLs under model of one theta\n"); }
  findmaxgenei = 0;
  ngenes = 1;
  for(i=0; i<truengenes; i++) {
    //set expr to gene i
    matrixRow(trueexpr, expr, i, totnindiv);
    simRegimes[0][0][0] = trueSimRegimes[i][0][0];  simRegimes[0][0][1] = trueSimRegimes[i][0][1];  simRegimes[0][0][2] = trueSimRegimes[i][0][2];  simRegimes[0][0][3] = trueSimRegimes[i][0][3];

    //logL for one theta
    oneThetamlLs[i] = logLnestedSharedBetaStable(oneThetaMLregimes[i]);
    if(verbose > 4) { printf("  gene %d: one theta logL=%lf\n", i, oneThetamlLs[i]); }
    if(verbose > 20) {   printf("  one theta theta %lf sigma2 %lf alpha %lf stationary var %lf pop var %lf", oneThetaMLregimes[i][0][0][0], oneThetaMLregimes[i][0][0][1], oneThetaMLregimes[i][0][0][2], oneThetaMLregimes[i][0][0][1]/(2.0*oneThetaMLregimes[i][0][0][2]), oneThetaMLregimes[i][0][0][3]*oneThetaMLregimes[i][0][0][1]/(2.0*oneThetaMLregimes[i][0][0][2]) ); }
  }
  freeStableRegimeEG(simRegimes);
  for(i=0; i<ngenes; i++) {
    free(trueSimRegimes[i][0]);
    free(trueSimRegimes[i]);
  }
  free(trueSimRegimes);


  //estimate regimes under shift
  ngenes = truengenes;
  estShiftRegimesSharedBeta(&simRegimes);
  trueSimRegimes = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    trueSimRegimes[i] = (double**) malloc(2*sizeof(double*));
    trueSimRegimes[i][0] = (double*) malloc(4*sizeof(double));
    trueSimRegimes[i][1] = (double*) malloc(4*sizeof(double));
    for(j=0; j<4; j++) {
      trueSimRegimes[i][0][j] = simRegimes[i][0][j];
      trueSimRegimes[i][1][j] = simRegimes[i][1][j];
    }
  }
  //print3dDouArr(simRegimes, ngenes, 2, 4);
  //print3dDouArr(trueSimRegimes, ngenes, 2, 4);
  setReg0onTree();
  for(i=0; i<nshiftnodes; i++)
    setBranchRegRecurse(shiftnodes[i], 1); 

  //for each gene two thetas with shift
  if(verbose > 4) { printf("Getting MLs under model of branch-specific shift\n"); }
  findmaxgenei = 0;
  ngenes = 1;
  for(i=0; i<truengenes; i++) {
    //set expr to gene i
    matrixRow(trueexpr, expr, i, totnindiv);
    simRegimes[0][0][0] = trueSimRegimes[i][0][0];  simRegimes[0][0][1] = trueSimRegimes[i][0][1];  simRegimes[0][0][2] = trueSimRegimes[i][0][2];  simRegimes[0][0][3] = trueSimRegimes[i][0][3];
    simRegimes[0][1][0] = trueSimRegimes[i][1][0];  simRegimes[0][1][1] = trueSimRegimes[i][1][1];  simRegimes[0][1][2] = trueSimRegimes[i][1][2];  simRegimes[0][1][3] = trueSimRegimes[i][1][3];

    //logL for two thetas
    twoThetamlLs[i] = logLnestedSharedBetaShift(twoThetaMLregimes[i]);
    if(verbose > 4) { printf("  gene %d: two theta logL=%lf\n", i, twoThetamlLs[i]); }
    if(verbose > 20) { printf("  two theta theta1 %lf theta2 %lf sigma2 %lf alpha %lf stationary var %lf pop var %lf\n", twoThetaMLregimes[i][0][0][0], twoThetaMLregimes[i][0][1][0], twoThetaMLregimes[i][0][0][1], twoThetaMLregimes[i][0][0][2], twoThetaMLregimes[i][0][0][1]/(2.0*twoThetaMLregimes[i][0][0][2]), twoThetaMLregimes[i][0][0][3]*twoThetaMLregimes[i][0][0][1]/(2.0*twoThetaMLregimes[i][0][0][2]) ); }
  }
  freeShiftRegimeEG(simRegimes);
  for(i=0; i<ngenes; i++) {
    free(trueSimRegimes[i][0]);
    free(trueSimRegimes[i][1]);
    free(trueSimRegimes[i]);
  }
  free(trueSimRegimes);

  //copy trueexpr back to expr
  for(i=0; i<ngenes; i++) {
    for(j=0; j<totnindiv; j++)
      expr[i][j] = trueexpr[i][j];
  }
  ngenes = truengenes;

  //free
  for(i=0; i<ngenes; i++) 
    free(trueexpr[i]);
  free(trueexpr);

  free(gmlLs);
}


void twoThetaSpeciesMeanML(double ****oneThetaMLregimes, double ****twoThetaMLregimes, double *oneThetamlLs, double *twoThetamlLs, int nshiftnodes, int *shiftnodes) {
  int i, j, k;
  double maxLogL;
  double **trueexpr, ***trueSimRegimes; 
  int truengenes;
  double tmp; 

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  //copy expr to trueexpr
  trueexpr = (double**) malloc(ngenes*sizeof(double*));
  for(i=0; i<ngenes; i++) {
    trueexpr[i] = (double*) malloc(totnindiv*sizeof(double));
    for(j=0; j<totnindiv; j++)
      trueexpr[i][j] = expr[i][j];
  }
  truengenes = ngenes;

  //make simRegimes and copy to trueSimRegimes
  estStableRegimesSharedBeta(&simRegimes);
  trueSimRegimes = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    trueSimRegimes[i] = (double**) malloc(1*sizeof(double*));
    trueSimRegimes[i][0] = (double*) malloc(4*sizeof(double));
    for(j=0; j<4; j++)
      trueSimRegimes[i][0][j] = simRegimes[i][0][j];
  }
  setReg0onTree();

  //for each gene one theta
  findmaxgenei = 0;
  ngenes = 1;
  for(i=0; i<truengenes; i++) {
    //set expr to gene i
    matrixRow(trueexpr, expr, i, totnindiv);
    simRegimes[0][0][0] = trueSimRegimes[i][0][0];  simRegimes[0][0][1] = trueSimRegimes[i][0][1];  simRegimes[0][0][2] = trueSimRegimes[i][0][2];  simRegimes[0][0][3] = trueSimRegimes[i][0][3];

    //logL for one theta
    oneThetamlLs[i] = logLFixedBetaStable(0, oneThetaMLregimes[i]); //logLnestedSharedBetaStable(oneThetaMLregimes[i]);
    if(verbose > 4) { printf("  gene %d: one theta logL=%lf\n", i, oneThetamlLs[i]); }
    if(verbose > 20) {   printf("  one theta theta %lf sigma2 %lf alpha %lf stationary var %lf pop var %lf\n", oneThetaMLregimes[i][0][0][0], oneThetaMLregimes[i][0][0][1], oneThetaMLregimes[i][0][0][2], oneThetaMLregimes[i][0][0][1]/(2.0*oneThetaMLregimes[i][0][0][2]), oneThetaMLregimes[i][0][0][3]*oneThetaMLregimes[i][0][0][1]/(2.0*oneThetaMLregimes[i][0][0][2]) ); }
  }
  freeStableRegimeEG(simRegimes);
  for(i=0; i<ngenes; i++) {
    free(trueSimRegimes[i][0]);
    free(trueSimRegimes[i]);
  }
  free(trueSimRegimes);


  //estimate regimes under shift
  ngenes = truengenes;
  estShiftRegimesFixedBeta(&simRegimes, 0.0);
  trueSimRegimes = (double***) malloc(ngenes*sizeof(double**));
  for(i=0; i<ngenes; i++) {
    trueSimRegimes[i] = (double**) malloc(2*sizeof(double*));
    trueSimRegimes[i][0] = (double*) malloc(4*sizeof(double));
    trueSimRegimes[i][1] = (double*) malloc(4*sizeof(double));
    for(j=0; j<4; j++) {
      trueSimRegimes[i][0][j] = simRegimes[i][0][j];
      trueSimRegimes[i][1][j] = simRegimes[i][1][j];
    }
  }
  //print3dDouArr(simRegimes, ngenes, 2, 4);
  //print3dDouArr(trueSimRegimes, ngenes, 2, 4);
  setReg0onTree();
  for(i=0; i<nshiftnodes; i++)
    setBranchRegRecurse(shiftnodes[i], 1); 

  //for each gene two thetas with shift
  findmaxgenei = 0;
  ngenes = 1;
  for(i=0; i<truengenes; i++) {
    //set expr to gene i
    matrixRow(trueexpr, expr, i, totnindiv);
    //simRegimes[0][0][0] = trueSimRegimes[i][0][0];  simRegimes[0][0][1] = trueSimRegimes[i][0][1];  simRegimes[0][0][2] = trueSimRegimes[i][0][2];  simRegimes[0][0][3] = trueSimRegimes[i][0][3];
    //simRegimes[0][1][0] = trueSimRegimes[i][1][0];  simRegimes[0][1][1] = trueSimRegimes[i][1][1];  simRegimes[0][1][2] = trueSimRegimes[i][1][2];  simRegimes[0][1][3] = trueSimRegimes[i][1][3];

    simRegimes[0][0][0] = oneThetaMLregimes[i][0][0][0];  simRegimes[0][0][1] = oneThetaMLregimes[i][0][0][1];  simRegimes[0][0][2] = oneThetaMLregimes[i][0][0][2];  simRegimes[0][0][3] = oneThetaMLregimes[i][0][0][3];
    simRegimes[0][1][0] = oneThetaMLregimes[i][0][0][0];  simRegimes[0][1][1] = oneThetaMLregimes[i][0][0][1];  simRegimes[0][1][2] = oneThetaMLregimes[i][0][0][2];  simRegimes[0][1][3] = oneThetaMLregimes[i][0][0][3];


    //logL for two thetas
    twoThetamlLs[i] = logLFixedBetaShift(0.0, twoThetaMLregimes[i]); 
    if(verbose > 4) { printf("  gene %d: two theta logL=%lf\n", i, twoThetamlLs[i]); }
    if(verbose > 20) { printf("  two theta theta1 %lf theta2 %lf sigma2 %lf alpha %lf stationary var %lf pop var %lf\n", twoThetaMLregimes[i][0][0][0], twoThetaMLregimes[i][0][1][0], twoThetaMLregimes[i][0][0][1], twoThetaMLregimes[i][0][0][2], twoThetaMLregimes[i][0][0][1]/(2.0*twoThetaMLregimes[i][0][0][2]), twoThetaMLregimes[i][0][0][3]*twoThetaMLregimes[i][0][0][1]/(2.0*twoThetaMLregimes[i][0][0][2]) ); }
  }
  freeShiftRegimeEG(simRegimes);
  for(i=0; i<ngenes; i++) {
    free(trueSimRegimes[i][0]);
    free(trueSimRegimes[i][1]);
    free(trueSimRegimes[i]);
  }
  free(trueSimRegimes);

  //copy trueexpr back to expr
  for(i=0; i<ngenes; i++) {
    for(j=0; j<totnindiv; j++)
      expr[i][j] = trueexpr[i][j];
  }
  ngenes = truengenes;

  //free
  for(i=0; i<ngenes; i++) 
    free(trueexpr[i]);
  free(trueexpr);

  free(gmlLs);
}


//given parameter files, does parameteric bootstrap   for Ha: beta_i != beta_shared test
//path is parameter file folder path after 'results/' and bootstrap data file folder path after data/bootstrap/
//resfile is the parameter file extension before '.res' [can be empty]
int parametric_bootstrap(char* path, char* resfile, int ng, char* treefilename, char* nindivfilename, int nbootstraps) {
  int i, rooti;
  char filename[1000];
  char oneBetaMLsFile[1000], varyBetaMLsFile[1000], oneBetaRegimesFile[1000], varyBetaRegimesFile[1000], LRTsFile[1000];
  double sharedBeta;
  double boottheta, bootsigma2, bootalpha, bootbeta;
  //double *fixBetamlLs, *varyBetamlLs;
  double *bootfixBetamlLs, *bootvaryBetamlLs;
  double ****fixBetaDataMLregimes, ****varyBetaDataMLregimes;
  double ****bootfixBetaDataMLregimes, ****bootvaryBetaDataMLregimes;
  int truengenes;

  //set some consts and read in tree
  ngenes = ng; 
  truengenes = ngenes;
  nspecies = readtree(treefilename);
  totnindiv = readNIndivs(nindivfilename);
  rooti = 2*nspecies-2;
  if(verbose>8) debugprinttree(rooti);

  //malloc and read in parameters for bootstrap
  setPerGeneRegimeEG(&fixBetaDataMLregimes, rooti);
  setPerGeneRegimeEG(&varyBetaDataMLregimes, rooti);
  sprintf(oneBetaRegimesFile, "results/%ssharedBetaMLparams%s.res", path, resfile);
  sprintf(varyBetaRegimesFile, "results/%sindivBetaMLparams%s.res", path, resfile);
  read4dDouArr(fixBetaDataMLregimes, ngenes, 1, 1, 4, oneBetaRegimesFile);
  read4dDouArr(varyBetaDataMLregimes, ngenes, 1, 1, 4, varyBetaRegimesFile);
  sharedBeta = fixBetaDataMLregimes[0][0][0][3];

  //do parametric bootstrap for each gene
  ngenes = nbootstraps;
  alloc_expr_data();
  for(i=0; i<truengenes; i++) {

    //set params for sim to gene i one beta fix params
    boottheta = fixBetaDataMLregimes[i][0][0][0];
    bootsigma2 = fixBetaDataMLregimes[i][0][0][1];
    bootalpha = fixBetaDataMLregimes[i][0][0][2];
    bootbeta = fixBetaDataMLregimes[i][0][0][3];
    
    //simulate expr
    setStableRegimes(&simRegimes, boottheta, bootsigma2, bootalpha, bootbeta);
    if(verbose>5) printf("====simulating %d genes for bootstrap for gene %d\n", nbootstraps, i);
    simExprEachGene();

    //print bootstrap simulated expr to file
    sprintf(filename, "data/%sbootstrap_sim%s_gene%d.dat", path, resfile, i);
    write2dDouArr(expr, nbootstraps, totnindiv, filename);
    if(verbose>6) printf("  printed data to %s\n", filename);

    //estimate parameter values
    estStableRegimesVaryBeta(&simRegimes);

    //set up ml regimes
    setPerGeneRegimeEG(&bootfixBetaDataMLregimes, rooti);
    setPerGeneRegimeEG(&bootvaryBetaDataMLregimes, rooti);
    bootfixBetamlLs = (double*) malloc(nbootstraps*sizeof(double));
    bootvaryBetamlLs = (double*) malloc(nbootstraps*sizeof(double));

    //do LR test
    if(verbose>4) printf("====testing beta shared vs vary for each gene in bootstrap for original gene %d\n", i);
    fixBetaVaryBetaML(sharedBeta, bootfixBetaDataMLregimes, bootvaryBetaDataMLregimes, bootfixBetamlLs, bootvaryBetamlLs);

    //write results to file
    sprintf(oneBetaRegimesFile, "results/%sbootstrap_sharedBetaMLparams%s_gene%d_boots%d.res", path, resfile, i, nbootstraps);
    sprintf(varyBetaRegimesFile, "results/%sbootstrap_indivBetaMLparams%s_gene%d_boots%d.res", path, resfile, i, nbootstraps);
    sprintf(oneBetaMLsFile, "results/%sbootstrap_sharedBetaMLs%s_gene%d_boots%d.res", path, resfile, i, nbootstraps);
    sprintf(varyBetaMLsFile, "results/%sbootstrap_indivBetaMLs%s_gene%d_boots%d.res", path, resfile, i, nbootstraps);
    sprintf(LRTsFile, "results/%sbootstrap_betaTestLRTs%s_gene%d_boots%d.res", path, resfile, i, nbootstraps);
    write4dDouArr(bootfixBetaDataMLregimes, nbootstraps, 1, 1, 4, oneBetaRegimesFile);
    write4dDouArr(bootvaryBetaDataMLregimes, nbootstraps, 1, 1, 4, varyBetaRegimesFile);
    writeDouArr(bootfixBetamlLs, nbootstraps, oneBetaMLsFile);
    writeDouArr(bootvaryBetamlLs, nbootstraps, varyBetaMLsFile);
    writeLRTArr(bootfixBetamlLs, bootvaryBetamlLs, ngenes, LRTsFile);

    //free
    freePerGeneRegimeEG(bootfixBetaDataMLregimes);
    freePerGeneRegimeEG(bootvaryBetaDataMLregimes);
    freeStableRegimeEG(simRegimes);
    free(bootfixBetamlLs);
    free(bootvaryBetamlLs);
  }

  //free
  ngenes = truengenes;
  freePerGeneRegimeEG(fixBetaDataMLregimes);
  freePerGeneRegimeEG(varyBetaDataMLregimes);  
  free_expr_data();

  ngenes = truengenes;

  return(0);
}

//do the simple beta_shared vs beta_i test for each gene in specified datafiles
//path is results file folder path after 'results/'
//resfile is the parameter file extension before '.res' [can be empty]
int experimental_data_shared_beta_test(char* path, char* resfile, int ng, char* treefilename, char* nindivfilename, char* exprfilename) {
  int i, rooti;
  char filename[1000];
  char oneBetaMLsFile[1000], varyBetaMLsFile[1000], oneBetaRegimesFile[1000], varyBetaRegimesFile[1000], LRTsFile[1000];
  double sharedBeta;
  double *fixBetamlLs, *varyBetamlLs;
  double ***oneBetaDataMLregimes, ****fixBetaDataMLregimes, ****varyBetaDataMLregimes;
  int truengenes;
  int maxgenename = 100;
  char **genenames;
  clock_t time0, time1; 

  //read/make/write tree
  ngenes = ng; 
  nspecies = readtree(treefilename);
  totnindiv = readNIndivs(nindivfilename);
  rooti = 2*nspecies-2;
  if(verbose>0) printf("Read in tree from %s\n", treefilename);
  if(verbose>8) debugprinttree(rooti);

  //set up expr data structs
  alloc_expr_data();
  genenames = (char**) malloc(ngenes*sizeof(char*));
  for(i=0; i<ngenes; i++) 
    genenames[i] = (char*) malloc(maxgenename*sizeof(char));
  fixBetamlLs = (double*) malloc(ngenes*sizeof(double));
  varyBetamlLs = (double*) malloc(ngenes*sizeof(double));

  //read in expr from file
  read2dDouArrRowNames(expr, genenames, totnindiv, exprfilename);
  //read2dDouArrNoHeaders(expr, ngenes, totnindiv, exprfilename);  //ALERT, CHANGE THIS DEPENDING ON INPUT FORMAT
  //print2dDouArr(expr, ngenes, totnindiv);
  //printStringArr(genenames, ngenes);
  if(verbose>0) printf("Read %d genes over %d individuals from file %s\n", ngenes, totnindiv, exprfilename);

  //malloc and set up regimes
  setStableRegimeEG(&oneBetaDataMLregimes, rooti); 
  setPerGeneRegimeEG(&fixBetaDataMLregimes, rooti);
  setPerGeneRegimeEG(&varyBetaDataMLregimes, rooti);

  if(verbose>6) printf("Testing shared beta vs gene-specific betas\n");

  //setup onebeta regimes and simulate expr under regimes (as opposed to shift regimes)
  simRegFlag = 2;

  //estStableRegimesSharedBetaFast(&simRegimes);
  estStableRegimesVaryBeta(&simRegimes);
  //print3dDouArr(simRegimes, ngenes, 1, 4);

  //estimate sharedBeta
  time0 = clock();
  sharedBeta = estMLSharedBeta(oneBetaDataMLregimes);
  time1 = clock();
  if(verbose>3) printf("estimated shared beta as %lf in %d seconds\n", sharedBeta, ((int) (((time1-time0)/CLOCKS_PER_SEC))));

  //find ml
  time0 = clock();
  fixBetaVaryBetaML(sharedBeta, fixBetaDataMLregimes, varyBetaDataMLregimes, fixBetamlLs, varyBetamlLs);
  time1 = clock();
  if(verbose>3) printf("did indiv gene tests in %d seconds\n", ((int) (((time1-time0)/CLOCKS_PER_SEC))));
  //printf("did whole sharedbeta test analysis in %d seconds\n", ((int) (((time1-time0)/CLOCKS_PER_SEC))));
  
  //write results to files
  sprintf(oneBetaRegimesFile, "results/%ssharedBetaMLparams%s.res", path, resfile);
  sprintf(varyBetaRegimesFile, "results/%sindivBetaMLparams%s.res", path, resfile);
  sprintf(oneBetaMLsFile, "results/%ssharedBetaMLs%s.res", path, resfile);
  sprintf(varyBetaMLsFile, "results/%sindivBetaMLs%s.res", path, resfile);
  sprintf(LRTsFile, "results/%sbetaTestLRTs%s.res", path, resfile);
  write4dDouArr(fixBetaDataMLregimes, ngenes, 1, 1, 4, oneBetaRegimesFile);
  write4dDouArr(varyBetaDataMLregimes, ngenes, 1, 1, 4, varyBetaRegimesFile);
  writeDouArr(fixBetamlLs, ngenes, oneBetaMLsFile);
  writeDouArr(varyBetamlLs, ngenes, varyBetaMLsFile);
  writeLRTArr(fixBetamlLs, varyBetamlLs, ngenes, LRTsFile);

  if(verbose > 3) {
    printf("wrote results to:\n\t%s\n\t%s\n\t%s\n\t%s\n", oneBetaRegimesFile, varyBetaRegimesFile, oneBetaMLsFile, varyBetaMLsFile);
  }

  //print info on multi and rand starts
  if(multiStartFlag > 0 && verbose > 30) {
    printf("Number of exaggerated parameter start points that did better than estimated start points: %d\n", (multiStartFlag-1));
  }
  if(randStartFlag > 0 && verbose > 30) {
    printf("Number of random parameter start points that did better than estimated start points: %d\n", (randStartFlag-1));
  }

  //free
  //freeStableRegimeEG(simRegimes);
  freeStableRegimeEG(oneBetaDataMLregimes);
  freePerGeneRegimeEG(fixBetaDataMLregimes);
  freePerGeneRegimeEG(varyBetaDataMLregimes);  
  free_expr_data();
  free(fixBetamlLs);
  free(varyBetamlLs);
  for(i=0; i<ngenes; i++) 
    free(genenames[i]);
  free(genenames);

  return(0);
}


//do the branch-specific per gene beta_1 =? beta_2 test
//path is results file folder path after 'results/'
//resfile is the parameter file extension before '.res' [can be empty]
int experimental_data_branch_specific_theta_test(char* path, char* resfile, int ng, char* treefilename, char* nindivfilename, char* exprfilename, int* shiftnodes, int nshiftnodes) {
  int i, rooti;
  char filename[1000];
  char oneThetaMLsFile[1000], twoThetaMLsFile[1000], oneThetaRegimesFile[1000], twoThetaRegimesFile[1000], LRTsFile[1000];
  double *oneThetamlLs, *twoThetamlLs;
  double ****oneThetaMLregimes, ****twoThetaMLregimes;
  //double oneThetamlL, twoThetamlL;
  int truengenes;
  int maxgenename = 100;
  char **genenames;
  clock_t time0, time1; 

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  //read/make/write tree
  ngenes = ng; 
  nspecies = readtree(treefilename);
  totnindiv = readNIndivs(nindivfilename);
  rooti = 2*nspecies-2;
  if(verbose>0) printf("Read in tree from %s\n", treefilename);
  if(verbose>8) debugprinttree(rooti);

  //set up expr data structs
  alloc_expr_data();
  genenames = (char**) malloc(ngenes*sizeof(char*));
  for(i=0; i<ngenes; i++) 
    genenames[i] = (char*) malloc(maxgenename*sizeof(char));
  oneThetamlLs = (double*) malloc(ngenes*sizeof(double));
  twoThetamlLs = (double*) malloc(ngenes*sizeof(double));

  //read in expr from file
  read2dDouArrRowNames(expr, genenames, totnindiv, exprfilename);
  if(verbose>0) printf("Read %d genes over %d individuals from file %s\n", ngenes, totnindiv, exprfilename);

  //malloc and set up regimes
  setPerGeneRegimeEG(&oneThetaMLregimes, rooti);
  setPerGeneRegimeEGShift(&twoThetaMLregimes, rooti);

  if(verbose>6) printf("Testing constant vs branch-specific thetas per gene\n");

  time0 = clock();
  twoThetaML(oneThetaMLregimes, twoThetaMLregimes, oneThetamlLs, twoThetamlLs, nshiftnodes, shiftnodes);
  time1 = clock();
  if(verbose>4) printf("Finished one vs two theta analysis in %d sec\n", ((int) (((time1-time0)/CLOCKS_PER_SEC))));

  //write results to files
  sprintf(oneThetaRegimesFile, "results/%soneThetaMLparams%s.res", path, resfile);
  sprintf(twoThetaRegimesFile, "results/%stwoBSThetaMLparams%s.res", path, resfile);
  sprintf(oneThetaMLsFile, "results/%soneThetaMLs%s.res", path, resfile);
  sprintf(twoThetaMLsFile, "results/%stwoBSThetaMLs%s.res", path, resfile);
  sprintf(LRTsFile, "results/%sBSThetaTestLRTs%s.res", path, resfile);
  write4dDouArr(oneThetaMLregimes, ngenes, 1, 1, 4, oneThetaRegimesFile);
  write4dDouArr(twoThetaMLregimes, ngenes, 1, 2, 4, twoThetaRegimesFile);
  writeDouArr(oneThetamlLs, ngenes, oneThetaMLsFile);
  writeDouArr(twoThetamlLs, ngenes, twoThetaMLsFile);
  writeLRTArr(oneThetamlLs, twoThetamlLs, ngenes, LRTsFile);

  //print info on multi and rand starts
  if(multiStartFlag > 0 && verbose>30) 
    printf("Number of exaggerated parameter start points that did better than estimated start points: %d\n", (multiStartFlag-1));
  if(randStartFlag > 0 && verbose>30)
    printf("Number of random parameter start points that did better than estimated start points: %d\n", (randStartFlag-1));

  if(verbose > 3) { printf("Wrote results to:\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n", oneThetaRegimesFile, twoThetaRegimesFile, oneThetaMLsFile, twoThetaMLsFile, LRTsFile); }


  //free
  freeStableRegimeEG(oneThetaMLregimes);  
  freeStableRegimeEG(twoThetaMLregimes);
  free_expr_data();
  free(oneThetamlLs);
  free(twoThetamlLs);
  for(i=0; i<ngenes; i++) 
    free(genenames[i]);
  free(genenames);

  return(0);
}



//do the branch-specific per gene beta_1 =? beta_2 test
//path is results file folder path after 'results/'
//resfile is the parameter file extension before '.res' [can be empty]
int experimental_data_branch_specific_species_mean_theta_test(char* path, char* resfile, int ng, char* treefilename, char* nindivfilename, char* exprfilename, int* shiftnodes, int nshiftnodes) {
  int i, j, k, rooti, indivi;
  char filename[1000];
  char oneThetaMLsFile[1000], twoThetaMLsFile[1000], oneThetaRegimesFile[1000], twoThetaRegimesFile[1000], LRTsFile[1000];
  double *oneThetamlLs, *twoThetamlLs;
  double ****oneThetaMLregimes, ****twoThetaMLregimes;
  //double oneThetamlL, twoThetamlL;
  int truengenes;
  int maxgenename = 100;
  char **genenames;
  clock_t time0, time1; 
  double **rawexpr;
  int rawtotnindiv; 

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  //read/make/write tree
  ngenes = ng; 
  nspecies = readtree(treefilename);
  rawtotnindiv = readNIndivs(nindivfilename);
  rooti = 2*nspecies-2;
  if(verbose>0) printf("Read in tree from %s\n", treefilename);
  if(verbose>8) debugprinttree(rooti);

  //read in expr from file
  genenames = (char**) malloc(ngenes*sizeof(char*));
  for(i=0; i<ngenes; i++) 
    genenames[i] = (char*) malloc(maxgenename*sizeof(char));
  rawexpr = (double**) malloc(ngenes*sizeof(double*));
  for(i=0; i<ngenes; i++) 
    rawexpr[i] = (double*) malloc(rawtotnindiv*sizeof(double));
  read2dDouArrRowNames(rawexpr, genenames, rawtotnindiv, exprfilename);  
  if(verbose>0) printf("Read raw expr %d genes over %d individuals from file %s\n", ngenes, rawtotnindiv, exprfilename);
  //printDouArr(rawexpr[0], rawtotnindiv);

  //reduce expr to species means
  totnindiv = nspecies;
  alloc_expr_data();
  for(k=0; k<ngenes; k++) {
    indivi = 0;
    for(i=0; i<nspecies; i++) {
      expr[k][i] = 0.0;
      for(j=0; j<tree[i].nindiv; j++) {
	expr[k][i] += rawexpr[k][indivi];
	//printf("expr[%d][%d] is %lf\n", k, i, expr[k][i]);
	//printf("cur rawexpr[%d][%d] is %lf\n", k, indivi, rawexpr[k][indivi]);
	indivi++;
      }
      expr[k][i] /= 1.0*tree[i].nindiv;
      //printf("final expr[%d][%d] is %lf\n", k, i, expr[k][i]);
    }
  }
  for(i=0; i<nspecies; i++) {
    tree[i].nindiv = 1;
  }
  if(verbose>4) printf("Reduced expr to species mean over %d genes and  %d \"individuals\"\n", ngenes, totnindiv);
  //print2dDouArr(expr, ngenes, totnindiv);

  //set up ml data structs
  oneThetamlLs = (double*) malloc(ngenes*sizeof(double));
  twoThetamlLs = (double*) malloc(ngenes*sizeof(double));

  //malloc and set up regimes
  setPerGeneRegimeEG(&oneThetaMLregimes, rooti);
  setPerGeneRegimeEGShift(&twoThetaMLregimes, rooti);

  if(verbose>6) printf("Testing constant vs branch-specific thetas per gene\n");

  time0 = clock();
  twoThetaSpeciesMeanML(oneThetaMLregimes, twoThetaMLregimes, oneThetamlLs, twoThetamlLs, nshiftnodes, shiftnodes);
  time1 = clock();
  if(verbose>4) printf("Finished one vs two theta analysis in %d sec\n", ((int) (((time1-time0)/CLOCKS_PER_SEC))));

  //write results to files
  sprintf(oneThetaRegimesFile, "results/%sSMoneThetaMLparams%s.res", path, resfile);
  sprintf(twoThetaRegimesFile, "results/%sSMtwoBSThetaMLparams%s.res", path, resfile);
  sprintf(oneThetaMLsFile, "results/%sSMoneThetaMLs%s.res", path, resfile);
  sprintf(twoThetaMLsFile, "results/%sSMtwoBSThetaMLs%s.res", path, resfile);
  sprintf(LRTsFile, "results/%sSMBSThetaTestLRTs%s.res", path, resfile);
  write4dDouArr(oneThetaMLregimes, ngenes, 1, 1, 4, oneThetaRegimesFile);
  write4dDouArr(twoThetaMLregimes, ngenes, 1, 2, 4, twoThetaRegimesFile);
  writeDouArr(oneThetamlLs, ngenes, oneThetaMLsFile);
  writeDouArr(twoThetamlLs, ngenes, twoThetaMLsFile);
  writeLRTArr(oneThetamlLs, twoThetamlLs, ngenes, LRTsFile);

  //print info on multi and rand starts
  if(multiStartFlag > 0 && verbose>30) 
    printf("Number of exaggerated parameter start points that did better than estimated start points: %d\n", (multiStartFlag-1));
  if(randStartFlag > 0 && verbose>30) 
    printf("Number of random parameter start points that did better than estimated start points: %d\n", (randStartFlag-1));

  //free
  freeStableRegimeEG(oneThetaMLregimes);  
  freeStableRegimeEG(twoThetaMLregimes);
  free_expr_data();
  free(oneThetamlLs);
  free(twoThetamlLs);
  for(i=0; i<ngenes; i++) 
    free(genenames[i]);
  free(genenames);
  for(i=0; i<ngenes; i++)
    free(rawexpr[i]);
  free(rawexpr);

  return(0);
}





//do the branch-specific per gene theta_1 =? theta_2 test on data simulated from indicated parameter files
//path is results file folder path after 'results/'
//resfile is the parameter file extension before '.res' [can be empty]
int simulate_data_branch_specific_theta_test(char* path, char* resfile, int ng, char* treefilename, char* nindivfilename, char* exprfilename, int* shiftnodes, int nshiftnodes) {
  int i, rooti;
  char filename[1000];
  char oneThetaMLsFile[1000], twoThetaMLsFile[1000], oneThetaRegimesFile[1000], twoThetaRegimesFile[1000], LRTsFile[1000];
  double *oneThetamlLs, *twoThetamlLs;
  double ****oneThetaMLregimes, ****twoThetaMLregimes;
  int truengenes;
  int maxgenename = 100;
  char **genenames;
  clock_t time0, time1; 

  //malloc global ml holder
  gmlLs = (double*) malloc(ngenes*sizeof(double));

  //read/make/write tree
  ngenes = ng; 
  nspecies = readtree(treefilename);
  totnindiv = readNIndivs(nindivfilename);
  rooti = 2*nspecies-2;
  if(verbose>0) printf("Read in tree from %s\n", treefilename);
  if(verbose>8) debugprinttree(rooti);

  //read in params from files
  setStableRegimes(&simRegimes, 1.0, 1.0, 1.0, 1.0);
  sprintf(oneThetaRegimesFile, "results/%ssharedThetaMLparams%s.res", path, resfile);

  read4dDouArrTo3dArr(simRegimes, ngenes, 1, 4, oneThetaRegimesFile);
    
  //simulate expression based on oneThetaMLRegimes (read into simRegimes)
  alloc_expr_data();
  simExprEachGene();
  if(verbose>3) printf("Simulated expression based on %s\n", oneThetaRegimesFile);

  //write sim'd expr to file
  //sprintf(filename, "data/sim/basedOn_oneThetaMLregimes%s.dat", resfile);
  sprintf(filename, "data/%ssimExpr%s.dat", path, resfile);
  write2dDouArr(expr, ngenes, totnindiv, filename);
  if(verbose>4) printf("Printed simulated data to %s\n", filename);
  
  //set up ML data structs
  oneThetamlLs = (double*) malloc(ngenes*sizeof(double));
  twoThetamlLs = (double*) malloc(ngenes*sizeof(double));

  //malloc and set up regimes
  setPerGeneRegimeEG(&oneThetaMLregimes, rooti);
  setPerGeneRegimeEGShift(&twoThetaMLregimes, rooti);

  if(verbose>2) printf("Testing constant vs branch-specific thetas per gene\n");
  time0 = clock();
  twoThetaML(oneThetaMLregimes, twoThetaMLregimes, oneThetamlLs, twoThetamlLs, nshiftnodes, shiftnodes);
  time1 = clock();
  if(verbose>4) printf("Finished one vs two theta analysis in %d sec\n", ((int) (((time1-time0)/CLOCKS_PER_SEC))));

  //write results to files
  sprintf(oneThetaRegimesFile, "results/%ssimOneThetaMLparams%s.res", path, resfile);
  sprintf(twoThetaRegimesFile, "results/%ssimTwoBSThetaMLparams%s.res", path, resfile);
  sprintf(oneThetaMLsFile, "results/%ssimOneThetaMLs%s.res", path, resfile);
  sprintf(twoThetaMLsFile, "results/%ssimTwoBSThetaMLs%s.res", path, resfile);
  sprintf(LRTsFile, "results/%ssimBSThetaTestLRTs%s.res", path, resfile);
  write4dDouArr(oneThetaMLregimes, ngenes, 1, 1, 4, oneThetaRegimesFile);
  write4dDouArr(twoThetaMLregimes, ngenes, 1, 2, 4, twoThetaRegimesFile);
  writeDouArr(oneThetamlLs, ngenes, oneThetaMLsFile);
  writeDouArr(twoThetamlLs, ngenes, twoThetaMLsFile);
  writeLRTArr(oneThetamlLs, twoThetamlLs, ngenes, LRTsFile);

  //print info on multi and rand starts
  if(multiStartFlag > 0 && verbose>30)
    printf("Number of exaggerated parameter start points that did better than estimated start points: %d\n", (multiStartFlag-1));
  if(randStartFlag > 0 && verbose>30)
    printf("Number of random parameter start points that did better than estimated start points: %d\n", (randStartFlag-1));

  //free
  freeStableRegimeEG(oneThetaMLregimes);  
  freeStableRegimeEG(twoThetaMLregimes);
  free_expr_data();
  free(oneThetamlLs);
  free(twoThetamlLs);

  return(0);
}


//simulate data (each gene using same params) and do the simple beta_shared vs beta_i test for each gene
//path is results file folder path after 'results/'???
//resfile is the parameter file extension before '.res' [can be empty]???
int simulate_data_shared_beta_test(char* path, char* resfile, int ng, int treeSizeRepeat, int treeSizeSeries, char* treefilename, char* nindivfilename, double theta, double sigma2, double alpha, double beta) { 
  int i, rooti;
  char filename[1000];//, nindivfilename[1000], treefilename[1000];
  char oneBetaMLsFile[1000], varyBetaMLsFile[1000], oneBetaRegimesFile[1000], varyBetaRegimesFile[1000], LRTsFile[1000];
  double sharedBeta;
  double *fixBetamlLs, *varyBetamlLs;
  double ***oneBetaDataMLregimes, ****fixBetaDataMLregimes, ****varyBetaDataMLregimes;
  int truengenes;
  int maxgenename = 100;
  char **genenames;
  clock_t time0, time1; 

  //get treefile names based on input
  if(treeSizeSeries > 0) {
    if(treeSizeSeries < 10) {
      sprintf(treefilename, "data/phylogenies/treeSizeSeries/size0%d.newick", treeSizeSeries);
      sprintf(nindivfilename, "data/phylogenies/treeSizeSeries/size0%d.nindiv", treeSizeSeries);
    } else {
      sprintf(treefilename, "data/phylogenies/treeSizeSeries/size%d.newick", treeSizeSeries);
      sprintf(nindivfilename, "data/phylogenies/treeSizeSeries/size%d.nindiv", treeSizeSeries);
    }
  } else if(treeSizeRepeat > 0) {
    sprintf(treefilename, "data/phylogenies/PerrySeqTreeX%d.newick", treeSizeRepeat);
    sprintf(nindivfilename, "data/phylogenies/PerrySeqTreeX%d.nindiv", treeSizeRepeat);
  } 

  //read in tree data
  ngenes = ng;
  nspecies = readtree(treefilename);
  totnindiv = readNIndivs(nindivfilename);
  rooti = 2*nspecies-2;
  if(verbose>1) printf("Considering %d simulated genes\n", ngenes);
  if(verbose>8) debugprinttree(rooti);

  //set up expr data structs
  alloc_expr_data();
  fixBetamlLs = (double*) malloc(ngenes*sizeof(double));
  varyBetamlLs = (double*) malloc(ngenes*sizeof(double));

  //malloc set up regimes to be same over genes
  setStableRegimes(&simRegimes, theta, sigma2, alpha, beta);

  //set sharedBeta
  sharedBeta = simRegimes[0][0][3];
  
  //simulate expr under regimes
  if(verbose>3) printf("====simulating %d genes\n", ngenes);
  simRegFlag = 2;
  simExprEachGene();

  //write sim'd expr to file
  if(treeSizeSeries > 0) {
    if(treeSizeSeries < 10) 
      sprintf(filename, "data/sim/treeSizeSeries_size0%d_%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f.dat", treeSizeSeries, ngenes, theta, sigma2, alpha, beta);
    else
      sprintf(filename, "data/sim/treeSizeSeries_size%d_%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f.dat", treeSizeSeries, ngenes, theta, sigma2, alpha, beta);
  } else if(treeSizeRepeat > 0) {
    sprintf(filename, "data/sim/PerrySeqTreeX%d_%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f.dat", treeSizeRepeat, ngenes, theta, sigma2, alpha, beta);
  } else {
    sprintf(filename, "data/%ssimExpr_%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f%s.dat", path, ngenes, theta, sigma2, alpha, beta, resfile);
  }
  write2dDouArr(expr, ngenes, totnindiv, filename);
  if(verbose>0) printf("  printed data to %s\n", filename);
  //delay(100);
  
  //set up ml regimes
  setPerGeneRegimeEG(&fixBetaDataMLregimes, rooti);
  setPerGeneRegimeEG(&varyBetaDataMLregimes, rooti);
  
  
  //estimate sharedBeta
  if(verbose>5) printf("====estimating shared beta\n");
  time0 = clock();
  setStableRegimeEG(&oneBetaDataMLregimes, rooti);
  sharedBeta = estMLSharedBeta(oneBetaDataMLregimes);
  freeStableRegimeEG(oneBetaDataMLregimes);
  time1 = clock();
  if(verbose>3) printf("estimated shared beta as %lf in %d seconds\n", sharedBeta, ((int) (((time1-time0)/CLOCKS_PER_SEC))));
  
  
  
  //do LR test
  if(verbose>4) printf("====testing beta shared vs vary for each gene\n");
  time0 = clock();
  fixBetaVaryBetaML(sharedBeta, fixBetaDataMLregimes, varyBetaDataMLregimes, fixBetamlLs, varyBetamlLs);
  time1 = clock();
  if(verbose>4) printf("did indiv gene tests in %d seconds\n", ((int) (((time1-time0)/CLOCKS_PER_SEC))));

  //write results
  if(treeSizeRepeat > 0) {
    sprintf(oneBetaRegimesFile, "results/runTrialSims/sim/oneBetaMLregimes%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_tree%dx.res", ngenes, theta, sigma2, alpha, beta, treeSizeRepeat);
    sprintf(varyBetaRegimesFile, "results/runTrialSims/sim/varyBetaMLregimes%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_tree%dx.res", ngenes, theta, sigma2, alpha, beta, treeSizeRepeat);
    sprintf(oneBetaMLsFile, "results/runTrialSims/sim/oneBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_tree%dx.res", ngenes, theta, sigma2, alpha, beta, treeSizeRepeat);
    sprintf(varyBetaMLsFile, "results/runTrialSims/sim/varyBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_tree%dx.res", ngenes, theta, sigma2, alpha, beta, treeSizeRepeat);
  } else if(treeSizeSeries > 0) {
    if(treeSizeSeries < 10) {
      sprintf(oneBetaRegimesFile, "results/runTrialSims/sim/treeSizeSeries/oneBetaMLregimes%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size0%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
      sprintf(varyBetaRegimesFile, "results/runTrialSims/sim/treeSizeSeries/varyBetaMLregimes%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size0%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
      sprintf(oneBetaMLsFile, "results/runTrialSims/sim/treeSizeSeries/oneBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size0%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
      sprintf(varyBetaMLsFile, "results/runTrialSims/sim/treeSizeSeries/varyBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size0%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
    } else {
      sprintf(oneBetaRegimesFile, "results/runTrialSims/sim/treeSizeSeries/oneBetaMLregimes%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
      sprintf(varyBetaRegimesFile, "results/runTrialSims/sim/treeSizeSeries/varyBetaMLregimes%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
      sprintf(oneBetaMLsFile, "results/runTrialSims/sim/treeSizeSeries/oneBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
      sprintf(varyBetaMLsFile, "results/runTrialSims/sim/treeSizeSeries/varyBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f_size%d.res", ngenes, theta, sigma2, alpha, beta, treeSizeSeries);
    }
  } else {
    sprintf(oneBetaRegimesFile, "results/%ssharedBetaMLparams%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f%s.res", path, ngenes, theta, sigma2, alpha, beta, resfile);
    sprintf(varyBetaRegimesFile, "results/%sindivBetaMLparams%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f%s.res", path, ngenes, theta, sigma2, alpha, beta, resfile);
    sprintf(oneBetaMLsFile, "results/%ssharedBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f%s.res", path, ngenes, theta, sigma2, alpha, beta, resfile);
    sprintf(varyBetaMLsFile, "results/%sindivBetaMLs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f%s.res", path, ngenes, theta, sigma2, alpha, beta, resfile);
    sprintf(LRTsFile, "results/%sbetaTestLRTs%dgenes_theta%.2f_sig2%.2f_alp%.2f_beta%.2f%s.res", path, ngenes, theta, sigma2, alpha, beta, resfile);
  }
  write4dDouArr(fixBetaDataMLregimes, ngenes, 1, 1, 4, oneBetaRegimesFile);
  write4dDouArr(varyBetaDataMLregimes, ngenes, 1, 1, 4, varyBetaRegimesFile);
  writeDouArr(fixBetamlLs, ngenes, oneBetaMLsFile);
  writeDouArr(varyBetamlLs, ngenes, varyBetaMLsFile);
  writeLRTArr(fixBetamlLs, varyBetamlLs, ngenes, LRTsFile);

  //free
  free(fixBetamlLs);
  free(varyBetamlLs);
  //freeStableRegimes(simRegimes);
  freeStableRegimeEG(simRegimes);
  free_expr_data();

  return(0);  
}


/////////////////////////////////////////////////////////////////////////
// main

main(int argc, char** argv) {
  int i;
  char treefilename[1000], nindivfilename[1000], filename[1000], treename[1000], exprfile[1000], genesetfile[1000];
  char path[1000], resfile[1000];

  int nboots, ngenesS;
  int expSharedBetaFlag, expBranchSpecificThetaFlag, simSharedBetaFlag, simThetaShiftFlag, expBootFlag, toySharedBetaFlag, speciesMeanThetaShiftFlag;
  int treeSizeSeries, treeSizeRepeat;
  double theta, sigma2, alpha, beta;
  int nshiftnodes, shiftnodes[100];

  unsigned long int initime;
  const gsl_rng_type *T;

  time_t t;
  time(&t);

  //set up random number generator
  time(&initime);
  gsl_rng_env_setup();
  T = gsl_rng_default;
  grng = gsl_rng_alloc(T);
  //grng = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_rng_set(grng, 0); //gsl_rng_set(grng, initime);              
  //printf("The SEED is %li.\n\n",initime);

  //turn off gls error aborting
  gsl_set_error_handler_off();

  //set default flags
  multiStartFlag = 1;
  randStartFlag = 0;
  expSharedBetaFlag = 0;
  expBranchSpecificThetaFlag = 0;
  speciesMeanThetaShiftFlag = 0;
  simSharedBetaFlag = 0;
  simThetaShiftFlag = 0;
  expBootFlag = 0;
  treeSizeRepeat = 0;
  treeSizeSeries = 0;

  //set default values
  nshiftnodes = 0; 
  verbose = 0;
  sprintf(treefilename, "");
  sprintf(nindivfilename, "");
  sprintf(exprfile, "");
  sprintf(path, "");
  sprintf(resfile, "");
  
  //get commandline args
  for(i=1; i<argc; i++) {
    if(argv[i][0] == '-') {
      switch(argv[i][1]) {
      case 'a':
	alpha = atof(&argv[i][3]); break;
      case 'b':
	nboots = atoi(&argv[i][3]); break;
      case 'd':
	(void) strcpy(exprfile, &argv[i][3]); break;
      case 'e':
	treeSizeSeries = atoi(&argv[i][3]); break;
      case 'f':
	(void) strcpy(resfile, &argv[i][3]); break;
      case 'g':
	sigma2 = atof(&argv[i][3]);  break;
      case 'h':
	theta = atof(&argv[i][3]);  break;
      case 'i':
	(void) strcpy(nindivfilename, &argv[i][3]); break;
      case 'j':
	beta = atof(&argv[i][3]);  break;
      case 'm':
	ngenesS = atoi(&argv[i][3]); break;
      case 'n':
	ngenes = atoi(&argv[i][3]); break;
      case 'o':
	shiftnodes[nshiftnodes] = atoi(&argv[i][3]); nshiftnodes++; break;
      case 'p':
	(void) strcpy(path, &argv[i][3]); break;
      case 'q':
	treeSizeRepeat = atoi(&argv[i][3]); break;
      case 's':
	(void) strcpy(genesetfile, &argv[i][3]); break;
      case 't':
	(void) strcpy(treefilename, &argv[i][3]); break;
      case 'v':
	verbose = atoi(&argv[i][3]); break;

      case 'B':
	expBootFlag = 1; break;
      case 'H':
	simSharedBetaFlag = 1; break;      
      case 'M':
	multiStartFlag = 1; break;
      case 'N':
	speciesMeanThetaShiftFlag = 1; break;
      case 'O':
	expBranchSpecificThetaFlag = 1; break;
      case 'P':
	simThetaShiftFlag = 1; break;
      case 'R':
	randStartFlag = 1; break;
      case 'S':
	expSharedBetaFlag = 1; break;

      default:
	printf("unknown option %s\n", argv[i]);
      }
    }
  }

  if(expSharedBetaFlag > 0) {
    experimental_data_shared_beta_test(path, resfile, ngenes, treefilename, nindivfilename, exprfile);
  }
  if(expBranchSpecificThetaFlag > 0) { // ./runTrialSims -n 675 -p runTrialSims/completePerryHigh/ -t data/phylogenies/PerrySeqTree.newick -i data/phylogenies/PerrySeqTree.nindiv -d data/expression/completePerryHigh.dat -f Human -o 14 -M -O -v 6
    experimental_data_branch_specific_theta_test(path, resfile, ngenes, treefilename, nindivfilename, exprfile, shiftnodes, nshiftnodes);
  }
  if(expBootFlag > 0) { // ./runTrialSims -n 675 -p runTrialSims/completePerryHigh/ -t data/phylogenies/PerrySeqTree.newick -i data/phylogenies/PerrySeqTree.nindiv -d data/expression/completePerryHigh.dat -b 100 -M -B -v 6
    parametric_bootstrap(path, resfile, ngenes, treefilename, nindivfilename, nboots);
  }
  if(simSharedBetaFlag > 0) {// ./runTrialSims -n 100 -q 1 -h 0.57 -g 2.66 -a 19.05 -j 0.39 -M -H
    simulate_data_shared_beta_test(path, resfile, ngenes, treeSizeRepeat, treeSizeSeries, treefilename, nindivfilename, theta, sigma2, alpha, beta);
  }
  if(simThetaShiftFlag > 0) { // ./runTrialSims -n 675 -p runTrialSims/completePerryHigh/ -t data/phylogenies/PerrySeqTree.newick -i data/phylogenies/PerrySeqTree.nindiv -d data/expression/completePerryHigh.dat -f Catarrhine -o 17 -M -P -v 6
    simulate_data_branch_specific_theta_test(path, resfile, ngenes, treefilename, nindivfilename, exprfile, shiftnodes, nshiftnodes);
  }
  if(speciesMeanThetaShiftFlag > 0) { //  ./runTrialSims -n 675 -p runTrialSims/completePerryHigh/ -t data/phylogenies/PerrySeqTree.newick -i data/phylogenies/PerrySeqTree.nindiv -d data/expression/completePerryHigh.dat -f Human -o 14 -M -N -v 6
    experimental_data_branch_specific_species_mean_theta_test(path, resfile, ngenes, treefilename, nindivfilename, exprfile, shiftnodes, nshiftnodes);
  }
}
