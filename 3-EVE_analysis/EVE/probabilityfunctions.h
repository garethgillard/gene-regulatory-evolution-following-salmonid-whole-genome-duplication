//declare useful global vars
extern struct node *tree; //phylogeny
extern double **expr; // expression data: expr[gene number][individual number]  
extern int ngenes; //total number of genes in dataset
extern int nspecies; //total number of species in dataset 
extern int totnindiv; //total number of individuals in all species
extern int findmaxgenei; //gene index considered in findmax
extern double findmaxbeta; //var for fixed beta val to be used in findmax
extern double **findmaxgeneparams; //params for all genes for nested findmax


void leafExpectation(double *Es, double **regimes);


double logL(double **regimes);
double logLfindmax(double *params);
double logLeg(double ***regimes);
double logLsharedBetaShiftfindmax(double *params);

//for (brent(bfgs)) nested max likelihood
double logLnestedSharedBetaShift(double ***regimes);
double logLnestedSharedBetaStable(double ***regimes);
double logLFixedBetaStable(double beta, double ***regimes);//, double varypopvar, double varyevolvar);
double logLFixedBetaShift(double beta, double ***regimes);




//estimate param functions
double estStableSharedBeta();


