/*
 *  Thanks to Rasmus Nielsen
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>
#include<time.h>

#include"myio.h"

//notice that my trees have their root in the ground
struct node {
  int down;
  int up[2];
  double bl;
  int regime;
  int nindiv;
  int level;
};

//some nice globals
struct node *tree; //phylogeny
double **expr; // expression data: expr[gene number][individual number]  
int ngenes; //total number of genes in dataset
int nspecies; //total number of species in dataset 
int totnindiv; //total number of individuals in all species
int verbose; //0:no in-process stdout, 100:max in-process stdout

/////////////////////////////////////////////////////////////////////////
// tree making(ish) functions

void alloc_tree() {
  tree = (struct node*) malloc((nspecies*2 - 1)*(sizeof(struct node)));
}

void alloc_expr_data() {
  int i, j;
  
  expr = (double**) malloc(ngenes*(sizeof(double*)));
  for (i=0; i<ngenes; i++)
    expr[i] = (double*) malloc(totnindiv*(sizeof(double)));
}

void free_expr_data() {
  int i;
  
  for (i=0; i<ngenes; i++)
    free(expr[i]);
  free(expr);
}

void copytree(struct node *totree, struct node *fromtree) {
  int i;
  
  for (i=0; i<2*nspecies-1; i++) {
    totree[i].down=fromtree[i].down;
    totree[i].up[0]=fromtree[i].up[0];
    totree[i].up[1]=fromtree[i].up[1];
    totree[i].bl=fromtree[i].bl;
  }
}

/////////////////////////////////////////////////////////////////////////
// tree and expr reading functions

int nodestack;
FILE *treefile;

char takeinchar() {
  char c;
  
  do{
    c=fgetc(treefile);
  } while(c=='\t'||c=='\n'||c==' ');
  if (c==EOF) {
    printf("tree read error 4\n");  
    exit(-1);
  }
  return c;
}

int getclade() {
  char c;
  int up1, up2, node, insertnode;
  
  c=takeinchar(treefile);
  if (c!='(') {
    ungetc(c,treefile);
    fscanf(treefile,"%i",&node);
    node--;
  }
  else {
    node = nodestack;
    nodestack--;
    up1 = getclade();
    if ((c=takeinchar(treefile))!=','){
      printf("read tree error 1 (%i, %c)\n",node,c); scanf("%i",&node); exit(-1);}
    up2 = getclade();
    if ((c=takeinchar(treefile))==','){/*trichotomy for unrooted tree*/
      insertnode = nodestack;
      nodestack--;
      tree[insertnode].up[0]=up1;
      tree[insertnode].up[1]=up2;
      tree[up1].down=tree[up2].down=insertnode;
      tree[insertnode].bl=0.0;                 /*insert a new node*/
      up1 = insertnode;
      up2=getclade();
      if ((c=takeinchar(treefile))!=')'){
	printf("read tree error 2 (%c)\n",c); scanf("%i",&node); exit(-1);}
    }
    tree[node].up[0]=up1;
    tree[node].up[1]=up2;
    tree[up1].down=tree[up2].down=node;
  }
  c=takeinchar(treefile);
  if (c==':')
    fscanf(treefile,"%lf",&tree[node].bl);
  else if (c!=';'){
    printf("read tree error 3 (%c)\n",c); scanf("%i",&node); exit(-1);}
  return node;
}

void addLevelsRecurse(int nodei, int curlev) {
  tree[nodei].level = curlev;
  //printf("set node %d level to %d\n", nodei, curlev);
  if(tree[nodei].up[0] > -1) { //root or internal
    addLevelsRecurse(tree[nodei].up[0], curlev+1);
    addLevelsRecurse(tree[nodei].up[1], curlev+1);
  }
}

// Rori's function to init a three-leaved tree 
// bls is array of branch lengths going to nodes 0,1,2,3 in this format
//   0   1   2
//    \   \ /
//     \   3
//      \ /
//       4
// and nindivs for nodes 0,1,2
int threeleaftree(double bl0, double bl1, double bl2, double bl3, int nind0, int nind1, int nind2) {
  int i;

  //define global val
  nspecies = 3;
  
  //alloc tree
  alloc_tree();
 
  //fill in tree vals
  tree[0].down = 4;
  tree[1].down = 3;  
  tree[2].down = 3;  
  tree[3].down = 4;  
  tree[4].down = -1;
  
  for(i=0; i<3; i++) {
    tree[i].up[0] = -1;
    tree[i].up[1] = -1;
  }
  tree[3].up[0] = 1;
  tree[3].up[1] = 2;
  tree[4].up[0] = 0;
  tree[4].up[1] = 3;


  tree[0].bl = bl0;
  tree[1].bl = bl1;
  tree[2].bl = bl2;
  tree[3].bl = bl3;
  tree[4].bl = 0.0;

  for(i=0; i<5; i++) 
    tree[i].regime = 0;

  tree[0].nindiv = nind0;
  tree[1].nindiv = nind1;
  tree[2].nindiv = nind2;

  tree[0].level = 1;
  tree[1].level = 2;
  tree[2].level = 2;
  tree[3].level = 1;
  tree[4].level = 0;

  return 3;
}

int readtree(char *filename) {
  int i, root, u;
  char c;
  
  if(verbose>1) printf("Reading tree (with species names as integers)\n");
  if (NULL == (treefile=fopen(filename, "r"))) {
    printf ("Cannot open tree file!: %s\n", filename);
    exit(-1);
  }
  
  fscanf(treefile,"%i", &nspecies);
  if(verbose>1) printf("Number of species: %i\n",nspecies);
  if (nspecies<4){
    printf("This program is takes at least four species=!\n");
    exit(1);
  }
  alloc_tree();
  while ((c=(fgetc(treefile)))!='\n'){
    if (c==EOF){
      printf("Error reading treefile\n");
      exit(-1);
    }
  }
  
  //while(takeinchar()!= '=');
  for (i=0; i<nspecies; i++){
    tree[i].up[0]=-1;
    tree[i].up[1]=-1;
  }
  root = 2*nspecies-2;
  nodestack = root;
  tree[root].down=-1;
  getclade(nspecies);
  u = tree[root].up[1];
  /*
  if (tree[u].bl>0){
    tree[tree[root].up[0]].bl = tree[tree[root].up[0]].bl + tree[u].bl;
    tree[u].bl = 0.0;
  }
  */
  fclose(treefile);

  addLevelsRecurse(root, 0);

  return nspecies;
}

//reads in nindivs from file
int readNIndivs(char *filename) {
  FILE *infile;
  int i, tot=0;
  
  if(verbose>1) printf("Reading nindivs file\n");
  if (NULL == (infile=fopen(filename, "r"))) {
    puts ("Cannot open nindiv file!");
    exit(-1);
  }

  for(i=0; i<nspecies; i++) {
    fscanf(infile,"%i", &tree[i].nindiv);
    tot += tree[i].nindiv;
  }

  close(infile);

  return(tot);
}

/////////////////////////////////////////////////////////////////////////
// tree and expr writing functions

void debugprinttree(int rooti) {
  int i;

  for(i=0; i<(rooti+1); i++) {
    printf("node %d:  down: %d  ups: %d, %d  bl: %lf  reg: %d  nindiv: %d  level: %d\n", i, tree[i].down, tree[i].up[0], tree[i].up[1], tree[i].bl, tree[i].regime, tree[i].nindiv, tree[i].level); 
  }
  
}

void printtreerecurse(int node) {
  int i;
  FILE *outfile;

  if (tree[node].up[0]==-1)	{
    fprintf(outfile,"%i",node);
    fprintf(outfile,": %lf", tree[node].bl);
  }
  else {
    fprintf(outfile,"(");
    printtreerecurse(tree[node].up[0]);
    fprintf(outfile,", ");
    printtreerecurse(tree[node].up[1]);
    fprintf(outfile,")");
    if (tree[node].down != -1) 
      fprintf(outfile,": %lf", tree[node].bl);
  }
}

void writeTree_newick(char *filename, int root) {
  FILE *outfile;
  if (NULL==(outfile=fopen(filename,"w"))){
    puts ("Cannot make the treefile!");
    exit(-1);}
  printtreerecurse(root);
  fclose(outfile);
}

void printTreeRegimes(int node) {
  if (tree[node].up[0]==-1)	{
    printf("%d",node);
    printf(":%d", tree[node].regime);
  }
  else {
    printf("(");
    printTreeRegimes(tree[node].up[0]);
    printf(",");
    printTreeRegimes(tree[node].up[1]);
    printf(")");
    if (tree[node].down != -1) 
      printf(":%d", tree[node].regime);
  }
  printf("\n");
}



/////////////////////////////////////////////////////////////////////////
// tree checking functions

int check_tree_recurse(struct node *tree, int node) {
  int u1, u2;
  
  u1 = tree[node].up[0];
  u2 = tree[node].up[1];
  if (u1==-1) {
    if (u2 != -1) return -1;
    else return 1;
  }
  else {
    if (u2==-1) return -1;
    else if (tree[u1].down != node || tree[u2].down != node ) return -1;
    else return check_tree_recurse(tree, u1) + check_tree_recurse(tree, u2);
  }
}

int check_tree(struct node *tree, int root, int nspecies) {
  if (check_tree_recurse(tree, root) != nspecies)	
    return 0;
  else 
    return 1;
}
