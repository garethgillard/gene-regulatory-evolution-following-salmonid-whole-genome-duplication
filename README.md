Gene regulatory evolution following salmonid whole genome duplication
================
Gareth Gillard
<gareth.gillard@nmbu.no>
20/05/2019

## Overview

![](readme_files/analysis_workflow.png)


<!-- 1- Ortholog data -->
![](readme_files/heading_1.png)
...

<!-- 2- Gene expression data -->
![](readme_files/heading_2.png)
...

<!-- 3- EVE analysis -->
![](readme_files/heading_3.png)
...

<!-- 4- Results -->
![](readme_files/heading_4.png)
...
