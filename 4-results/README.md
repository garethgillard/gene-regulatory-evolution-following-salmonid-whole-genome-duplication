EVE results
================
Gareth Gillard
18/05/2019

-   [Curate table of EVE results](#curate-table-of-eve-results)
    -   [Example of EVE results from a single orthogroup](#example-of-eve-results-from-a-single-orthogroup)
-   [Investigating EVE model parameters and LRT scores](#investigating-eve-model-parameters-and-lrt-scores)
    -   [LRT results against a Chi Squared distribution](#lrt-results-against-a-chi-squared-distribution)
    -   [EVE parameters for shifting genes](#eve-parameters-for-shifting-genes)
-   [Comparing gene expression shifts between duplicates and singletons](#comparing-gene-expression-shifts-between-duplicates-and-singletons)
    -   [Stats for gene shifts](#stats-for-gene-shifts)
    -   [Shifts at different levels of expression](#shifts-at-different-levels-of-expression)
    -   [Heatmap showing overlap of test results for gene shifts on different branches](#heatmap-showing-overlap-of-test-results-for-gene-shifts-on-different-branches)
    -   [Heatmap of gene expression from duplicate and singleton shifting orthogroups](#heatmap-of-gene-expression-from-duplicate-and-singleton-shifting-orthogroups)
-   [Functional enrichment in shifted genes](#functional-enrichment-in-shifted-genes)
    -   [KEGG pathway enrichment](#kegg-pathway-enrichment)
    -   [GO term enrichment](#go-term-enrichment)
    -   [Examples of divergent genes in lipid metabolism (fatty acid elongase genes)](#examples-of-divergent-genes-in-lipid-metabolism-fatty-acid-elongase-genes)

``` r
knitr::opts_chunk$set(eval = TRUE, warning=FALSE, message=FALSE, error=FALSE)
options(stringsAsFactors = FALSE)

# Libraries
library(tidyverse)
library(pheatmap)
library(gridExtra)
library(shiny)
library(plotly)
library(ape)
library(RColorBrewer)
library(Ssa.RefSeq.db)
library(limma)
library(DT)
library(knitr)

# Default ggplot theme
theme_set(theme_bw())

# Function to extract a legend from a ggplot
g_legend <- function(a.gplot){ 
  tmp <- ggplot_gtable(ggplot_build(a.gplot)) 
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box") 
  legend <- tmp$grobs[[leg]] 
  return(legend)
} 

# Species factor levels
species <- c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis")
species <- factor(species, levels = species)

# Species colours
species.colours <- list(species = c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis"),
                        colours = c("gray20", "gray40", "gray60", "darkorange3", "darkorange2", "darkorange1", "darkorange"))
```

Curate table of EVE results
===========================

``` r
# Read in all EVE LRT score results
date <- "November"
EVE.results.files <- dir(path = paste0("../3-EVE_analysis/EVE/results/", date), 
                         pattern = "LRTs",
                         recursive = TRUE,
                         full.names = TRUE)

# Levels of result factors
data.type.levels <- c("WSN", "BSNcmb", "BSNsgl", "BSNdpR")
gene.type.levels <- c("single", "dupA", "dupB", 
                      "single.Tel", "dupA.Tel", "dupB.Tel",
                      "single.Sal", "dupA.Sal", "dupB.Sal",
                      "single.Ssal", "dupA.Ssal", "dupB.Ssal",
                      "single.Salp", "dupA.Salp", "dupB.Salp",
                      "single.Omyk", "dupA.Omyk", "dupB.Omyk",
                      "single.Okis", "dupA.Okis", "dupB.Okis")
gene.type2.levels <- c("single", "duplicate")
test.type.levels <- c("Var", "Ss4R", "Drer", "Olat", "Eluc", "Ssal", "Salp", "Omyk", "Okis")

# Create combined table of EVE LRT scores
EVE.results.table <- NULL
for (file in EVE.results.files) {
  file.name <- unlist(strsplit(sub(paste0(".*", date, "/"), "", file), "/"))
  data.type <- factor(file.name[file.name %in% data.type.levels], levels = data.type.levels)[1]
  gene.type <- file.name[file.name %in% gene.type.levels][1]
  gene.type2 <- factor(ifelse(grepl("single", gene.type), "single", "duplicate"), levels = gene.type2.levels)[1]
  test.type <- factor(sub(".*test(\\w+)\\.res", "\\1", file.name[grep("test", file.name)]), levels = test.type.levels)[1]
  clan.names <- system(paste0("cut -d ' ' -f1 ", sub(basename(file), "", sub("results", "data", file)), "expression.dat"), intern = TRUE)[-1]
  LRT <- as.numeric(unlist(strsplit(readLines(file), split = " ")))
  if (grepl("BSThetaTest", file)) {
    test.params <- read.table(sub("BSThetaTestLRTs", "twoBSThetaMLparams", file), 
                              col.names = c("theta", "sigma.sq", "alpha", "beta"))
    test.params.theta <- test.params[seq(1, nrow(test.params), by = 2), ]
    test.params.thetaShift <- test.params[seq(2, nrow(test.params), by = 2), ]
    theta <- test.params.theta$theta
    thetaShift <- test.params.thetaShift$theta
    shift.direction <- ifelse(thetaShift > theta, "up", "down")
    alpha <- test.params.theta$alpha
    beta <- test.params.theta$beta
    sigma.sq <- test.params.theta$sigma.sq
  } else {
    theta <- NA
    thetaShift <- NA
    shift.direction <- NA
    alpha <- NA
    beta <- NA
    sigma.sq <- NA
  }
  table <- tbl_df(data.frame(clan = clan.names,
                             data.type = data.type,
                             gene.type = gene.type,
                             gene.type2 = gene.type2,
                             test.type = test.type,
                             LRT = LRT,
                             alpha = alpha,
                             beta = beta,
                             sigma.sq = sigma.sq,
                             theta = theta,
                             thetaShift = thetaShift,
                             shift.direction = shift.direction))
  EVE.results.table <- EVE.results.table %>% bind_rows(table)
}

# Find clan maximun LRTs
EVE.results.table <- EVE.results.table %>%
  group_by(clan, data.type, test.type) %>%
  mutate(clanMaxLRT = ifelse(gene.type2 == "single", "single", 
                             ifelse(LRT == max(LRT), "dupMax", "dupMin"))) %>%
  ungroup()

# Calculate pvalue for LRT result
EVE.results.table$pval <- EVE.results.table$LRT %>%
  sapply(function(x) pchisq(x, df=1, lower.tail=FALSE)) 

# FDR adjusted
#EVE.results.table$padj <- EVE.results.table$pval %>%
#  p.adjust(method = "fdr")

# Create symbol for level of significance
EVE.results.table$sig <- EVE.results.table$pval %>%
  sapply(function(x) ifelse(x >= 0.05, "ns", ifelse(x >= 0.01, "*", ifelse(x >= 0.001, "**", "***"))))

# Order columns and rows
EVE.results.table <- EVE.results.table %>%
  select(clan, data.type, gene.type, gene.type2, test.type, LRT, pval, sig, clanMaxLRT, 
         theta, thetaShift, shift.direction, alpha, beta, sigma.sq) %>%
  arrange(clan)


# Save results table
save(EVE.results.table, file = "EVE.results.table.15.11.RData")
write.table(EVE.results.table, file = "EVE.results.table.15.11.txt", quote = FALSE, row.names = FALSE, sep = "\t")
```

Example of EVE results from a single orthogroup
-----------------------------------------------

``` r
load("EVE.results.table.15.11.RData")
kable(filter(EVE.results.table, clan == "OG0000016_5"), digits = 4)
```

| clan         | data.type | gene.type | gene.type2 | test.type |      LRT|    pval| sig | clanMaxLRT |    theta|  thetaShift| shift.direction |   alpha|    beta|  sigma.sq|
|:-------------|:----------|:----------|:-----------|:----------|--------:|-------:|:----|:-----------|--------:|-----------:|:----------------|-------:|-------:|---------:|
| OG0000016\_5 | BSNcmb    | dupA      | duplicate  | Var       |   3.1875|  0.0742| ns  | dupMin     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNcmb    | dupA      | duplicate  | Ss4R      |  -4.8292|  1.0000| ns  | dupMax     |  -6.0114|     -6.0114| down            |  0.3221|  6.2032|    0.4150|
| OG0000016\_5 | BSNcmb    | dupB      | duplicate  | Var       |   4.2853|  0.0384| \*  | dupMax     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNcmb    | dupB      | duplicate  | Ss4R      |  -7.0120|  1.0000| ns  | dupMin     |  -4.9079|     -4.9079| down            |  1.6520|  0.9267|   10.9168|
| OG0000016\_5 | BSNdpR    | dupA      | duplicate  | Var       |   3.0033|  0.0831| ns  | dupMin     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNdpR    | dupB      | duplicate  | Var       |   4.2889|  0.0384| \*  | dupMax     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNdpR    | dupB      | duplicate  | Ss4R      |  -6.3254|  1.0000| ns  | dupMax     |  -4.8865|     -4.8865| down            |  1.7745|  0.8351|   12.5949|
| OG0000016\_5 | BSNsgl    | dupA.Okis | duplicate  | Okis      |  -3.0502|  1.0000| ns  | dupMax     |  -5.5055|     -5.5055| down            |  0.3244|  9.0888|    0.4209|
| OG0000016\_5 | BSNsgl    | dupA.Sal  | duplicate  | Var       |   3.7266|  0.0536| ns  | dupMin     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNsgl    | dupA.Salp | duplicate  | Salp      |  -2.3665|  1.0000| ns  | dupMax     |  -6.3041|     -6.3041| down            |  0.2309|  2.7822|    0.2132|
| OG0000016\_5 | BSNsgl    | dupA.Ssal | duplicate  | Drer      |  -4.4419|  1.0000| ns  | dupMin     |  -5.8453|     -5.8453| down            |  0.4553|  6.0858|    0.8291|
| OG0000016\_5 | BSNsgl    | dupA.Ssal | duplicate  | Eluc      |  -4.4419|  1.0000| ns  | dupMin     |  -5.8453|     -5.8453| down            |  0.4553|  6.0858|    0.8291|
| OG0000016\_5 | BSNsgl    | dupA.Ssal | duplicate  | Olat      |  -4.4419|  1.0000| ns  | dupMin     |  -5.8453|     -5.8453| down            |  0.4553|  6.0858|    0.8291|
| OG0000016\_5 | BSNsgl    | dupA.Ssal | duplicate  | Ssal      |  -4.4419|  1.0000| ns  | dupMin     |  -5.8453|     -5.8453| down            |  0.4553|  6.0858|    0.8291|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Var       |   2.6391|  0.1043| ns  | dupMin     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Eluc      |  -5.4225|  1.0000| ns  | dupMin     |  -5.9934|     -5.9934| down            |  0.3473|  6.2005|    0.4825|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Okis      |  -5.4225|  1.0000| ns  | dupMin     |  -5.9934|     -5.9934| down            |  0.3473|  6.2005|    0.4825|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Omyk      |  -5.4225|  1.0000| ns  | dupMax     |  -5.9934|     -5.9934| down            |  0.3473|  6.2005|    0.4825|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Salp      |  -5.4225|  1.0000| ns  | dupMin     |  -5.9934|     -5.9934| down            |  0.3473|  6.2005|    0.4825|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Ss4R      |  -5.4225|  1.0000| ns  | dupMax     |  -5.9934|     -5.9934| down            |  0.3473|  6.2005|    0.4825|
| OG0000016\_5 | BSNsgl    | dupA      | duplicate  | Ssal      |  -5.4225|  1.0000| ns  | dupMin     |  -5.9934|     -5.9934| down            |  0.3473|  6.2005|    0.4825|
| OG0000016\_5 | BSNsgl    | dupB.Okis | duplicate  | Okis      |  -5.2596|  1.0000| ns  | dupMin     |  -4.7801|     -4.7801| down            |  1.5904|  1.4641|   10.1180|
| OG0000016\_5 | BSNsgl    | dupB.Sal  | duplicate  | Var       |   1.6945|  0.1930| ns  | dupMin     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNsgl    | dupB.Salp | duplicate  | Salp      |  -3.3148|  1.0000| ns  | dupMin     |  -4.4425|     -4.4425| down            |  2.3924|  0.1721|   22.8942|
| OG0000016\_5 | BSNsgl    | dupB.Ssal | duplicate  | Drer      |  -4.0541|  1.0000| ns  | dupMax     |  -4.8719|     -4.8719| down            |  1.3138|  1.8791|    6.9042|
| OG0000016\_5 | BSNsgl    | dupB.Ssal | duplicate  | Eluc      |  -4.0541|  1.0000| ns  | dupMax     |  -4.8719|     -4.8719| down            |  1.3138|  1.8791|    6.9042|
| OG0000016\_5 | BSNsgl    | dupB.Ssal | duplicate  | Olat      |  -4.0541|  1.0000| ns  | dupMax     |  -4.8719|     -4.8719| down            |  1.3138|  1.8791|    6.9042|
| OG0000016\_5 | BSNsgl    | dupB.Ssal | duplicate  | Ssal      |  -4.0541|  1.0000| ns  | dupMax     |  -4.8719|     -4.8719| down            |  1.3138|  1.8791|    6.9042|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Var       |   4.3995|  0.0359| \*  | dupMax     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Eluc      |  -7.2410|  1.0000| ns  | dupMin     |  -4.9296|     -4.9296| down            |  1.5375|  1.0441|    9.4560|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Okis      |  -7.2410|  1.0000| ns  | dupMin     |  -4.9296|     -4.9296| down            |  1.5375|  1.0441|    9.4560|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Omyk      |  -7.2410|  1.0000| ns  | dupMin     |  -4.9296|     -4.9296| down            |  1.5375|  1.0441|    9.4560|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Salp      |  -7.2410|  1.0000| ns  | dupMin     |  -4.9296|     -4.9296| down            |  1.5375|  1.0441|    9.4560|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Ss4R      |  -7.2410|  1.0000| ns  | dupMin     |  -4.9296|     -4.9296| down            |  1.5375|  1.0441|    9.4560|
| OG0000016\_5 | BSNsgl    | dupB      | duplicate  | Ssal      |  -7.2410|  1.0000| ns  | dupMin     |  -4.9296|     -4.9296| down            |  1.5375|  1.0441|    9.4560|
| OG0000016\_5 | WSN       | dupA      | duplicate  | Var       |   2.7723|  0.0959| ns  | dupMin     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | WSN       | dupA      | duplicate  | Ss4R      |  -5.1019|  1.0000| ns  | dupMax     |  -5.9885|     -5.9885| down            |  0.3459|  5.8741|    0.4785|
| OG0000016\_5 | WSN       | dupB      | duplicate  | Var       |   4.3557|  0.0369| \*  | dupMax     |       NA|          NA| NA              |      NA|      NA|        NA|
| OG0000016\_5 | WSN       | dupB      | duplicate  | Ss4R      |  -7.0937|  1.0000| ns  | dupMin     |  -4.9447|     -4.9447| down            |  1.5135|  1.0271|    9.1630|

Investigating EVE model parameters and LRT scores
=================================================

LRT results against a Chi Squared distribution
----------------------------------------------

**On LRT cutoff** From <http://www.cbs.dtu.dk/dtucourse/cookbooks/gorm/27615/lrt.php>

It is conventional to reject the null hypothesis (that the simpler model is consistent with the data), and therefore to select model B over model A, if the LRTS Q exceeds the 95% quantile of the reference distribution. If the LRTS lies below this quantile then the null hypothesis is not rejected, and model A is selected in favour of model B. For nested models the reference distribution can be shown, asymptotically (i.e. as the sample size becomes sufficiently large), to be a chisquared distribution with pB - pA d.f., where pA and pB denote the number of parameters in model A and model B, respectively. It is standard to assume that this reference distribution also remains approximately valid for finite samples, so long as the sample size is moderately large.

``` r
# Number of single and duplicate genes
n.dups <- EVE.results.table %>% filter(gene.type == "dupA") %>% select(clan) %>% distinct() %>% nrow()
n.single <- EVE.results.table %>% filter(gene.type == "single") %>% select(clan) %>% distinct() %>% nrow() 
# chiSq
chiSq.data <- list()
chiSq.data$single <- rchisq(n.single, df = 1)
chiSq.data$dups <- rchisq(n.dups, df = 1)
# chiSq distributions
ggplot(data.frame(chiSq = chiSq.data$single), aes(x = chiSq)) + geom_density(colour = "blue") +
  geom_density(data = data.frame(chiSq = chiSq.data$dups), mapping = aes(x = chiSq), colour = "orange")
```

![](4-EVE_results_files/figure-markdown_github/chisq%20distribution%20and%20LRT%20scores-1.png)

``` r
ggplot(data.frame(chiSq = chiSq.data$single), aes(x = chiSq)) + geom_density(colour = "blue") +
  geom_density(data = data.frame(chiSq = chiSq.data$dups), mapping = aes(x = chiSq), colour = "orange") +
  geom_density(data = EVE.results.table %>% filter(data.type == "BSNsgl", gene.type == "single", test.type == "Ss4R"), aes(x = (LRT)), colour = "red") +
  geom_segment(x = 4, xend = 4, y = 0, yend = Inf, linetype = 2) +
  coord_cartesian(xlim = c(-1, 10))
```

![](4-EVE_results_files/figure-markdown_github/chisq%20distribution%20and%20LRT%20scores-2.png)

EVE parameters for shifting genes
---------------------------------

``` r
# Theta shifts for single and duplicates
g.thetaDiff <- EVE.results.table %>%
  filter(data.type == "BSNsgl", test.type == "Ss4R", sig != "ns", clanMaxLRT %in% c("dupMax", "single")) %>%
  ggplot(aes(x = log2(thetaShift / theta), colour = shift.direction, linetype = gene.type2)) +
  geom_density() +
  scale_colour_manual(values = c("firebrick2", "dodgerblue2"), limits = c("up", "down")) +
  scale_linetype_manual(values = c("dashed", "solid"), limits = c("single", "duplicate")) +
  guides(colour = "none", linetype = "none") +
  theme(axis.title.y = element_blank())

# Beta values
g.beta <- EVE.results.table %>%
  filter(data.type == "BSNsgl", test.type == "Ss4R", sig != "ns", clanMaxLRT %in% c("dupMax", "single")) %>%
  ggplot(aes(x = log10(beta), colour = shift.direction, linetype = gene.type2)) +
  geom_density() +
  scale_colour_manual(values = c("firebrick2", "dodgerblue2"), limits = c("up", "down")) +
  scale_linetype_manual(values = c("dashed", "solid"), limits = c("single", "duplicate")) +
  guides(colour = "none", linetype = "none") +
  theme(axis.title.y = element_blank())

# Alpha values
g.alpha <- EVE.results.table %>%
  filter(data.type == "BSNsgl", test.type == "Ss4R", sig != "ns", clanMaxLRT %in% c("dupMax", "single")) %>%
  ggplot(aes(x = log10(alpha), colour = shift.direction, linetype = gene.type2)) +
  geom_density() +
  scale_colour_manual(values = c("firebrick2", "dodgerblue2"), limits = c("up", "down")) +
  scale_linetype_manual(values = c("dashed", "solid"), limits = c("single", "duplicate")) +
  guides(colour = "none", linetype = "none") +
  theme(axis.title.y = element_blank())

# Sigma sq values
g.sigmaSq <- EVE.results.table %>%
  filter(data.type == "BSNsgl", test.type == "Ss4R", sig != "ns", clanMaxLRT %in% c("dupMax", "single")) %>%
  ggplot(aes(x = log10(sigma.sq), colour = shift.direction, linetype = gene.type2)) +
  geom_density() +
  scale_colour_manual(values = c("firebrick2", "dodgerblue2"), limits = c("up", "down")) +
  scale_linetype_manual(values = c("dashed", "solid"), limits = c("single", "duplicate")) +
  guides(colour = "none", linetype = "none") +
  theme(axis.title.y = element_blank())

g.evoVar <- EVE.results.table %>%
  filter(data.type == "BSNsgl", test.type == "Ss4R", sig != "ns", clanMaxLRT %in% c("dupMax", "single")) %>%
  ggplot(aes(x = log10(beta*(sigma.sq/2*alpha)), colour = shift.direction, linetype = gene.type2)) +
  geom_density() +
  scale_colour_manual(values = c("firebrick2", "dodgerblue2"), limits = c("up", "down")) +
  scale_linetype_manual(values = c("dashed", "solid"), limits = c("single", "duplicate")) +
  guides(colour = "none", linetype = "none") +
  theme(axis.title.y = element_blank())

# Legend
g.legend <- EVE.results.table %>%
  filter(data.type == "BSNsgl", test.type == "Ss4R", sig != "ns", clanMaxLRT %in% c("dupMax", "single")) %>%
  ggplot(aes(x = log10(thetaShift / theta), colour = shift.direction, linetype = gene.type2)) +
  geom_density() +
  scale_colour_manual(values = c("firebrick2", "dodgerblue2"), limits = c("up", "down")) +
  scale_linetype_manual(values = c("dashed", "solid"), limits = c("single", "duplicate")) +
  theme(legend.position = "top")
 g.legend <- g_legend(g.legend)

grid.arrange(g.legend, g.thetaDiff, g.beta, g.alpha, g.sigmaSq,
             layout_matrix = matrix(c(1, 2, 1, 3, 1, 4, 1, 5), nrow = 2), 
             heights = unit(c(1, 3), "in"))
```

![](4-EVE_results_files/figure-markdown_github/EVE%20parameter%20plots-1.png)

Comparing gene expression shifts between duplicates and singletons
==================================================================

Stats for gene shifts
---------------------

``` r
shift.stats <- data.frame()
for (test in c("Ss4R", "Drer", "Olat", "Eluc", "Ssal", "Salp", "Okis")) {

  gene <- c("single", "dupA", "dupB")
  if (test %in% c("Drer", "Olat", "Eluc", "Ssal")) {
    gene <- paste(gene, "Ssal", sep = ".")
  } else if (test %in% c("Salp", "Omyk", "Okis")) {
    gene <- paste(gene, test, sep = ".")
  }
  
  table <- EVE.results.table %>% 
    filter(test.type == test,
           gene.type %in% gene,
           clanMaxLRT == "dupMax", 
           data.type == "BSNsgl") %>% 
    select(clan, dMax.sig = sig, dMax.shiftD = shift.direction) %>%
    left_join(EVE.results.table %>% 
                filter(clanMaxLRT == "dupMin", 
                       test.type == test,
                       gene.type %in% gene,
                       data.type == "BSNsgl") %>%
                select(clan, dMin.sig = sig, dMin.shiftD = shift.direction),
              by = "clan")
  
  dup.stats <- tibble(
    test.type = test,
    gene.type = "duplicate",
    total = length(unique(table$clan)),
    noShift = sum(table$dMax.sig == "ns" & 
                    table$dMin.sig == "ns", 
                  na.rm = TRUE),
    upShift1 = sum(table$dMax.sig != "ns" & 
                     table$dMin.sig == "ns" & 
                     table$dMax.shiftD == "up", 
                   na.rm = TRUE),
    downShift1 = sum(table$dMax.sig != "ns" & 
                       table$dMin.sig == "ns" & 
                       table$dMax.shiftD == "down", 
                     na.rm = TRUE),
    upShift2 = sum(table$dMax.sig != "ns" & 
                     table$dMin.sig != "ns" & 
                     table$dMax.shiftD == "up" & 
                     table$dMin.shiftD == "up", 
                   na.rm = TRUE),
    downShift2 = sum(table$dMax.sig != "ns" & 
                       table$dMin.sig != "ns" & 
                       table$dMax.shiftD == "down" & 
                       table$dMin.shiftD == "down", 
                     na.rm = TRUE),
    counterShift = sum(table$dMax.sig != "ns" & 
                         table$dMin.sig != "ns" & 
                         table$dMax.shiftD != table$dMin.shiftD, 
                       na.rm = TRUE)
  )
  
  table <- EVE.results.table %>% 
    filter(clanMaxLRT == "single",
           gene.type %in% gene,
           test.type == test, 
           data.type == "BSNsgl")
  
  single.stats <-  tibble(
    test.type = test,
    gene.type = "single",
    total = length(unique(table$clan)),
    noShift = sum(table$sig == "ns", 
                  na.rm = TRUE),
    upShift1 = sum(table$sig != "ns" &
                     table$shift.direction == "up",
                   na.rm = TRUE),
    downShift1 = sum(table$sig != "ns" &
                       table$shift.direction == "down",
                     na.rm = TRUE)
  )
  
  shift.stats <- shift.stats %>% bind_rows(dup.stats, single.stats)
    
}

shift.stats <- shift.stats %>%
  mutate(noShift.pct = (noShift / total) * 100,
         upShift1.pct = (upShift1 / total) * 100,
         downShift1.pct = (downShift1 / total) * 100,
         upShift2.pct = (upShift2 / total) * 100,
         downShift2.pct = (downShift2 / total) * 100,
         counterShift.pct = (counterShift / total) * 100)

kable(shift.stats, digits = 2)
```

| test.type | gene.type |  total|  noShift|  upShift1|  downShift1|  upShift2|  downShift2|  counterShift|  noShift.pct|  upShift1.pct|  downShift1.pct|  upShift2.pct|  downShift2.pct|  counterShift.pct|
|:----------|:----------|------:|--------:|---------:|-----------:|---------:|-----------:|-------------:|------------:|-------------:|---------------:|-------------:|---------------:|-----------------:|
| Ss4R      | duplicate |   2835|     2063|        93|         380|        16|         236|            24|        72.77|          3.28|           13.40|          0.56|            8.32|              0.85|
| Ss4R      | single    |   2182|     1828|       223|         131|        NA|          NA|            NA|        83.78|         10.22|            6.00|            NA|              NA|                NA|
| Drer      | duplicate |   2815|     2368|       108|         126|       112|          89|             0|        84.12|          3.84|            4.48|          3.98|            3.16|              0.00|
| Drer      | single    |   2179|     1894|        65|         220|        NA|          NA|            NA|        86.92|          2.98|           10.10|            NA|              NA|                NA|
| Olat      | duplicate |   2815|     2351|       143|         106|       146|          57|             0|        83.52|          5.08|            3.77|          5.19|            2.02|              0.00|
| Olat      | single    |   2179|     1854|       193|         132|        NA|          NA|            NA|        85.08|          8.86|            6.06|            NA|              NA|                NA|
| Eluc      | duplicate |   1648|     1444|        64|          73|        36|          19|             0|        87.62|          3.88|            4.43|          2.18|            1.15|              0.00|
| Eluc      | single    |   2179|     1994|        95|          90|        NA|          NA|            NA|        91.51|          4.36|            4.13|            NA|              NA|                NA|
| Ssal      | duplicate |   2101|     1556|        66|         287|        10|         148|            22|        74.06|          3.14|           13.66|          0.48|            7.04|              1.05|
| Ssal      | single    |   2179|     1948|       123|         108|        NA|          NA|            NA|        89.40|          5.64|            4.96|            NA|              NA|                NA|
| Salp      | duplicate |   1713|     1212|       173|         209|        13|          20|            17|        70.75|         10.10|           12.20|          0.76|            1.17|              0.99|
| Salp      | single    |   2173|     1913|       126|         134|        NA|          NA|            NA|        88.03|          5.80|            6.17|            NA|              NA|                NA|
| Okis      | duplicate |   2151|     1628|        49|         303|        11|         137|            15|        75.69|          2.28|           14.09|          0.51|            6.37|              0.70|
| Okis      | single    |   2180|     1945|       142|          93|        NA|          NA|            NA|        89.22|          6.51|            4.27|            NA|              NA|                NA|

``` r
shift.stats %>%
  select(1:9) %>%
  gather(shift, count, 3:ncol(.)) %>%
  filter(shift != "total") %>%
  bind_cols(
    shift.stats %>%
      select(10:15) %>%
      gather(shift, percent, 1:ncol(.)) %>%
      select(-shift)
  ) %>%
  mutate(shift = factor(shift, levels = rev(c("downShift1", "downShift2", "upShift1", "upShift2", "counterShift", "noShift"))),
         test.type = factor(test.type, levels = ((c("Drer", "Olat", "Eluc", "Ssal", "Salp", "Okis", "Ss4R"))))) %>%
  
  ggplot(aes(x = gene.type, y = percent, fill = shift)) +
  geom_bar(stat = "identity", position = "stack") +
  #geom_text(aes(label = paste0(round(percent), "%", "|",count), y = percent, group = shift), position = position_stack()) +
  facet_grid(test.type ~ ., switch = "y") + 
  coord_flip(ylim = c(0, 30)) +
  scale_fill_manual(values = c("dodgerblue", "dodgerblue4", "firebrick1", "firebrick4", "darkorchid", NA), 
                    limits = c("downShift1", "downShift2", "upShift1", "upShift2", "counterShift", "noShift")) +
  #theme_minimal() +
  theme(axis.title = element_blank(), 
        panel.grid = element_blank(),
        strip.background = element_blank())
```

![](4-EVE_results_files/figure-markdown_github/bar%20plot%20of%20shifts-1.png)

Shifts at different levels of expression
----------------------------------------

``` r
## Singleton shifts
test <- "Ss4R"
gene <- c("single")

theta.LQ <- summary(
  EVE.results.table %>% 
    filter(test.type == test,
           gene.type %in% gene,
           data.type == "BSNsgl") %>%
    .$theta)["1st Qu."]
theta.Med <- summary(
  EVE.results.table %>% 
    filter(test.type == test,
           gene.type %in% gene,
           data.type == "BSNsgl") %>%
    .$theta)["Median"]
theta.UQ <- summary(
  EVE.results.table %>% 
    filter(test.type == test,
           gene.type %in% gene,
           data.type == "BSNsgl") %>%
    .$theta)["3rd Qu."]

table <- EVE.results.table %>% 
  filter(test.type == test,
         gene.type %in% gene,
         data.type == "BSNsgl") %>%
  mutate(expr.group = factor(ifelse(theta <= theta.LQ, "low", 
                                    ifelse((theta > theta.LQ) & (theta <= theta.Med), "medium low",
                                           ifelse((theta > theta.Med) & (theta <= theta.UQ), "medium high",
                                                  "high"))), 
                             levels = c("low", "medium low", "medium high", "high"))) %>%
  select(clan, dMax.sig = sig, dMax.shiftD = shift.direction, expr.group) %>%
  mutate(shift = ifelse(dMax.sig == "ns", "noShift",
                        ifelse(dMax.sig != "ns" & dMax.shiftD == "up", "upShift1",
                               ifelse(dMax.sig != "ns" & dMax.shiftD == "down", "downShift1", NA)))) %>%
  filter(!is.na(shift))
table$shift <- factor(table$shift, levels = c("noShift", "upShift1", "downShift1"))

table.counts <- table %>%
  select(expr.group, shift) %>%
  group_by(expr.group) %>%
  count(shift) %>%
  ungroup() %>%
  mutate(p = n / sum(n)) %>%
  mutate(shift = factor(shift, levels = c(NA, "noShift", "upShift1", "downShift1"))) %>%
  mutate(gene.type = factor("Singletons", levels = c("Singletons", "Duplicates")))

## Duplicate shifts
test <- "Ss4R"
gene <- c("dupA", "dupB")

table <- EVE.results.table %>% 
  filter(test.type == test,
         gene.type %in% gene,
         clanMaxLRT == "dupMax", 
         data.type == "BSNsgl") %>%
  mutate(expr.group = factor(ifelse(theta <= theta.LQ, "low", 
                                    ifelse((theta > theta.LQ) & (theta <= theta.Med), "medium low",
                                           ifelse((theta > theta.Med) & (theta <= theta.UQ), "medium high",
                                                  "high"))), 
                             levels = c("low", "medium low", "medium high", "high"))) %>%
  select(clan, dMax.sig = sig, dMax.shiftD = shift.direction, expr.group) %>%
  left_join(EVE.results.table %>% 
              filter(clanMaxLRT == "dupMin", 
                     test.type == test,
                     gene.type %in% gene,
                     data.type == "BSNsgl") %>%
              select(clan, dMin.sig = sig, dMin.shiftD = shift.direction),
            by = "clan") %>%
  mutate(shift = ifelse(dMax.sig == "ns" & dMin.sig == "ns", "noShift",
                        ifelse(dMax.sig != "ns" & dMin.sig == "ns" & dMax.shiftD == "up", "upShift1",
                               ifelse(dMax.sig != "ns" & dMin.sig == "ns" & dMax.shiftD == "down", "downShift1",
                                      ifelse(dMax.sig != "ns" & dMin.sig != "ns" & 
                                               dMax.shiftD == "up" & dMin.shiftD == "up", "upShift2",
                                             ifelse(dMax.sig != "ns" & dMin.sig != "ns" & 
                                                      dMax.shiftD == "down" & dMin.shiftD == "down", "downShift2",
                                                    ifelse(dMax.sig != "ns" & dMin.sig != "ns" & 
                                                             dMax.shiftD != dMin.shiftD, "oppositeShift",
                                                           NA)))))))

table$shift <- factor(table$shift, levels = c(NA, "noShift", "oppositeShift", "upShift2", "upShift1", "downShift2", "downShift1"))
table <- filter(table, !is.na(shift))

table.counts <- table.counts %>%
  rbind(
    table %>%
      select(expr.group, shift) %>%
      group_by(expr.group) %>%
      count(shift) %>%
      ungroup() %>%
      mutate(p = n / sum(n)) %>%
      mutate(shift = factor(shift, levels = c(NA, "noShift", "oppositeShift", "upShift2", "upShift1", "downShift2", "downShift1"))) %>%
      mutate(gene.type = factor("Duplicates", levels = c("Singletons", "Duplicates")))
  )

# Bar plot of proportions
table.counts %>%
  ggplot(aes(x = expr.group, y = p, fill = shift)) +
  geom_bar(stat = "identity") +
  scale_fill_manual(values = c("dodgerblue", "dodgerblue4", "firebrick1", "firebrick4", "darkorchid", "grey90"),
                    limits = c("downShift1", "downShift2", "upShift1", "upShift2", "oppositeShift", "noShift")) +
  coord_flip() +
  labs(x = "Expression level group", y = "Proportion") +
  facet_grid(gene.type~., switch = "y") +
  theme_classic(base_size = 16) +
  theme(strip.background = element_blank())
```

![](4-EVE_results_files/figure-markdown_github/unnamed-chunk-1-1.png)

Heatmap showing overlap of test results for gene shifts on different branches
-----------------------------------------------------------------------------

``` r
### Duplicate
sumShiftDirection <- function (shiftA, shiftB) {
 shiftSum <- NULL
 for (i in 1:length(shiftA)) {
   if (is.na(shiftA[i]) & is.na(shiftB[i])) {
     shiftSum[i] <- 0
   } else {
     shiftSum[i] <- sum(c(shiftA[i], shiftB[i]), na.rm = TRUE)
   }
 }
 return (shiftSum)
}

## Ss4R
Ss4R.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Ss4R", gene.type == "dupA") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Ss4R", gene.type == "dupB") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Ss4R = rowSums(cbind(dupA, dupB)))

Eluc.Ssal.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Eluc", gene.type == "dupA.Ssal") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Eluc", gene.type == "dupB.Ssal") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Eluc = rowSums(cbind(dupA, dupB)))

Ssal.Ssal.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Ssal", gene.type == "dupA.Ssal") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Ssal", gene.type == "dupB.Ssal") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Ssal = rowSums(cbind(dupA, dupB)))

Drer.Ssal.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Drer", gene.type == "dupA.Ssal") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Drer", gene.type == "dupB.Ssal") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Drer = rowSums(cbind(dupA, dupB)))

Olat.Ssal.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Olat", gene.type == "dupA.Ssal") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Olat", gene.type == "dupB.Ssal") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Olat = rowSums(cbind(dupA, dupB)))

Salp.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Salp", gene.type == "dupA.Salp") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Salp", gene.type == "dupB.Salp") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Salp = rowSums(cbind(dupA, dupB)))

Okis.table <-
  # dupMax
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Okis", gene.type == "dupA.Okis") %>%
  transmute(clan, dupA = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)) %>%
  # dupMin
  left_join(
    EVE.results.table %>% 
      filter(data.type == "BSNsgl", test.type == "Okis", gene.type == "dupB.Okis") %>%
      transmute(clan, dupB = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1)),
    by = "clan") %>%
  # Sum shift directions
  transmute(clan, Okis = rowSums(cbind(dupA, dupB)))

heatmap.data <-
  Drer.Ssal.table %>%
  left_join(Olat.Ssal.table, by = "clan") %>%
  left_join(Eluc.Ssal.table, by = "clan") %>%
  left_join(Ssal.Ssal.table, by = "clan") %>%
  left_join(Salp.table, by = "clan") %>%
  left_join(Okis.table, by = "clan") %>%
  left_join(Ss4R.table, by = "clan")

dup.heatmap <-
  heatmap.data[which(rowSums(heatmap.data[,-1] != 0) > 0), -1] %>% t() %>%
  pheatmap(show_colnames = FALSE, cluster_rows = FALSE, scale = "none", cellheight = 10, main = "Duplicate shifts")
```

![](4-EVE_results_files/figure-markdown_github/heatmaps%20of%20overlap%20of%20shifts%20from%20different%20tests-1.png)

``` r
### Singleton
Ss4R.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Ss4R", gene.type == "single") %>%
  transmute(clan, Ss4R = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

Eluc.Ssal.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Eluc", gene.type == "single.Ssal") %>%
  transmute(clan, Eluc = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

Ssal.Ssal.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Ssal", gene.type == "single.Ssal") %>%
  transmute(clan, Ssal = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

Drer.Ssal.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Drer", gene.type == "single.Ssal") %>%
  transmute(clan, Drer = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

Olat.Ssal.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Olat", gene.type == "single.Ssal") %>%
  transmute(clan, Olat = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

Salp.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Salp", gene.type == "single.Salp") %>%
  transmute(clan, Salp = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

Okis.table <-
  EVE.results.table %>% 
  filter(data.type == "BSNsgl", test.type == "Okis", gene.type == "single.Okis") %>%
  transmute(clan, Okis = ifelse(sig == "ns", 0, 1) * ifelse(shift.direction == "up", 1, -1))

heatmap.data <-
  Drer.Ssal.table %>%
  left_join(Olat.Ssal.table, by = "clan") %>%
  left_join(Eluc.Ssal.table, by = "clan") %>%
  left_join(Ssal.Ssal.table, by = "clan") %>%
  left_join(Salp.table, by = "clan") %>%
  left_join(Okis.table, by = "clan") %>%
  left_join(Ss4R.table, by = "clan")

single.heatmap <- 
  heatmap.data[which(rowSums(heatmap.data[,-1] != 0) > 0), -1] %>% t() %>%
  pheatmap(show_colnames = FALSE, cluster_rows = FALSE, scale = "none", cellheight = 10, main = "Singleton shifts")
```

![](4-EVE_results_files/figure-markdown_github/heatmaps%20of%20overlap%20of%20shifts%20from%20different%20tests-2.png)

### Compared heatmaps, singleton and duplicate

``` r
grid.arrange(dup.heatmap[[4]], single.heatmap[[4]], ncol = 1)
```

![](4-EVE_results_files/figure-markdown_github/unnamed-chunk-2-1.png)

Heatmap of gene expression from duplicate and singleton shifting orthogroups
----------------------------------------------------------------------------

``` r
load("../2-gene_expression_data/2.3-expression_normalisation/EVE.normalised.exprs.tables.08.11.RData")
```

``` r
# Singletons
test <- "Ss4R"
gene <- "single"
data <- "BSNsgl"

table <- EVE.results.table %>% 
  filter(test.type == test, gene.type == gene, data.type == data, sig != "ns")

# Expression
expr <- norm.exprs.tables[[data]][[gene]] %>% 
  filter(clan %in% table$clan) %>% 
  group_by(clan) %>% slice(1) %>% as.data.frame()
rownames(expr) <- expr$clan
expr$clan <- NULL

# Order species (cols)
col.order <- c(grep("Okis", colnames(expr)),
               grep("Omyk", colnames(expr)),
               grep("Salp", colnames(expr)),
               grep("Ssal", colnames(expr)),
               grep("Eluc", colnames(expr)),
               grep("Olat", colnames(expr)),
               grep("Drer", colnames(expr)))
expr <- expr[, rev(col.order)]

# Scale
expr <- t(scale(t(expr), center = TRUE, scale = TRUE))

# Cluster genes
expr.dist <- dist(expr)
expr.hclust <- hclust(expr.dist)
expr.group <- cutree(expr.hclust, k = 2)

# Annotate columns, genes
annot.col <- data.frame(
  row.names = table$clan,
  shift = table$shift.direction
)

# Annotate rows, samples
species.group = sub("\\..*", "", colnames(expr))

# Annotation colours
annot.colours <- list(
  shift = c("up" = "firebrick1", "down" = "dodgerblue")
)

# legend colours
breaks <- c(min(expr), seq(-3, 3, length.out = 100), max(expr))
pheat.colours <- colorRampPalette(rev(brewer.pal(n = 11, name = "RdYlBu")))(101)
colours <- rev(brewer.pal(n = 9, name = "RdYlBu"))

png("figures/singleton.shifts.pheatmap.png", height = 300 , width = 500)
pheatmap(t(expr), 
         scale = "none", 
         show_rownames = FALSE, 
         show_colnames = FALSE, 
         cluster_cols = TRUE,
         cluster_rows = FALSE,
         gaps_row = c(seq(4, 20, by = 4), 23),
         annotation_col = annot.col,
         annotation_colors = annot.colours,
         color = pheat.colours,
         cellwidth = 1, 
         cellheight = 5,
         breaks = breaks
         )
dev.off()
```

``` r
# Duplicate
test <- "Ss4R"
gene <- "dupA"
gene2 <- "dupB"

dup.sig.clans <- EVE.results.table %>% filter(test.type == test, gene.type2 == "duplicate", data.type == data, sig != "ns") %>% .$clan %>% unique

table <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == "duplicate", data.type == data, clan %in% dup.sig.clans) %>%
  select(clan, gene.type, clanMaxLRT) %>%
  spread(clanMaxLRT, gene.type) %>% 
  select(clan, dupMin, dupMax)

dup.which.shift <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == "duplicate", data.type == data) %>%
  select(clan, gene.type, sig) %>%
  spread(gene.type, sig) %>%
  mutate(shift = ifelse(dupA != "ns" & dupB != "ns", "2",
                        ifelse(dupA != "ns" | dupB != "ns", "1", "0"))) %>%
  select(clan, shift) %>%
  left_join(
    EVE.results.table %>% 
      filter(test.type == test, gene.type2 == "duplicate", data.type == data) %>%
      select(clan, clanMaxLRT, shift.direction) %>%
      mutate(shift.direction = ifelse(shift.direction == "up", 1, -1)) %>%
      spread(clanMaxLRT, shift.direction)
  ) %>%
  mutate(shift.code = ifelse(
    # If both shift
    shift == 2,
    # If both dups are up, then its '2'
    ifelse(dupMax == 1 & dupMin == 1, 2,
           # else if both dups are down, then its '-2'
           ifelse(dupMax == -1 & dupMin == -1, -2,
                  # else if both shift but not the same, then its '9'
                  9)),
    # Else, if only one shift
    ifelse(shift == 1, 
           # If one shifts up, then its 1
           ifelse(dupMax == 1, 1,
                  # Else if one shifts down, then its -1. If not, then just 0
                  ifelse(dupMax == -1, -1, 0)
           ),
           # Else if none shift, set 0
           0
    )
  ))
dup.which.shift <- data.frame(dup.which.shift)
rownames(dup.which.shift) <- dup.which.shift$clan
dup.which.shift$clan <- NULL

expr <- data.frame()
for (i in 1:nrow(table)) {
  c <- table$clan[i]
  dupMax <- table$dupMax[i]
  dupMin <- table$dupMin[i]
  
  exprs <- norm.exprs.tables[[data]][[dupMax]] %>% 
    select(c(1:13)) %>%
    filter(clan == c) %>%
    bind_cols(
      norm.exprs.tables[[data]][[dupMin]] %>%
        filter(clan == c) %>%
        select(c(14:ncol(.))) %>%
        setNames(paste0(colnames(.), ".dupMin"))
        
    ) %>%
    bind_cols(
      norm.exprs.tables[[data]][[dupMax]] %>%
        filter(clan == c) %>%
        select(c(14:ncol(.))) %>%
        setNames(paste0(colnames(.), ".dupMax"))
        
    )
  expr <- expr %>% 
    bind_rows(exprs)
}
rownames(expr) <- expr$clan
expr$clan <- NULL

# Order species (cols)
col.order <- c(grep("Okis.*dupMax", colnames(expr)),
               grep("Omyk.*dupMax", colnames(expr)),
               grep("Salp.*dupMax", colnames(expr)),
               grep("Ssal.*dupMax", colnames(expr)),
               
               grep("Okis.*dupMin", colnames(expr)),
               grep("Omyk.*dupMin", colnames(expr)),
               grep("Salp.*dupMin", colnames(expr)),
               grep("Ssal.*dupMin", colnames(expr)),
               
               grep("Eluc", colnames(expr)),
               grep("Olat", colnames(expr)),
               grep("Drer", colnames(expr)))
expr <- expr[, rev(col.order)]

# Scale
expr <- t(scale(t(expr), center = TRUE, scale = TRUE))

# Cluster genes
expr.dist <- dist(expr)
expr.hclust <- hclust(expr.dist)
expr.group <- cutree(expr.hclust, k = 4)

# Annotate rows, samples
species.group = sub("\\.exprs(\\.\\d){0,1}", "", colnames(expr))

# Annotate columns, genes
annot.col <- data.frame(
  row.names = rownames(dup.which.shift),
  shift = paste(dup.which.shift$shift.code)
)

# Annotation colours
annot.colours <- list(
  shift = c("1" = "firebrick1", "2" = "firebrick4", "-1" = "dodgerblue", "-2" = "dodgerblue4", "9" = "purple", "NA" = "black", "0" = "white")
)

# legend colours
breaks <- c(min(expr), seq(-3, 3, length.out = 100), max(expr))
pheat.colours <- colorRampPalette(rev(brewer.pal(n = 11, name = "RdYlBu")))(101)
colours <- rev(brewer.pal(n = 9, name = "RdYlBu"))

png("figures/duplicate.shifts.pheatmap.png", height = 400 , width = 900)
pheatmap(t(expr), 
         scale = "none", 
         show_rownames = FALSE, 
         show_colnames = FALSE, 
         cluster_cols = TRUE,
         cluster_rows = FALSE,
         gaps_row = c(seq(4, 20, by = 4), seq(23, 35, by = 4), 38),
         annotation_col = annot.col,
         annotation_colors = annot.colours,
         color = pheat.colours,
         cellwidth = 1, 
         cellheight = 5,
         breaks = breaks
         )
dev.off()
```

### Singleton shifts heatmap of expression

![Singleton shift heatmap](figures/singleton.shifts.pheatmap.png)

### Duplicate shifts heatmap of expression

![Duplicate shift heatmap](figures/duplicate.shifts.pheatmap.png)

Functional enrichment in shifted genes
======================================

KEGG pathway enrichment
-----------------------

``` r
top.kegg.results <- list()

load("../1-ortholog_data/1.2-find_singleton+duplicate_genes/EVE.clan.tables.RData")

# Single shift enrichment
# Up shift
test <- "Ss4R"
gene <- "single"
data <- "BSNsgl"
shift <- "up"
top.clans <- EVE.results.table %>% filter(test.type == test, gene.type == gene, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
kegg.universe <- sub(".*:", "", get.id(paste0(single.clan.table$Ssal, ".1"), id.type = "protein")$gene_id)
kegg.enriched <- sub(".*:", "", get.id(paste0(single.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id)
top.kegg <- kegga(kegg.enriched, universe = kegg.universe, species.KEGG = "sasa")
top.kegg <- top.kegg[order(top.kegg$P.DE),]
top.kegg.results$single.up <- top.kegg[top.kegg$P.DE < 0.05, ]
tibble(top.kegg.results$single.up)

# Down shift
shift <- "down"
top.clans <- EVE.results.table %>% filter(test.type == test, gene.type == gene, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
kegg.universe <- sub(".*:", "", get.id(paste0(single.clan.table$Ssal, ".1"), id.type = "protein")$gene_id)
kegg.enriched <- sub(".*:", "", get.id(paste0(single.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id)
top.kegg <- kegga(kegg.enriched, universe = kegg.universe, species.KEGG = "sasa")
top.kegg <- top.kegg[order(top.kegg$P.DE),]
top.kegg.results$single.down <- top.kegg[top.kegg$P.DE < 0.05, ]
tibble(top.kegg.results$single.down)

# Duplicate shift enrichment
# Shift up
test <- "Ss4R"
gene <- "duplicate"
clanMax <- "dupMax"
data <- "BSNsgl"
shift <- "up"
top.clans <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == gene, clanMaxLRT == clanMax, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
kegg.universe <- sub(".*:", "", get.id(paste0(dupA.clan.table$Ssal, ".1"), id.type = "protein")$gene_id)
kegg.enriched <- sub(".*:", "", get.id(paste0(dupA.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id)
top.kegg <- kegga(kegg.enriched, universe = kegg.universe, species.KEGG = "sasa")
top.kegg <- top.kegg[order(top.kegg$P.DE),]
top.kegg.results$duplicate.up <- top.kegg[top.kegg$P.DE < 0.05, ]
tibble(top.kegg.results$duplicate.up)

# Shift down
shift <- "down"
top.clans <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == gene, clanMaxLRT == clanMax, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
kegg.universe <- sub(".*:", "", get.id(paste0(dupA.clan.table$Ssal, ".1"), id.type = "protein")$gene_id)
kegg.enriched <- sub(".*:", "", get.id(paste0(dupA.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id)
top.kegg <- kegga(kegg.enriched, universe = kegg.universe, species.KEGG = "sasa")
top.kegg <- top.kegg[order(top.kegg$P.DE),]
top.kegg.results$duplicate.down <- top.kegg[top.kegg$P.DE < 0.05, ]
tibble(top.kegg.results$duplicate.down)

library(writexl)
write_xlsx(top.kegg.results, path = "top.kegg.results.xlsx")
```

GO term enrichment
------------------

``` r
# function to run topGO 
runTopGO <- function(DEgeneIDs, bkgGeneIDs, gene2GO, ontology = "BP", plotFilePrefix = NULL){

  # Convert the DEgeneIDs and bkgGeneIDs to named factor
  allGenes <- factor(as.integer(bkgGeneIDs %in% DEgeneIDs)) %>% 
    setNames(bkgGeneIDs)

  # build GO DAG of the included GOs
  GOdata <- new("topGOdata",
              ontology = ontology, 
              allGenes = allGenes,
              #nodeSize = 5, # only include GO terms with more than 5 genes
              annot = annFUN.gene2GO, 
              gene2GO = gene2GO)
  
  # run tests
  classic.fisher <- runTest(GOdata, algorithm = "classic", statistic = "fisher")
  weight01.fisher <- runTest(GOdata, algorithm = "weight01", statistic = "fisher")

  # Generate table of enrichment scores with all GOs
  allGOs <- names(score(classic.fisher))
  resTable <- 
    termStat(GOdata, allGOs) %>% 
    rownames_to_column(var = "GO") %>% 
    mutate( fisher.Pval = score(classic.fisher)[GO] ) %>% 
    mutate( fisherWeighted.Pval = score(weight01.fisher)[GO] ) %>% 

    # Add FDR adjusted P-values (Benjamini & Hochberg)
    mutate( fisher.Padj = p.adjust(fisher.Pval, method="BH")) %>% 
    arrange( fisher.Pval ) %>% 
    mutate( term = getGOterms(GO) )
  
  return(resTable)
}

library(biomaRt)
getGOterms <- function(GOids){
  
  quotedGOids <- paste(paste("'", unique(GOids), "'", sep = ""), collapse = ",")
  retVal <- DBI::dbGetQuery(GO_dbconn(), 
                       paste0("select term, go_id FROM go_term ",
                              "WHERE go_id IN (", quotedGOids, ");"))
  return( setNames(retVal$term,retVal$go_id)[GOids] )
}  
```

``` r
library(topGO)

top.go.results <- list()

# Get all BP GO terms for Atlantic salmon genes
gene2GO.table <- get.GO("*", TERM = "BP")
gene2GO <- setNames(gene2GO.table$go_id, gene2GO.table$gene_id)

# Single shift enrichment
# Up shift
test <- "Ss4R"
gene <- "single"
data <- "BSNsgl"
shift <- "up"
top.clans <- EVE.results.table %>% filter(test.type == test, gene.type == gene, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
universe.geneIds <- get.id(paste0(single.clan.table$Ssal, ".1"), id.type = "protein")$gene_id
enriched.geneIds <- get.id(paste0(single.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id
top.go <- runTopGO( DEgeneIDs = enriched.geneIds,
                    bkgGeneIDs = universe.geneIds, 
                    gene2GO = gene2GO, 
                    ontology = "BP")
top.go.results$single.up <- top.go[order(top.go$fisher.Pval),]
tibble(top.go.results$single.up)

# Down shift
shift <- "down"
top.clans <- EVE.results.table %>% filter(test.type == test, gene.type == gene, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
universe.geneIds <- get.id(paste0(single.clan.table$Ssal, ".1"), id.type = "protein")$gene_id
enriched.geneIds <- get.id(paste0(single.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id
top.go <- runTopGO( DEgeneIDs = enriched.geneIds,
                    bkgGeneIDs = universe.geneIds, 
                    gene2GO = gene2GO, 
                    ontology = "BP")
top.go.results$single.down <- top.go[order(top.go$fisher.Pval),]
tibble(top.go.results$single.down)

# Duplicate shift enrichment
# Shift up
test <- "Ss4R"
gene <- "duplicate"
clanMax <- "dupMax"
data <- "BSNsgl"
shift <- "up"
top.clans <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == gene, clanMaxLRT == clanMax, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
universe.geneIds <- get.id(paste0(dupA.clan.table$Ssal, ".1"), id.type = "protein")$gene_id
enriched.geneIds <- get.id(paste0(dupA.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id
top.go <- runTopGO( DEgeneIDs = enriched.geneIds,
                    bkgGeneIDs = universe.geneIds, 
                    gene2GO = gene2GO, 
                    ontology = "BP")
top.go.results$duplicate.up <- top.go[order(top.go$fisher.Pval),]
tibble(top.go.results$duplicate.up)

# Shift down
shift <- "down"
top.clans <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == gene, clanMaxLRT == clanMax, data.type == data, shift.direction == shift, sig != "ns") %>% .$clan
universe.geneIds <- get.id(paste0(dupA.clan.table$Ssal, ".1"), id.type = "protein")$gene_id
enriched.geneIds <- get.id(paste0(dupA.clan.table %>% filter(clan %in% top.clans) %>% .$Ssal, ".1"), id.type = "protein")$gene_id
top.go <- runTopGO( DEgeneIDs = enriched.geneIds,
                    bkgGeneIDs = universe.geneIds, 
                    gene2GO = gene2GO, 
                    ontology = "BP")
top.go.results$duplicate.down <- top.go[order(top.go$fisher.Pval),]
tibble(top.go.results$duplicate.down)

write_xlsx(top.go.results, path = "top.go.results.xlsx")
```

Examples of divergent genes in lipid metabolism (fatty acid elongase genes)
---------------------------------------------------------------------------

``` r
# Clan elovl6
clan.elovl6 <- "OG0005756_1"

# Clan elovl5
clan.elovl5 <- "OG0007812_1"

# Clan 
# Ssal: elovl1 (shift) / elovl7
# Omyk: elovl1 (shift) / elovl1
# Eluc: elovl1
# Drer: elovl1b
clan.elovl1 <- "OG0007656_1"
```

``` r
# Duplicate
test <- "Ss4R"
gene <- "dupA"
gene2 <- "dupB"
data <- "BSNsgl"

dup.sig.clans <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == "duplicate", data.type == data, sig != "ns") %>% 
  .$clan %>% unique

table <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == "duplicate", data.type == data, clan %in% dup.sig.clans) %>%
  select(clan, gene.type, clanMaxLRT) %>%
  spread(clanMaxLRT, gene.type) %>% 
  select(clan, dupMin, dupMax)

dup.which.shift <- EVE.results.table %>% 
  filter(test.type == test, gene.type2 == "duplicate", data.type == data) %>%
  select(clan, gene.type, sig) %>%
  spread(gene.type, sig) %>%
  mutate(shift = ifelse(dupA != "ns" & dupB != "ns", "2",
                        ifelse(dupA != "ns" | dupB != "ns", "1", "0"))) %>%
  select(clan, shift) %>%
  left_join(
    EVE.results.table %>% 
      filter(test.type == test, gene.type2 == "duplicate", data.type == data) %>%
      select(clan, clanMaxLRT, shift.direction) %>%
      mutate(shift.direction = ifelse(shift.direction == "up", 1, -1)) %>%
      spread(clanMaxLRT, shift.direction)
  ) %>%
  mutate(shift.code = ifelse(
    # If both shift
    shift == 2,
    # If both dups are up, then its '2'
    ifelse(dupMax == 1 & dupMin == 1, 2,
           # else if both dups are down, then its '-2'
           ifelse(dupMax == -1 & dupMin == -1, -2,
                  # else if both shift but not the same, then its '9'
                  9)),
    # Else, if only one shift
    ifelse(shift == 1, 
           # If one shifts up, then its 1
           ifelse(dupMax == 1, 1,
                  # Else if one shifts down, then its -1. If not, then just 0
                  ifelse(dupMax == -1, -1, 0)
           ),
           # Else if none shift, set 0
           0
    )
  ))

expr <- data.frame()
for (i in 1:nrow(table)) {
  c <- table$clan[i]
  dupMax <- table$dupMax[i]
  dupMin <- table$dupMin[i]
  
  exprs <- norm.exprs.tables[[data]][[dupMax]] %>% 
    select(c(1:13)) %>%
    filter(clan == c) %>%
    bind_cols(
      norm.exprs.tables[[data]][[dupMin]] %>%
        filter(clan == c) %>%
        select(c(14:ncol(.))) %>%
        setNames(paste0(colnames(.), ".dupMin"))
        
    ) %>%
    bind_cols(
      norm.exprs.tables[[data]][[dupMax]] %>%
        filter(clan == c) %>%
        select(c(14:ncol(.))) %>%
        setNames(paste0(colnames(.), ".dupMax"))
        
    )
  expr <- expr %>% 
    bind_rows(exprs)
}

expr <- tbl_df(expr) %>%
  filter(clan %in% c(clan.elovl1, clan.elovl5, clan.elovl6)) %>%
  gather(species, expression, 2:ncol(.)) %>%
  mutate(species = factor(sub("\\.exprs(\\.\\d){0,1}", "", species), levels = rev(c("Drer", "Olat", "Eluc", "Ssal.dupMin", "Salp.dupMin", "Omyk.dupMin", "Okis.dupMin", "Ssal.dupMax", "Salp.dupMax", "Omyk.dupMax", "Okis.dupMax")))) %>%
  arrange(clan)

expr$gene.type <- factor(ifelse(grepl("dupMin", expr$species), "Salmonid duplicate (min)", 
                         ifelse(grepl("dupMax", expr$species), "Salmonid duplicate (max)", 
                                "Pre-WGD teleosts")),
                         levels = c("Pre-WGD teleosts", "Salmonid duplicate (min)", "Salmonid duplicate (max)"))

expr$gene.name <- factor(ifelse(grepl("OG0005756_1", expr$clan), "elovl6", 
                         ifelse(grepl("OG0007812_1", expr$clan), "elovl5", 
                                "elovl1")),
                         levels = c("elovl1", "elovl5", "elovl6"))
```

``` r
expr %>%
  ggplot(aes(x = species, y = expression, fill = species)) +
  geom_boxplot() +
  coord_flip() +
  scale_fill_manual(values = c(species.colours$colours, species.colours$colours[4:8]), limits = c("Drer", "Olat", "Eluc", "Ssal.dupMin", "Salp.dupMin", "Omyk.dupMin", "Okis.dupMin", "Ssal.dupMax", "Salp.dupMax", "Omyk.dupMax", "Okis.dupMax")) +
  facet_grid(gene.type ~ gene.name, scales = "free", switch = "y") +
  labs(y = "Gene expression (log2TPM)", x = "none") +
  guides(fill = "none") +
  theme_bw()
```

![](4-EVE_results_files/figure-markdown_github/boxplots-1.png)

``` r
ggsave("figures/elovl.boxplots.pdf", width = 6, height = 5, units = "in")
```

``` r
# elovl1
EVE.results.table %>%
  filter(clan == clan.elovl1, data.type == "BSNsgl", test.type %in% c("Ss4R")) %>%
  kable(caption = "Elovl1 EVE results")
```

| clan         | data.type | gene.type | gene.type2 | test.type |        LRT|       pval| sig  | clanMaxLRT |      theta|  thetaShift| shift.direction |        alpha|       beta|    sigma.sq|
|:-------------|:----------|:----------|:-----------|:----------|----------:|----------:|:-----|:-----------|----------:|-----------:|:----------------|------------:|----------:|-----------:|
| OG0007656\_1 | BSNsgl    | dupA      | duplicate  | Ss4R      |   8.068906|  0.0045031| \*\* | dupMax     |   2.620635|    5.913525| up              |  1914.136325|  0.6393530|  2780.53049|
| OG0007656\_1 | BSNsgl    | dupB      | duplicate  | Ss4R      |  -1.198156|  1.0000000| ns   | dupMin     |  -0.003584|   -0.003584| down            |     3.307044|  0.0842056|    43.74617|

``` r
# elovl5
EVE.results.table %>%
  filter(clan == clan.elovl5, data.type == "BSNsgl", test.type %in% c("Ss4R")) %>%
  kable(caption = "Elovl5 EVE results")
```

| clan         | data.type | gene.type | gene.type2 | test.type |        LRT|       pval| sig | clanMaxLRT |     theta|  thetaShift| shift.direction |         alpha|       beta|      sigma.sq|
|:-------------|:----------|:----------|:-----------|:----------|----------:|----------:|:----|:-----------|---------:|-----------:|:----------------|-------------:|----------:|-------------:|
| OG0007812\_1 | BSNsgl    | dupA      | duplicate  | Ss4R      |  4.4455706|  0.0349919| \*  | dupMax     |  3.992374|    8.269446| up              |  5.625272e+02|  1.1243084|  1.937307e+03|
| OG0007812\_1 | BSNsgl    | dupB      | duplicate  | Ss4R      |  0.8726377|  0.3502261| ns  | dupMin     |  3.992565|    5.269623| up              |  3.262704e+07|  0.9473984|  1.581839e+08|

``` r
# elovl6
EVE.results.table %>%
  filter(clan == clan.elovl6, data.type == "BSNsgl", test.type %in% c("Ss4R")) %>%
  kable(caption = "Elovl6 EVE results")
```

| clan         | data.type | gene.type | gene.type2 | test.type |       LRT|       pval| sig | clanMaxLRT |     theta|  thetaShift| shift.direction |         alpha|     beta|      sigma.sq|
|:-------------|:----------|:----------|:-----------|:----------|---------:|----------:|:----|:-----------|---------:|-----------:|:----------------|-------------:|--------:|-------------:|
| OG0005756\_1 | BSNsgl    | dupA      | duplicate  | Ss4R      |  5.107962|  0.0238163| \*  | dupMax     |  4.071059|    7.325304| up              |      241.7051|  3.55511|      310.8792|
| OG0005756\_1 | BSNsgl    | dupB      | duplicate  | Ss4R      |  1.055902|  0.3041518| ns  | dupMin     |  4.080705|    5.288670| up              |  1767244.4633|  2.81246|  4542674.4936|

<!-- OLD



# Intersect of 'significant' shift detection between input data types











-->
